package com.auto.framework.base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.openqa.selenium.Platform;
import org.testng.Reporter;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import java.time.format.DateTimeFormatter;

public abstract class BaseTest {
	public static ExtentHtmlReporter report = null;
	public static ExtentReports extent = null;
	public static ExtentTest test = null;

	public static ExtentTest extentnode;
	public static String testFileName = TestListener.getContextName();
	public static String macPath = System.getProperty("user.dir") + "/TestReport_" + java.time.LocalDateTime.now()
			+ ".html";
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-M-yyyy hh.mm a");
	public static String windowsPath = System.getProperty("user.dir") + "\\TestReport_"
			+ java.time.LocalDateTime.now().format(formatter) + ".html";
	private static Platform platform;
	public AppiumDriver<MobileElement> driver;
	public static ExtentTest logInfo = null;

	public static ExtentReports setUp() {
		try {
			platform = getCurrentPlatform();
			String fileName = getReportFileLocation(platform);
			ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
			htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
			htmlReporter.config().setChartVisibilityOnOpen(true);
			htmlReporter.config().setTheme(Theme.STANDARD);
			htmlReporter.config().setDocumentTitle(fileName);
			htmlReporter.config().setEncoding("utf-8");
			htmlReporter.config().setReportName(fileName);
			htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
			extent = new ExtentReports();
			extent.attachReporter(htmlReporter);
			return extent;
		} catch (Exception e) {
			Log.error("Exception Occured During Extent Reports Setup from Base Class " + e.getMessage());
		}
		return extent;
	}

	public static ExtentTest createTest(String name) {
		test = extent.createTest(name);
		return test;
	}

	public static void createNode(ExtentTest Test, String NodeText) throws Exception {
		logInfo = test.createNode(new GherkinKeyword(": "), NodeText);

	}

	public static void writePassResult(String PassText) {
		try {

			test.log(Status.PASS, PassText);
			createNode(test, PassText);
		} catch (Exception e) {
			// Log.error("Error Occured During Writing Pass Result from Base Class");
		}
	}

	public static void writeFailResult(String text) {
		try {
			test.log(Status.FAIL, text);
			createNode(test, text);
			reportEnd();
		} catch (Exception e) {
			// Log.error("Error Occured During Writing Failed Result from Base Class");
		}
	}

	public static void reportEnd() {
		try {
			extent.flush();
		} catch (Exception e) {
			// Log.error("Error Occured During Extent Flush from Base Class");
		}

	}

	public static Platform getCurrentPlatform() {
		if (platform == null) {
			String operSys = System.getProperty("os.name").toLowerCase();
			if (operSys.contains("win")) {
				platform = Platform.WINDOWS;
			} else if (operSys.contains("nix") || operSys.contains("nux") || operSys.contains("aix")) {
				platform = Platform.LINUX;
			} else if (operSys.contains("mac")) {
				platform = Platform.MAC;
			}
		}
		return platform;
	}

	public static String getReportFileLocation(Platform platform) {
		String reportLocation = null;
		switch (platform) {
		case MAC:
			reportLocation = macPath;
			// Log.info("ExtentReport Path for MAC: " + macPath + "\n");
			break;
		case WINDOWS:
			reportLocation = windowsPath;
			// Log.info("ExtentReport Path for WINDOWS: " + windowsPath + "\n");
			break;
		default:
			Log.info("ExtentReport path has not been set! There is a problem!\n");
			break;
		}
		return reportLocation;
	}

	public AppiumDriver<MobileElement> getDriver() {
		return driver;
	}

	public void consoleLog(String message){
		Log.info("Message: " + message);
		Reporter.log(message);
	}
}
