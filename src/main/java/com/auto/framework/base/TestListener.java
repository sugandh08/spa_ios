package com.auto.framework.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;

public class TestListener extends BaseTest implements ITestListener {

	// Extent Report Declarations
	private static ExtentReports extent = ExtentManager.createInstance();
	ExtentManager em = new ExtentManager();
	private static ThreadLocal<ExtentTest> test = new ThreadLocal<>();
	private static Platform platform;

	@Override
	public synchronized void onStart(ITestContext context) {
		Log.info("=========== Test Suite started :- " + context.getName() + "===============");
		// System.out.println("Extent Reports Version 3 Test Suite started!");
	}

	@Override
	public synchronized void onFinish(ITestContext context) {
		Log.info("=========== Test Suite is ending :-  " + context.getName() + "===============");
		// System.out.println(("Extent Reports Version 3 Test Suite is
		// ending!"));
		extent.flush();
	}

	@Override
	public synchronized void onTestStart(ITestResult result) {
		Log.info(result.getMethod().getMethodName() + " Started");
		Log.info(result.getMethod().getDescription());
		ExtentTest extentTest = extent.createTest(result.getMethod().getMethodName(),
				result.getMethod().getDescription());
		test.set(extentTest);
	}

	@Override
	public synchronized void onTestSuccess(ITestResult result) {
		Log.info(result.getMethod().getMethodName() + " Passed");
		// test.get().pass("Test passed");
		test.get().log(Status.PASS, result.getTestClass().getName() + "." + result.getName() + " - PASS");
	}

	@SuppressWarnings("rawtypes")
	@Override
	public synchronized void onTestFailure(ITestResult result) {
		try {
			BaseTest.writeFailResult("Failed because of - " + result.getMethod().getMethodName() + ":- "
					+ getStackTrace(result.getThrowable()));
		} catch (Exception e) {

		}
		// test.get().fail(result.getThrowable());
		// ITestContext context = result.getTestContext();

		Class clazz = result.getTestClass().getRealClass();
		Field field = null;
		AppiumDriver<?> driver = null;
		try {
			field = clazz.getDeclaredField("driver");

			field.setAccessible(true);
			driver = (AppiumDriver<?>) field.get(result.getInstance());
		} catch (NoSuchFieldException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String targetLocation = null;
		String testClassName = result.getTestClass().getName().trim();

		String testMethodName = result.getName().toString().trim();
		String screenShotName = testMethodName + java.time.LocalDateTime.now() + ".png";
		String fileSeperator = System.getProperty("file.separator");
		platform = ExtentManager.getCurrentPlatform();
		String reportsPath = ExtentManager.getReportLocation(platform) + fileSeperator + "screenshots";
		Log.info("Screen shots reports path - " + reportsPath);
		try {
			File file = new File(reportsPath + fileSeperator + testClassName);// Set
																				// screenshots
			if (!file.exists()) {
				if (file.mkdirs()) {
					Log.info("Directory: " + file.getAbsolutePath() + " is created!");
				} else {
					Log.info("Failed to create directory: " + file.getAbsolutePath());
				}

			}
			File screenshotFile = driver.getScreenshotAs(OutputType.FILE);
			// File screenshotFile = ((TakesScreenshot)
			// driver).getScreenshotAs(OutputType.FILE);
			targetLocation = reportsPath + fileSeperator + testClassName + fileSeperator + screenShotName;// define
																											// location
			File targetFile = new File(targetLocation);
			Log.info("Screen shot file location - " + screenshotFile.getAbsolutePath());
			Log.info("Target File location - " + targetFile.getAbsolutePath());
			// FileHandler.copy(screenshotFile, targetFile);
			FileUtils.copyFile(screenshotFile, targetFile);

		} catch (FileNotFoundException e) {
			Log.info("File not found exception occurred while taking screenshot " + e.getMessage());
		} catch (Exception e) {
			Log.info("An exception occurred while taking screenshot " + e.getCause());
		}

		// attach screenshots to report
		try {
			test.get().fail("Screenshot", MediaEntityBuilder.createScreenCaptureFromPath(targetLocation).build());
		} catch (IOException e) {
			Log.info("An exception occured while taking screenshot " + e.getCause());
		}
		test.get().log(Status.FAIL, result.getTestClass().getName() + "." + result.getName()
				+ " Failed with exception : " + getStackTrace(result.getThrowable()));

	}

	@Override
	public synchronized void onTestSkipped(ITestResult result) {
		Log.info(result.getMethod().getMethodName() + " Skipped!");
		// System.out.println((result.getMethod().getMethodName() + "
		// skipped!"));
		test.get().log(Status.SKIP, result.getTestClass().getName() + "." + result.getName() + " Skip with exception : "
				+ getStackTrace(result.getThrowable()));

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		Log.info("onTestFailedButWithinSuccessPercentage for " + result.getMethod().getMethodName() + " --> "
				+ result.getThrowable());
		test.get().log(Status.FAIL, result.getTestClass().getName() + "." + result.getName()
				+ " Failed with exception : " + getStackTrace(result.getThrowable()));
		// System.out.println(("onTestFailedButWithinSuccessPercentage for " +
		// result.getMethod().getMethodName()));
	}

	protected String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return sw.toString();
	}

	public static String getContextName() {
		return testFileName;
	}

}