package com.myh_genesis_spa.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public  interface BlueLinkAppDevice {
	
	/**
	 * Only for Android to scroll to text matches exactly
	 * @param text
	 */
	public  void scrollToExact(String text);
	
	/**
	 * Only for Android to scroll to text partially matched
	 * @param text
	 */
	public abstract void scrollTo(String text);
	
	/**
	 * Only for Ios to scroll down the screen
	 */
	public abstract void scrollDown();
	
	public abstract void swipeScrollWheelUp(String scrollWheelXpath, int entriesToScroll);
	
	public abstract void swipeScrollWheelDown(String scrollWheelXpath, int entriesToScroll);
	
	
	public abstract AppiumDriver<MobileElement> getDriver();
	public abstract AppiumDriverLocalService getServer();
	
	public abstract void sleepFor(long timeInMillis);
	
	public abstract void waitForElementToLoad(MobileElement element);
	
	public abstract void waitForElementToLoadWithProvidedTime(MobileElement element, int limit);
	
	public abstract boolean isElementDisplayed(MobileElement element);
	
	//public abstract void waitFluentlyForElementToLoad(String element, long waitTimeInSeconds, long pollingTimeInSeconds,String strLocater);
	
	public abstract void scrollByText(String text);

	public abstract void swipeDownScreen(int entries);

	public abstract void swipeUpScreen(int entries);
	

	public abstract void swipeup(int entries);

	public abstract void swipeLeft(int entriesToSwipe);

	public abstract void swipeRight(int entriesToSwipe);
	
	public abstract String getCurrentPlatform();

	

	

	
}
