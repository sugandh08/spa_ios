package com.myh_genesis_spa.utils;

import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;



import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.myh_genesis_spa.utils.LocateElements.locaterType;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class IosBlueLinkAppDevice implements BlueLinkAppDevice {

	private AppiumDriver<MobileElement> driver;
	private AppiumDriverLocalService server;
	private BlueLinkAppDevice appDevice;
	
	private Dimension size;

    private int yAxis;
    private int xAxis;

	int entries = 20;


    private int startPoint;
    private int endPoint;
    private int anchorPoint;

    private double startPercentage = 0.5;
    private double percentageModifier = 0.2;

    private double anchorPercentageShort = 0.2;
    private double anchorPercentageLong = 0.5;
	
	
	public IosBlueLinkAppDevice(IOSDriver<MobileElement> driver, AppiumDriverLocalService server) {
		this.driver = driver;
		this.server = server;
	}
	
	
	private void swipeAction(int numberOfSwipes, int xStart, int xEnd, int yStart, int yEnd, long waitTime) {
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);

        for(int i=0;i<numberOfSwipes;i++) {
            new TouchAction(driver)
                    .press(new PointOption().withCoordinates(xStart, yStart))
                    .waitAction()
                    .moveTo(new PointOption().withCoordinates(xEnd, yEnd))
                    .release()
                    .perform();
        }
    }
	
	public void swipeup(int numberOfSwipes) {
        // Move along y axis
        // Keep x value same
        startPoint = (int) (yAxis * startPercentage);
        endPoint = (int) (yAxis * (startPercentage - percentageModifier));
        anchorPoint = (int) (xAxis * anchorPercentageShort);

        swipeAction(numberOfSwipes, anchorPoint, anchorPoint, startPoint, endPoint);
    }
	
	private void swipeAction(int numberOfSwipes, int xStart, int xEnd, int yStart, int yEnd) {
        swipeAction(numberOfSwipes, xStart, xEnd, yStart, yEnd, 5);
    }


	@SuppressWarnings("rawtypes")
	public void scrollToExact(String text) {
		// TODO Auto-generated method stub
		TouchAction touchAction = new TouchAction(driver);
		ElementOption elementStart = ElementOption
				.element(driver.findElement(By.xpath("(//*[contains(@name,'" + text + "')])[" + 1 + "]")));
		touchAction.longPress(elementStart).release().perform();
	}

	public void scrollTo(String text) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("rawtypes")
	public void longPress(MobileElement element) {
		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress(PointOption.point(179, 183)).release().perform();

	}

	public AppiumDriver<MobileElement> getDriver() {
		return driver;
	}

	public AppiumDriverLocalService getServer() {
		return server;
	}

	public void sleepFor(long timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void waitForElementToLoad(MobileElement element) {
		try {
			this.waitFor(15).until(ExpectedConditions.visibilityOf(element));

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void waitForElementToLoadWithProvidedTime(MobileElement element, int limit) {
		try {
			this.waitFor(limit).until(ExpectedConditions.visibilityOf(element));

		} catch (Exception e) {
			e.getMessage();
		}
	}

	private WebDriverWait waitFor(int limit) {
		return new WebDriverWait(driver, limit);
	}

	public boolean isElementDisplayed(MobileElement element) {
		try {
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public void scrollDown() {
		// TODO
	}

	@SuppressWarnings("rawtypes")
	public void popUpTap(String element) {
		TouchAction touchAction = new TouchAction(driver);
		if (element.contains("Save")) {
			ElementOption elementStart = ElementOption
					.element(driver.findElement(By.xpath("(//*[contains(@name,'" + element + "')])[" + 3 + "]")));
			touchAction.longPress(elementStart).release().perform();
		} else {
			ElementOption elementStart = ElementOption
					.element(driver.findElement(By.xpath("(//*[contains(@name,'" + element + "')])[" + 1 + "]")));
			touchAction.longPress(elementStart).release().perform();
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void swipeScrollWheelUp(String scrollWheelXpath, int entriesToScroll) {
		Rectangle dimensions = driver.findElement(By.xpath(scrollWheelXpath)).getRect();
		int xCoordinate = dimensions.x + (int) (dimensions.width * 0.5);
		int yCoordinateStart = dimensions.y + (int) (dimensions.height * 0.04);
		int yCoordinateEnd = dimensions.y + (int) (dimensions.height * 0.80);
		PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
		PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
		WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));
		TouchAction touchAction = new TouchAction(driver);
		for (int i = 0; i < entriesToScroll; i++)
			touchAction.press(pointStart).waitAction(waitOptions).moveTo(pointEnd).release().perform();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void swipeScrollWheelDown(String scrollWheelXpath, int entriesToScroll) {
		Rectangle dimensions = driver.findElement(By.xpath(scrollWheelXpath)).getRect();
		int xCoordinate = dimensions.x + (int) (dimensions.width * 0.5);
		int yCoordinateStart = dimensions.y + (int) (dimensions.height * 0.84);
		int yCoordinateEnd = dimensions.y + (int) (dimensions.height * 0.05);
		PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
		PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
		WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));
		TouchAction touchAction = new TouchAction(driver);
		for (int i = 0; i < entriesToScroll; i++)
			touchAction.press(pointStart).waitAction(waitOptions).moveTo(pointEnd).release().perform();
	}
	
	
	@Override
	public void scrollByText(String text) {
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "down");
		scrollObject.put("value", text);
		driver.executeScript("mobile:scroll", scrollObject);

		/*
		 * int size = driver.findElements(By.xpath("(//*[contains(@name,'" + text +
		 * "')])[" + 1 + "]")).size(); PointOption pointStart = new
		 * PointOption().withCoordinates(144, 540); PointOption pointEnd = new
		 * PointOption().withCoordinates(144, 776); WaitOptions waitOptions = new
		 * WaitOptions().withDuration(Duration.ofMillis(1000)); TouchAction touchAction
		 * = new TouchAction(driver); while (size != 1) {
		 * touchAction.press(pointStart).waitAction(waitOptions).moveTo(pointEnd).
		 * release().perform(); }
		 */ 

	}
	
	public boolean getNotificationElement(String notificationType, int notificationIndex) {
		try {			
			MobileElement ele = driver.findElement(By.xpath(""));
			return appDevice.isElementDisplayed(ele);
		} catch (Exception e) {
			return false;
		}

	}


	public void startServer() {
		// TODO Auto-generated method stub
		System.out.println("Start Server sysout added");

	}

	@Override
	@SuppressWarnings("rawtypes")
	public void swipeDownScreen(int entries) {
		// Dimension dimensions = driver.manage().window().getSize();
		PointOption pointStart = new PointOption().withCoordinates(144, 540);
		PointOption pointEnd = new PointOption().withCoordinates(144, 776);
		WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));
		TouchAction touchAction = new TouchAction(driver);

		for (int i = 0; i < entries; i++) {
			touchAction.press(pointStart).waitAction(waitOptions).moveTo(pointEnd).release().perform();

		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void swipeUpScreen(int entries) {

		
		WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(3000));
		PointOption pointStart = new PointOption().withCoordinates(24, 132);
		PointOption pointEnd = new PointOption().withCoordinates(24,730 );

		TouchAction touchAction = new TouchAction(driver);
		for (int i = 0; i < entries; i++) {
			touchAction.press(pointEnd).waitAction(waitOptions).moveTo(pointStart).release().perform();
		}
	}
	
	@Override
	public void swipeLeft(int entriesToSwipe) {
		// TODO Auto-generated method stub

	}

	@Override
	public void swipeRight(int entriesToSwipe) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getCurrentPlatform() {
		return DeviceConfiguration.getInstance().getDevicePlatform().toString();
	}

	public void waitFluentlyForElementToLoad(String element, long waitTimeInSeconds, long pollingTimeInSeconds,
			String strLocater) {
		@SuppressWarnings("deprecation")
		Wait<AppiumDriver<MobileElement>> wait = new FluentWait<AppiumDriver<MobileElement>>(driver)
				.withTimeout(waitTimeInSeconds, TimeUnit.SECONDS).pollingEvery(pollingTimeInSeconds, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).ignoring(TimeoutException.class);

		LocateElements.locaterType lt = LocateElements.locaterType.valueOf(locaterType.class, strLocater.toUpperCase());
		switch (lt) {

		case XPATH:
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element)));

			break;

		case ID:
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element)));

			break;

		case TAGNAME:
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(element)));

			break;

		case CLASSNAME:
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(element)));

			break;

		case ACCESSIBILITY:

			wait.until(ExpectedConditions.visibilityOf(driver.findElementByAccessibilityId(element)));

			break;

		}

	}

}
