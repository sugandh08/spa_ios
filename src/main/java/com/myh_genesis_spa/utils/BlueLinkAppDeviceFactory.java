package com.myh_genesis_spa.utils;

import java.io.File;
import java.util.HashMap;

import javax.swing.JOptionPane;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.common.base.Strings;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

public class BlueLinkAppDeviceFactory {

	private static AppiumDriverLocalService server;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void startAppiumServer() {
		AppiumServiceBuilder serviceBuilder = new AppiumServiceBuilder();
		// Use any port, in case the default 4723 is already taken (maybe by another
		// Appium server)
		serviceBuilder.usingAnyFreePort();
		// Tell serviceBuilder where node is installed. Or set this path in an
		// environment variable named NODE_PATH
		serviceBuilder.usingDriverExecutable(new File("/usr/local/bin/node"));
		// serviceBuilder.withArgument(GeneralServerFlag.LOG_LEVEL, "warn");
		// Tell serviceBuilder where Appium is installed. Or set this path in an
		// environment variable named APPIUM_PATH
		serviceBuilder.withAppiumJS(new File("/usr/local/bin/appium"));
		// The XCUITest driver requires that a path to the Carthage binary is in the
		// PATH variable. I have this set for my shell, but the Java process does not
		// see it. It can be inserted here.
		HashMap<String, String> environment = new HashMap();
		environment.put("PATH", "/usr/local/bin:" + System.getenv("PATH"));
		serviceBuilder.withEnvironment(environment);

		server = AppiumDriverLocalService.buildService(serviceBuilder);
		if (!server.isRunning())
			server.start();
	}

	public static BlueLinkAppDevice create(DeviceConfiguration deviceConfiguration) {
		DeviceType type = deviceConfiguration.getDevicePlatform();

		// startAppiumServer();
		if (type == null) {
			String platformName = JOptionPane.showInputDialog("Please Provide Platforms: Android/Ios");
			if (platformName.toLowerCase().contains("android")) {
				type = DeviceType.Android;
			} else {
				if (!platformName.toLowerCase().contains("ios")) {
					throw new IllegalArgumentException("No Platform available for provided value");
				}
				type = DeviceType.Ios;
			}
		}
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability("forceMjsonwp", true);
		if (!Strings.isNullOrEmpty(deviceConfiguration.getDeviceUDID())) {
			desiredCapabilities.setCapability("udid", deviceConfiguration.getDeviceUDID());
		}
		desiredCapabilities.setCapability("noReset", false);
		desiredCapabilities.setCapability("app", deviceConfiguration.getAppPath());
		desiredCapabilities.setCapability("clearSystemFiles", true);
		desiredCapabilities.setCapability("deviceName", deviceConfiguration.getDeviceName());
		desiredCapabilities.setCapability("newCommandTimeout", 90000);
		switch (type) {
		case Android:
			if (!Strings.isNullOrEmpty(deviceConfiguration.getAndroidVirtualDevice())) {
				desiredCapabilities.setCapability("avd", deviceConfiguration.getAndroidVirtualDevice());
			}
			desiredCapabilities.setCapability("commandTimeout", 40000);
			desiredCapabilities.setCapability("dontStopAppOnReset", true);
			desiredCapabilities.setCapability("noSign", true);
			//desiredCapabilities.setCapability("autoLaunch", false);
			desiredCapabilities.setCapability("appPackage", (String) deviceConfiguration.getAppPackage());
			desiredCapabilities.setCapability("appWaitActivity", (String) deviceConfiguration.getAppActivity());
			AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(deviceConfiguration.getAppiumURL(),desiredCapabilities);
			driver.launchApp();
			return new AndroidBlueLinkAppDevice(driver, server);
		case Ios:
			desiredCapabilities.setCapability("automationName", "XCUITEST");
			desiredCapabilities.setCapability("appPushTimeout", 90000);
			desiredCapabilities.setCapability("showIOSLog", false);
			desiredCapabilities.setCapability("platformVersion", deviceConfiguration.getOSVersion());
			// desiredCapabilities.setCapability("session-override", true);
			desiredCapabilities.setCapability("bundleId", (String) deviceConfiguration.getAppPackage());
			// IOSDriver<MobileElement> ioSDriver = new
			// IOSDriver<MobileElement>(server.getUrl(),desiredCapabilities);
			IOSDriver<MobileElement> ioSDriver = new IOSDriver<MobileElement>(deviceConfiguration.getAppiumURL(),
					desiredCapabilities);
			// ioSDriver.launchApp();
			return new IosBlueLinkAppDevice(ioSDriver, server);
		default:
			return null;
		}
	}
}
