package com.myh_genesis_spa.utils;

import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Rectangle;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class AndroidBlueLinkAppDevice implements BlueLinkAppDevice {

	private AndroidDriver<MobileElement> driver;
	private AppiumDriverLocalService server;

	public AndroidBlueLinkAppDevice(AndroidDriver<MobileElement> driver, AppiumDriverLocalService server) {
		this.driver = driver;
		this.server = server;
	}

	public AppiumDriver<MobileElement> getDriver() {
		return driver;
	}

	public AppiumDriverLocalService getServer() {
		return server;
	}

	public void sleepFor(long timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void waitForElementToLoad(MobileElement element) {
		boolean flag = false;

		for (int i = 0; i <= 10; i++) {
			if (!flag) {
				try {
					sleepFor(2000);
					String isVisible = element.getAttribute("visible");
					flag = isVisible.equalsIgnoreCase("true") ? true : false;
					flag = true;
				} catch (Exception e) {
					sleepFor(1000);
					flag = false;
				}
			} else {
				break;
			}

		}
	}

	public void waitForElementToLoadWithProvidedTime(MobileElement element, int limit) {
		boolean flag = false;

		for (int i = 0; i <= limit; i++) {
			if (!flag) {
				try {
					sleepFor(2000);
					String isVisible = element.getAttribute("visible");
					flag = isVisible.equalsIgnoreCase("true") ? true : false;
					flag = true;
				} catch (Exception e) {
					sleepFor(1000);
					flag = false;
				}
			} else {
				break;
			}

		}
	}

	public boolean isElementDisplayed(MobileElement element) {
		try {
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

//	@Override
//	public void waitFluentlyForElementToLoad(String element, long waitTimeInSeconds, long pollingTimeInSeconds,
//			String strLocater) {
//		@SuppressWarnings("deprecation")
//		Wait<AppiumDriver<MobileElement>> wait = new FluentWait<AppiumDriver<MobileElement>>(driver)
//				.withTimeout(waitTimeInSeconds, TimeUnit.SECONDS).pollingEvery(pollingTimeInSeconds, TimeUnit.SECONDS)
//				.ignoring(NoSuchElementException.class).ignoring(TimeoutException.class);
//
//		LocateElements.locaterType lt = LocateElements.locaterType.valueOf(locaterType.class, strLocater.toUpperCase());
//		switch (lt) {
//
//		case XPATH:
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element)));
//
//			break;
//
//		case ID:
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element)));
//
//			break;
//
//		case TAGNAME:
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(element)));
//
//			break;
//
//		case CLASSNAME:
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(element)));
//
//			break;
//
//		case ACCESSIBILITY:
//
//			wait.until(ExpectedConditions.visibilityOf(driver.findElementByAccessibilityId(element)));
//
//			break;
//
//		}
//
//	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void scrollByText(String text) {
		// scroll to item
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap scrollObject = new HashMap<>();
		scrollObject.put("predicateString", "name == '" + text + "'");
		// .put("direction", "down");
		js.executeScript("mobile: scroll", scrollObject);

	}

	@Override
	public void scrollToExact(String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scrollTo(String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scrollDown() {
		// TODO Auto-generated method stub

	}

	@Override
	public void swipeScrollWheelUp(String scrollWheelXpath, int entriesToScroll) {
		// TODO Auto-generated method stub

	}

	@Override
	public void swipeScrollWheelDown(String scrollWheelXpath, int entriesToScroll) {
		// TODO Auto-generated method stub

	}

	@Override
	public void swipeDownScreen(int entries) {
		// TODO Auto-generated method stub

	}

	@Override
	@SuppressWarnings("rawtypes")
	public void swipeUpScreen(int entries) {

		
		WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(3000));
		PointOption pointStart = new PointOption().withCoordinates(24, 132);
		PointOption pointEnd = new PointOption().withCoordinates(24,730 );

		TouchAction touchAction = new TouchAction(driver);
		for (int i = 0; i < entries; i++) {
			touchAction.press(pointEnd).waitAction(waitOptions).moveTo(pointStart).release().perform();
		}
	}

	@Override
	public void swipeup(int entries) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void swipeLeft(int entriesToSwipe) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void swipeRight(int entriesToSwipe) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getCurrentPlatform() {
		return DeviceConfiguration.getInstance().getDevicePlatform().toString();
	}
		
	}


