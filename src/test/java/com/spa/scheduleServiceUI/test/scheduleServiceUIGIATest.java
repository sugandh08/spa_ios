package com.spa.scheduleServiceUI.test;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class scheduleServiceUIGIATest extends BaseTest {

	String strEmail = "genesis31@mailinator.com";
	String strPassword = "test12345";
	String strVin = "KMTFN4JE3LT301816";

	String strSNo, strDealerCd, strZipCode, strDealerNm, strVendorName = null;

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MapPage_GIA mapPage;

	String sheetName = null;
	String strStatus = "Execution Not complete";
	String actualDealerName = null;
	String actualDealerName_SchedulePage = "";
	String strComments = "";
	String errorMessageOrSchedulePageType = "";
	String ServiceValetPage = "No";

	int serialColNo = 0;
	int dealerCodeColNo = 1;
	int dealerNameColNo = 2;
	int vendorNameColNo = 3;
	int zipCodeColNo = 4;
	int actualDealerNameColNo = 5;
	int actualDealerName_SchedulePageColNo = 6;
	int statusColNo = 7;
	int executionTimeColNo = 8;
	int commentsColNo = 9;
	int errorMessageCol = 10;
	int serviceValetPage = 11;

	@Factory(dataProvider = "userDataProvider")
	public scheduleServiceUIGIATest(String strSNo, String strDealerCd, String strZipCode, String strDealerNm,
			String strVendorName) {
		this.strSNo = strSNo;
		this.strDealerCd = strDealerCd;
		this.strZipCode = strZipCode;
		// System.out.println("Zip Code " + strZipCode);
		this.strDealerNm = strDealerNm;
		this.strVendorName = strVendorName;
	}

	@DataProvider(name = "userDataProvider")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTabArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/DealerOnboarding_20200407.xlsx",
				"Sheet3");
	}

	@BeforeSuite(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
	}

	public void relaunchAppAndInitializedVariable() throws Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
	}

	public void loginConditions() throws Exception {
		try {
			System.out.println("Logged In using : " + strEmail);

			if (!dashboardPage.isUnlockButtonDisplayWithinExpectedTime(1)) {
				loginPage.validLogin(strEmail, strPassword, strVin);
			} else {
				System.out.println("Already Login with " + strEmail);
			}
		} catch (NoSuchElementException ex) {
			// System.out.println("Logged In Again Due To No Such Element Error : " +
			// strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		} catch (WebDriverException e) {
			// System.out.println("Logged In Again Due To Wed Driver Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		}

	}

	@Test(description = "Search dealer POI by passing zipcode as search parameter and set poi as prefferd location and then perform send to car")
	public void testSearchDealerPOI() throws Exception

	{
		// Fetching integer from dealer code for Hyundai and Genesis URL selection
		// int dealerCdLength = strDealerCd.length();
		// int dealerCdLastLength = dealerCdLength - 3;
		// String codeDealer = strDealerCd.substring(dealerCdLastLength);
		actualDealerName = "";
		// System.out.println("Integer Value Of DealerCd " + codeDealer);
		// Compare Dealer Code interger for comparison
		/*
		 * if (Integer.parseInt(codeDealer) < 701) { sheetName = "HYNSHEET"; } else {
		 * sheetName = "GenesisOutputStatus"; }
		 */
		sheetName = "GenesisAndroid";
		boolean flag2 = false;

		loginConditions();
		assertTrue(dashboardPage.isUnlockButtonDisplay());
		dashboardPage.navigateToMapOptionPage();
		mapPage.clickDealersLocatorOption();
		System.out.println("Zip Code " + strZipCode);

		if (mapPage.isSearchBarDisplay()) {
			mapPage.enterSearchText(strZipCode);
			mapPage.clickUpArrow();
			actualDealerName = mapPage.getDealerName();
			System.out.println(actualDealerName);
		}

		if (!actualDealerName.equalsIgnoreCase(strDealerNm)) {
			strComments = "Dealer Name From Dealer Detail Not Matched";
		}

		mapPage.clickBtnScheduleService();

		if (mapPage.isScheduleValetPickUpDisplay()) {
			ServiceValetPage = "YES";
			mapPage.clickScheduleDropoff();
		}

		Thread.sleep(2000);

		if (mapPage.isPageScheduleServiceDisplay()) {
			System.out.println("if condition started");
			flag2 = mapPage.isScreenScheduleServicePage2Displayed();
			// flag1 = mapPage.isScheduleservicePageVisible();
			if (flag2) {
				System.out.println("flag 2 started");
				strStatus = "Pass";
				actualDealerName_SchedulePage = mapPage.getScheduleServiceNameFromScheduleServicePage();

				System.out.println(actualDealerName_SchedulePage);

				// System.out.println("Schedule service Page Visible With" +
				// actualDealerName_SchedulePage);

				if (actualDealerName_SchedulePage.contains("SCHEDULE A SERVICE CHECK IN")
						|| actualDealerName_SchedulePage.contains("SCHEDULE A SERVICE APPOINTMENT")
						|| actualDealerName_SchedulePage.contains("West Broad Hyundai")) {
					errorMessageOrSchedulePageType = actualDealerName_SchedulePage + " Page Is Displayed";
					actualDealerName_SchedulePage = "";
					strStatus = "Conditional Pass";
					System.out.println(errorMessageOrSchedulePageType);
				}

				else if (!actualDealerName_SchedulePage.toLowerCase().contains(strDealerNm.toLowerCase())) {
					strComments = strComments + " Dealer Name From Schedule Page Not Matched";
				}

			} else {
				strStatus = "Fail";
				if (mapPage.isErrorScheduleServicePageDisplayed()) {
					errorMessageOrSchedulePageType = mapPage.getNetworkIssueText();
				}
			}

			mapPage.clickOnBackButton();
			Thread.sleep(1000);
			mapPage.clickOnBackButton();

			System.out.print(flag2);
			Assert.assertTrue(flag2, "Schedule Service Page not displayed");
		} else {
			strStatus = "SLT Vendor Not Assigned";
			mapPage.clickonCancelButton();
			mapPage.clickOnBackButton();

		}
		mapPage.clickOnBackButton();
		assertTrue(dashboardPage.isUnlockButtonDisplay());
	}

	@AfterMethod
	public void WriteExcel() throws Exception {

		System.out.println("TestComplete");

		String strFilePath = System.getProperty("user.dir")
				+ "/src/test/resources/TestingData/DealerOnboarding_20200407.xlsx";

		ExcelReader excelReader = new ExcelReader();
		System.out.println(strSNo);
		int rowNum = Integer.parseInt(strSNo.replace(".0", ""));

		System.out.println(strFilePath);
		System.out.println(rowNum);
		System.out.println(sheetName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, serialColNo, String.valueOf(rowNum));
		excelReader.writeExcel(strFilePath, sheetName, rowNum, dealerCodeColNo, strDealerCd);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, dealerNameColNo, strDealerNm);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, vendorNameColNo, strVendorName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, zipCodeColNo, strZipCode.replace(".0", ""));
		excelReader.writeExcel(strFilePath, sheetName, rowNum, actualDealerNameColNo, actualDealerName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, actualDealerName_SchedulePageColNo,
				actualDealerName_SchedulePage);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, statusColNo, strStatus);

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		System.out.println(formatter.format(date));
		excelReader.writeExcel(strFilePath, sheetName, rowNum, executionTimeColNo, formatter.format(date).toString());
		excelReader.writeExcel(strFilePath, sheetName, rowNum, commentsColNo, strComments);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, errorMessageCol, errorMessageOrSchedulePageType);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, serviceValetPage, ServiceValetPage);
		excelReader.closeWorkBook();

	}

}
