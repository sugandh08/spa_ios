package com.spa.api.tests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.myh_genesis_spa.utils.ExcelReader;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class ApiTest {
	String status = "Fail";

	int dealerCodeColNo = 0;  
	int dealerNameColNo = 1; 
	int vendorIdColNo = 2;
	int vendorNameColNo = 3;
	int zipCodeColNo = 4;
	int scheduleConsumerUrlColNo = 5;
	int statusColNo = 6;
	int executionTimeColNo = 7;
	int commentsColNo = 8;

	List<String> listDealerCode, listDealerName, listVendorId, listVendorName, listconsumerUrl, listZipCode = null;
	String strDealerCode, strDealerName, strVendorId, strVendorName, strconsumerUrl, strComments = null;
	String strSNo, strDealerCd, strZipCode, strDealerNm = null;
	String sheetName = null;

	@Factory(dataProvider = "userDataProvider")
	public ApiTest(String strSNo, String strDealerCd, String strZipCode, String strDealerNm) {

		this.strSNo = strSNo;
		this.strDealerCd = strDealerCd;
		this.strZipCode = strZipCode;
		this.strDealerNm = strDealerNm;
	}

	@DataProvider(name = "userDataProvider")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTabArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/ScheduleServiceUIStatus.xlsx",
				"GenInput");
	}

	@Test(description = "Verify Hyundai API Response")
	public void testHyundaiResponseCode() throws Exception {
		
		// Intializing the variables
		strDealerCode = null;
		strDealerName = null;
		strVendorId = null;
		strVendorName = null;
		strconsumerUrl = null;
		strComments = null;
		int code = 0;
		String URL = null;
		String consumerUrl = null;

		System.out.println("Start TESTING");
		System.out.println(" Row No. " + strSNo);
		System.out.println(strDealerCd);
		System.out.println(strZipCode);

		// Fetching integer from dealer code for Hyundai and Genesis URL selection
		int dealerCdLength = strDealerCd.length();
		int dealerCdLastLength = dealerCdLength - 3;
		String codeDealer = strDealerCd.substring(dealerCdLastLength);
		System.out.println("Integer Value Of DealerCd " + codeDealer);

		// Append 0 in the zipcode if zipcode length is less than 5
		String zipCode = strZipCode.replace(".0", "");
		if (zipCode.length() < 5) {
			zipCode = 0 + zipCode;
		}

		// Compare Dealer Code interger for comparison
		if (Integer.parseInt(codeDealer) < 701) {
			sheetName = "HyundaiOutputStatus";
			//sheetName = "HyOut1";
			consumerUrl = "dealers.scheduleConsumerUrl";
			URL = "https://sapp.services.hma.us/SharedServices/hyundai/all/dealer/2019/service/v2/byZip?lang=en-us&zip="
					+ zipCode + "&maxdealers=50";
			System.out.println("Hyundai URL");
			System.out.println(URL);

		} else {
			sheetName = "GenesisOutputStatus";
			//sheetName = "GenOut1";
			consumerUrl = "dealers.scheduleGConsumerUrl";
			URL = "https://sapp.services.hma.us/SharedServices/genesis/G80/dealer/2018/service/v2/byZip?lang=en-us&zip="
					+ zipCode + "&maxdealers=50";
			System.out.println("Gensis URL");
			System.out.println(URL);

		}

		// Fetching response of URL
		RestAssured.useRelaxedHTTPSValidation();
		Response jsonResponse = RestAssured.get(URL);
		JsonPath jsonPathEvaluator = jsonResponse.jsonPath();

		listDealerCode = Arrays.asList(jsonPathEvaluator.get("dealers.dealerCd").toString().replace("[", "")
				.replace("]", "").replace(" ", "").split(","));
		listDealerName = Arrays.asList(
				jsonPathEvaluator.get("dealers.dealerNm").toString().replace("[", "").replace("]", "").split(","));
		listVendorId = Arrays.asList(
				jsonPathEvaluator.get("dealers.vendorId").toString().replace("[", "").replace("]", "").split(","));
		listVendorName = Arrays.asList(
				jsonPathEvaluator.get("dealers.vendorName").toString().replace("[", "").replace("]", "").split(","));
		listZipCode = Arrays.asList(jsonPathEvaluator.get("dealers.zipCd").toString().replace("[", "").replace("]", "")
				.replace(" ", "").split(","));
		listconsumerUrl = Arrays.asList(jsonPathEvaluator.get(consumerUrl).toString().replace("[", "").replace("]", "")
				.replace("-pt1", "").split(","));

		System.out.println(listDealerCode);
		System.out.println(listZipCode);

		int indexOfDealerCode = listDealerCode.indexOf(strDealerCd);
		System.out.println(indexOfDealerCode);

		// Logic for dealer code i.e. if dealer code is present inder the response we
		// get
		if (indexOfDealerCode != -1) {

			System.out.println(listZipCode.get(indexOfDealerCode));
			String indexZipCode = listZipCode.get(indexOfDealerCode).trim();

			if (indexZipCode.equals(zipCode)) {
				System.out.println("Zip Code Conditions is True");
				strDealerName = listDealerName.get(indexOfDealerCode).trim();
				strDealerCode = listDealerCode.get(indexOfDealerCode).trim();
				strVendorId = listVendorId.get(indexOfDealerCode).trim();
				strVendorName = listVendorName.get(indexOfDealerCode).trim();
				strconsumerUrl = listconsumerUrl.get(indexOfDealerCode).trim();
				System.out.println(strconsumerUrl);

				// Fetching response code for consumer url
				if (!strconsumerUrl.equals("") && strconsumerUrl != null) {
					System.out.println("Consumer URL is not null");
					Response ScheduleConsumerUrlresp = RestAssured.get(strconsumerUrl);
					code = ScheduleConsumerUrlresp.getStatusCode();
					
					System.out.println("Status Code is: " + code);
					if (code == 200) {
						status = "Pass";
					}
				} else {
					System.out.println("Consumer URL is null");
					status = "NA";
					strComments = "Consumer URL not present";
				}

			} else {
				System.out.println("Zip Code Conditions is False");
				strDealerCode = strDealerCd;
				strDealerName = strDealerNm;
				status = "ZipCode Not Matched";
				strComments = "Dealer Code  Display In Column A Present in: "+indexZipCode;
			}

		} else {
			System.out.println("Dealer Code Not Present In The Response");
			strDealerCode = strDealerCd;
			strDealerName = strDealerNm;
			status = "Dealer Code Not Present";
			strComments = "Mentioned Dealer Code Not Present In The Response";
		}

		Assert.assertEquals(code, 200);
	}

	@AfterMethod
	public void WriteExcel() throws Exception {
		System.out.println("TestComplete");

		String strFilePath = System.getProperty("user.dir")
				+ "/src/test/resources/TestingData/DealerOnboarding_20200407.xlsx";

		ExcelReader excelReader = new ExcelReader();
		System.out.println(strSNo);
		int rowNum = Integer.parseInt(strSNo.replace(".0", ""));

		System.out.println(strFilePath);
		System.out.println(rowNum);
		System.out.println(sheetName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, dealerCodeColNo, strDealerCode);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, dealerNameColNo, strDealerName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, vendorIdColNo, strVendorId);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, vendorNameColNo, strVendorName);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, zipCodeColNo, strZipCode.replace(".0", ""));
		excelReader.writeExcel(strFilePath, sheetName, rowNum, scheduleConsumerUrlColNo, strconsumerUrl);
		excelReader.writeExcel(strFilePath, sheetName, rowNum, statusColNo, status);

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		System.out.println(formatter.format(date));
		excelReader.writeExcel(strFilePath, sheetName, rowNum, executionTimeColNo, formatter.format(date).toString());
		excelReader.writeExcel(strFilePath, sheetName, rowNum, commentsColNo, strComments);
		excelReader.closeWorkBook();

	}

}
