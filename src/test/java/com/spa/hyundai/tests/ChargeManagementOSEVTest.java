package com.spa.hyundai.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.ChargeManagementPage;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.OffPeakChargingSettingsPage;
import com.spa.hyundai.pageObjects.ScheduledClimateSettingsPage;
import com.spa.hyundai.pageObjects.StartDrivingSettingsPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ChargeManagementOSEVTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static DashboardPage dashboardPage;
	private static ChargeManagementPage chargeManagementPage;
	private static StartDrivingSettingsPage startDrivingSettingsPage;
	private static ScheduledClimateSettingsPage scheduledClimateSettingsPage;
	private static OffPeakChargingSettingsPage offPeakChargingSettingsPage;
	private static LoginPage loginPage;

	

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage(empAppDevice);
		chargeManagementPage = new ChargeManagementPage(empAppDevice);
		startDrivingSettingsPage = new StartDrivingSettingsPage(empAppDevice);
		scheduledClimateSettingsPage=new ScheduledClimateSettingsPage(empAppDevice);
		offPeakChargingSettingsPage=new OffPeakChargingSettingsPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
	}

	/*****************************************************************************************
	 * @Description : Set Up Charge Management Schedule and go back without
	 *              editing/saving schedule
	 * @Steps : Login and go to the Charge Management screen. Go back to the
	 *        dashboard without saving any schedule.
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException 
	 *
	 * @Date : 09-July-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule")
	public void chargeManagementExitWithoutSaving() throws IOException {
//		ExcelReader _excelReader = new ExcelReader("./src//test//resources//TestingData//TestingDataInventory.xlsx",
//				"LoginCredentials");

//		String _userName = _excelReader.getValue(2, 1);
//		String _password = _excelReader.getValue(2, 2);
//		String _vin=_excelReader.getValue(2, 3);
		
		String _userName = "hata412@mailinator.com";
		String _password = "test1234";
		String _vin = "KMHE54L28JA067125";
		loginPage.validLogin(_userName, _password, _vin);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();

		dashboardPage.navigateToChargeManagementScreen();

		chargeManagementPage.isSetScheduleTitleDisplay();
		chargeManagementPage.navigateBackToDashboard();
		Assert.assertTrue(dashboardPage.isbtnRefreshVehicleStatusDisplay(), "Electric Vehicle Dashboard is not present");

	}

	/*****************************************************************************************
	 * @Description : Set Up 2 Start Driving schedules
	 * @Steps : Login and go to the Charge Management screen. Go to Start
	 *        Driving Settings Page and set both the schedules without off Peak
	 *        Charging and climate schedule
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException 
	 *
	 * @Date : 10-July-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Set Up 2 Start Driving schedules")
	public void create2StartDrivingSchedulesWithNoClimateScheduleNoOffPeak() throws IOException {
//		ExcelReader _excelReader = new ExcelReader("./src//test//resources//TestingData//TestingDataInventory.xlsx",
//				"LoginCredentials");
//
//		String _userName = _excelReader.getValue(2, 1);
//		String _password = _excelReader.getValue(2, 2);
//		String _vin=_excelReader.getValue(2, 3);
		String _userName = "hata412@mailinator.com";
		String _password = "test1234";
		String _vin = "KMHE54L28JA067125";

		String REMOTE_COMMAND = "SET ELECTRIC CHARGE SCHEDULE";
		String[] days1 = { "sunday", "monday" };
		String[] days2 = { "saturday", "wednesday" };
		loginPage.validLogin(_userName, _password, _vin);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();

		dashboardPage.navigateToChargeManagementScreen();

		chargeManagementPage.isSetScheduleTitleDisplay();
		chargeManagementPage.navigateToStartDrivingSettingsPage();

		startDrivingSettingsPage.isPresent();
		startDrivingSettingsPage.setStartDriving1Toggle("1");
		startDrivingSettingsPage.setStartDriving1States(days1);
		startDrivingSettingsPage.setStartDriving2States(days2);
		startDrivingSettingsPage.clickOnSavebutton();
		chargeManagementPage.setClimateToggle("0");
		chargeManagementPage.setOffPeakChargingToggle("0");

		chargeManagementPage.clickSubmitButton();

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getRemoteSuccessMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("processed successfully"),
				"The Remote Set Schedule command was not successful");

	}

	/*****************************************************************************************
	 * @Description : Set Up 1 Start Driving schedule with climate schedule and
	 *              off Peak Charging
	 * @Steps : Login and go to the Charge Management screen. Go to Start
	 *        Driving Settings Page and set 1start Driving schedules with off
	 *        Peak Charging and climate schedule
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException 
	 *
	 * @Date : 10-July-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Set Up 1 Start Driving schedule with climate schedule and off Peak Charging")
	public void create1StartDrivingScheduleWithClimateScheduleAndOffPeakCharging() throws IOException {
//		ExcelReader _excelReader = new ExcelReader("./src//test//resources//TestingData//TestingDataInventory.xlsx",
//				"LoginCredentials");
//
//		String _userName = _excelReader.getValue(2, 1);
//		String _password = _excelReader.getValue(2, 2);
//		String _vin=_excelReader.getValue(2, 3);
		
		String _userName = "hata412@mailinator.com";
		String _password = "test1234";
		String _vin = "KMHE54L28JA067125";

		String REMOTE_COMMAND = "SET ELECTRIC CHARGE SCHEDULE";
		String[] days1 = { "thursday", "monday" };

		loginPage.validLogin(_userName, _password, _vin);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();

		dashboardPage.navigateToChargeManagementScreen();

		chargeManagementPage.isSetScheduleTitleDisplay();
		chargeManagementPage.navigateToStartDrivingSettingsPage();

		startDrivingSettingsPage.isPresent();
		startDrivingSettingsPage.setStartDriving1Toggle("1");
		startDrivingSettingsPage.setStartDriving1States(days1);

		startDrivingSettingsPage.clickOnSavebutton();
		chargeManagementPage.setClimateToggle("1");
		chargeManagementPage.navigateToScheduledClimateSettingsPage();
		scheduledClimateSettingsPage.isPresent();
		scheduledClimateSettingsPage.setTemperature();
		scheduledClimateSettingsPage.setFrontDefrostToggle("1");
		scheduledClimateSettingsPage.clickOnSavebutton();
		
		chargeManagementPage.setOffPeakChargingToggle("1");
		chargeManagementPage.navigateToOffPeakChargingSettingsPage();
		offPeakChargingSettingsPage.isPresent();
		offPeakChargingSettingsPage.setOffPeakChargingTimeStates();
		offPeakChargingSettingsPage.clickOffPeakChargingOnlyRadioButton();
		offPeakChargingSettingsPage.clickOnSavebutton();
		

		chargeManagementPage.clickSubmitButton();

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getRemoteSuccessMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("processed successfully"),
				"The Remote Set Schedule command was not successful");

	} 

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
