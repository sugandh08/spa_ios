package com.spa.hyundai.tests;

import java.io.IOException;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.AboutAndSupportPage;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.DealerLocatorPage;
import com.spa.hyundai.pageObjects.EnterPinPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.MapPage;
import com.spa.hyundai.pageObjects.MenuPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class AboutAndSupportTest extends BaseTest {
	private String strVehicleType, strTestRun, strEmail, strPassword, strPin, strVin = "";
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;

	private static AboutAndSupportPage aboutAndSupportPage;
	private static LoginPage loginPage;
	private static MenuPage menuPage;
	private static DashboardPage dashboardPage;

	@Factory(dataProvider = "TestLoginData")
	public AboutAndSupportTest(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVin) {

		this.strVehicleType = strVehicleType;
		this.strTestRun = strTestRun;
		this.strEmail = strEmail;
		this.strPassword = strPassword;
		this.strPin = strPin;
		this.strVin = strVin;

	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");
	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		aboutAndSupportPage = new AboutAndSupportPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		menuPage = new MenuPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
		System.out.println(strEmail);
	}

	public void relaunchAppAndInitializedVariable() throws Exception {
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		aboutAndSupportPage = new AboutAndSupportPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		menuPage = new MenuPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
		System.out.println(strEmail);
	}

	public void loginConditions() throws Exception {
		try {
			System.out.println("Logged In using : " + strEmail);
			if (!dashboardPage.isVehicleStatusButtonDisplayeWithinExpectedTime(2)) {
				loginPage.validLogin(strEmail, strPassword, strVin);
				dashboardPage.VehicleStatusButtonDisplayStatusWithoutHandelException();
				System.out.println(dashboardPage.VehicleStatusButtonDisplayStatusWithoutHandelException());
				// i++;
				System.out.println("Logged In Fisr Time With : " + strEmail);
			} else {
				System.out.println("Already Login with " + strEmail);
			}
		}

		catch (NoSuchElementException ex) {
			System.out.println("Logged In Again Due To No Such Element Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		} catch (WebDriverException e) {
			System.out.println("Logged In Again Due To Wed Driver Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		}

	}

	/*****************************************************************************************
	 * @Description : About And Support- Verify Blue Link Section
	 * @Steps : Login and navigate to About and Support page from hamburger menu.
	 *
	 * @author : Pushpa Kumari
	 * @throws Exception
	 * @throws IOException
	 *
	 * @Date : 07-Jan-2020
	 *
	 ******************************************************************************************/

	@Test(description = "About And Support- Verify Blue Link Section")
	public void testBlueLinkSection_AboutAndSupport() throws Exception {

		loginConditions();
		System.out.println("Logged In using : " + strEmail);

		menuPage.openMenu();
		menuPage.navigateToAboutAndSupport();

		aboutAndSupportPage.isAboutAndSupportPageDisplayed();

		// test Blue link information link
		aboutAndSupportPage.textLinkName("BLUE LINK", "Blue Link Information").isDisplayed();
		aboutAndSupportPage.textLinkName("BLUE LINK", "Blue Link Information").click();

		empAppDevice.sleepFor(3000);
		dashboardPage.clickOnPopupOKButton();
		aboutAndSupportPage.clickOnDontAllowCurrentLocation();

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Blue Link Info"), "Blue Link Info");

		aboutAndSupportPage.clickOnBackButton();

		// test Blue Link How-To Videos link
		aboutAndSupportPage.textLinkName("BLUE LINK", "Blue Link How-To Videos").isDisplayed();
		aboutAndSupportPage.textLinkName("BLUE LINK", "Blue Link How-To Videos").click();
		empAppDevice.sleepFor(2000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Blue Link How-To Videos"), "Blue Link How-To Videos");
		aboutAndSupportPage.clickOnBackButton();
	}

	/*****************************************************************************************
	 * @Description : About And Support- Verify Bluetooth Section
	 * @Steps : Login and navigate to About and Support page from hamburger menu.
	 *
	 * @author : Pushpa Kumari
	 * @throws Exception
	 * @throws IOException
	 *
	 * @Date : 07-Jan-2020
	 *
	 ******************************************************************************************/

	@Test(description = "About And Support- Verify Bluetooth Section")
	public void testBluetoothSection_AboutAndSupport() throws Exception {

		loginConditions();
		System.out.println("Logged In using : " + strEmail);

		menuPage.openMenu();
		menuPage.navigateToAboutAndSupport();

		aboutAndSupportPage.isAboutAndSupportPageDisplayed();

		// test Bluetooth & Multimedia Support link
		Assert.assertTrue(
				aboutAndSupportPage.textLinkName("BLUETOOTH", "Bluetooth & Multimedia Support").isDisplayed());

		// test Bluetooth Videos link
		aboutAndSupportPage.textLinkName("BLUETOOTH", "Bluetooth Videos").isDisplayed();
		aboutAndSupportPage.textLinkName("BLUETOOTH", "Bluetooth Videos").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Bluetooth"), "Bluetooth");
		aboutAndSupportPage.clickOnBackButton();
	}

	/*****************************************************************************************
	 * @Description : About And Support- Verify Guides Section
	 * @Steps : Login and navigate to About and Support page from hamburger menu.
	 *
	 * @author : Pushpa Kumari
	 * @throws Exception
	 * @throws IOException
	 *
	 * @Date : 07-Jan-2020
	 *
	 ******************************************************************************************/
	@Test(description = "About And Support- Verify Guides Section")
	public void testGuidesSection_AboutAndSupport() throws Exception {

		loginConditions();
		System.out.println("Logged In using : " + strEmail);

		menuPage.openMenu();
		menuPage.navigateToAboutAndSupport();

		aboutAndSupportPage.isAboutAndSupportPageDisplayed();

		// test Getting Started Guide link
		Assert.assertTrue(aboutAndSupportPage.textLinkName("GUIDES", "Getting Started Guide").isDisplayed());

		// test Indicator Guide link
		Assert.assertTrue(aboutAndSupportPage.textLinkName("GUIDES", "Indicator Guide").isDisplayed());

		// test Owner's Manual link
		// Assert.assertTrue(aboutAndSupportPage.textLinkName("GUIDES","Owner's
		// Manual").isDisplayed());

		// test Maintenance Information link
		Assert.assertTrue(aboutAndSupportPage.textLinkName("GUIDES", "Maintenance Information").isDisplayed());

		// test Warranty Info link
		Assert.assertTrue(aboutAndSupportPage.textLinkName("GUIDES", "Warranty Info").isDisplayed());
	}

	/*****************************************************************************************
	 * @Description : About And Support- Verify Hyundai Resources Section
	 * @Steps : Login and navigate to About and Support page from hamburger menu.
	 *
	 * @author : Pushpa Kumari
	 * @throws Exception
	 * @throws IOException
	 *
	 * @Date : 08-Jan-2020
	 *
	 ******************************************************************************************/
	@Test(description = "About And Support- Verify Hyundai Resources Section")
	public void testHyundaiResourcesSection_AboutAndSupport() throws Exception {

		loginConditions();
		System.out.println("Logged In using : " + strEmail);

		menuPage.openMenu();
		menuPage.navigateToAboutAndSupport();

		aboutAndSupportPage.isAboutAndSupportPageDisplayed();

		// empAppDevice.scrollTo("//XCUIElementTypeOther[@name='HYUNDAI RESOURCES']");

		// test Getting Started link
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Getting Started").isDisplayed();
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Getting Started").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Getting Started"), "Getting Started");
		aboutAndSupportPage.clickOnBackButton();

		// test Blue Link link
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Blue Link").isDisplayed();
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Blue Link").click();
		empAppDevice.sleepFor(3000);
		aboutAndSupportPage.clickOnDontAllowCurrentLocation();

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Blue Link"), "Blue Link");
		aboutAndSupportPage.clickOnBackButton();

		// test Vehicle Health link
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Vehicle Health").isDisplayed();
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Vehicle Health").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Vehicle Health"), "Vehicle Health");
		aboutAndSupportPage.clickOnBackButton();

		// test Manuals & Warranties link
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Manuals & Warranties").isDisplayed();
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Manuals & Warranties").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Manuals & Warranties"), "Manuals & Warranties");
		aboutAndSupportPage.clickOnBackButton();

		// test Bluetooth Assistance link
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Bluetooth Assistance").isDisplayed();
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Bluetooth Assistance").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Bluetooth Assistance"), "Bluetooth Assistance");
		aboutAndSupportPage.clickOnBackButton();

		// test Multimedia and Navigation link
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Multimedia and Navigation").isDisplayed();
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Multimedia and Navigation").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Multimedia and Navigation"),
				"Multimedia and Navigation");
		aboutAndSupportPage.clickOnBackButton();

		// test Vehicle Operation link
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Vehicle Operation").isDisplayed();
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "Vehicle Operation").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Vehicle Operation"), "Vehicle Operation");
		aboutAndSupportPage.clickOnBackButton();

		// test General Information link
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "General Information").isDisplayed();
		aboutAndSupportPage.textLinkName("HYUNDAI RESOURCES", "General Information").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("General Information"), "General Information");
		aboutAndSupportPage.clickOnBackButton();
	}

	/*****************************************************************************************
	 * @Description : About And Support- Verify LINKS Section
	 * @Steps : Login and navigate to About and Support page from hamburger menu.
	 *
	 * @author : Pushpa Kumari
	 * @throws Exception
	 * @throws IOException
	 *
	 * @Date : 08-Jan-2020
	 *
	 ******************************************************************************************/
	@Test(description = "About And Support- Verify LINKS Section")
	public void testLINKSSection_AboutAndSupport() throws Exception {

		loginConditions();
		System.out.println("Logged In using : " + strEmail);

		menuPage.openMenu();
		menuPage.navigateToAboutAndSupport();

		aboutAndSupportPage.isAboutAndSupportPageDisplayed();

		empAppDevice.scrollDown();

		// test MyHyundai.comlink
		aboutAndSupportPage.textLinkName("LINKS", "MyHyundai.com").isDisplayed();
		aboutAndSupportPage.textLinkName("LINKS", "MyHyundai.com").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("MyHyundai.com"), "MyHyundai.com");
		aboutAndSupportPage.clickOnBackButton();

		// test Hyundai USA link
		aboutAndSupportPage.textLinkName("LINKS", "Hyundai USA").isDisplayed();
		aboutAndSupportPage.textLinkName("LINKS", "Hyundai USA").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Hyundai USA"), "Hyundai USA");
		aboutAndSupportPage.clickOnBackButton();

		// test Hyundai Accessories link
		aboutAndSupportPage.textLinkName("LINKS", "Hyundai Accessories").isDisplayed();
		aboutAndSupportPage.textLinkName("LINKS", "Hyundai Accessories").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Accessories"), "Accessories");
		aboutAndSupportPage.clickOnBackButton();

		// test Hyundai Parts & Collision Info link
		aboutAndSupportPage.textLinkName("LINKS", "Hyundai Parts & Collision Info").isDisplayed();
		aboutAndSupportPage.textLinkName("LINKS", "Hyundai Parts & Collision Info").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Hyundai Parts & Collision Info"),
				"Hyundai Parts & Collision Info");
		aboutAndSupportPage.clickOnBackButton();

		// test Hyundai Hope On Wheels link
		aboutAndSupportPage.textLinkName("LINKS", "Hyundai Hope On Wheels").isDisplayed();
		aboutAndSupportPage.textLinkName("LINKS", "Hyundai Hope On Wheels").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Hyundai Hope On Wheels"), "Hyundai Hope On Wheels");
		aboutAndSupportPage.clickOnBackButton();
	}

	/*****************************************************************************************
	 * @Description : About And Support- Verify Contact Us Section
	 * @Steps : Login and navigate to About and Support page from hamburger menu.
	 *
	 * @author : Pushpa Kumari
	 * @throws Exception
	 * @throws IOException
	 *
	 * @Date : 08-Jan-2020
	 *
	 ******************************************************************************************/
	@Test(description = "About And Support- Verify Contact Us Section")
	public void testContactUsSection_AboutAndSupport() throws Exception {

		loginConditions();
		System.out.println("Logged In using : " + strEmail);

		menuPage.openMenu();
		menuPage.navigateToAboutAndSupport();

		aboutAndSupportPage.isAboutAndSupportPageDisplayed();

		empAppDevice.scrollDown();

		// test Email App Support link
		aboutAndSupportPage.textLinkName("CONTACT US", "Email App Support").isDisplayed();
		aboutAndSupportPage.textLinkName("CONTACT US", "Email App Support").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Email App Support"), "Email App Support");
		aboutAndSupportPage.clickOnBackButton();

		// test Consumer Assistance Center link
		aboutAndSupportPage.textLinkName("CONTACT US", "Consumer Assistance Center").isDisplayed();
		aboutAndSupportPage.textLinkName("CONTACT US", "Consumer Assistance Center").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Consumer Assistance Center"),
				"Consumer Assistance Center");
		aboutAndSupportPage.clickOnBackButton();
	}

	/*****************************************************************************************
	 * @Description : About And Support- Verify Contact Us Section
	 * @Steps : Login and navigate to About and Support page from hamburger menu.
	 *
	 * @author : Pushpa Kumari
	 * @throws Exception
	 * @throws IOException
	 *
	 * @Date : 08-Jan-2020
	 *
	 ******************************************************************************************/
	@Test(description = "About And Support- Verify Contact Us Section")
	public void testVersionSection_AboutAndSupport() throws Exception {

		loginConditions();
		System.out.println("Logged In using : " + strEmail);

		menuPage.openMenu();
		menuPage.navigateToAboutAndSupport();

		aboutAndSupportPage.isAboutAndSupportPageDisplayed();

		empAppDevice.scrollDown();
		empAppDevice.scrollDown();

		// test FAQ link
		aboutAndSupportPage.textLinkName("Version", "FAQ").isDisplayed();
		aboutAndSupportPage.textLinkName("Version", "FAQ").click();
		empAppDevice.sleepFor(2000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("FAQ"), "FAQ");
		aboutAndSupportPage.clickOnBackButton();

		// test TnC link
		aboutAndSupportPage.textLinkName("Version", "Terms & Conditions").isDisplayed();
		aboutAndSupportPage.textLinkName("Version", "Terms & Conditions").click();
		empAppDevice.sleepFor(3000);

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Terms & Conditions"), "Terms & Conditions");

		// test Privacy Policy link
		aboutAndSupportPage.textLinkName("Version", "Privacy Policy").isDisplayed();
		aboutAndSupportPage.textLinkName("Version", "Privacy Policy").click();
		empAppDevice.sleepFor(3000);
		dashboardPage.clickOnPopupOKButton();

		Assert.assertEquals(aboutAndSupportPage.getSubTitleText("Privacy Policy"), "Privacy Policy");
		aboutAndSupportPage.clickOnBackButton();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

	@AfterMethod
	public void afterMethod() {

		if (aboutAndSupportPage.isBackButtonDisplay()) {
			aboutAndSupportPage.clickOnBackButton();
		}

	}
}
