package com.spa.hyundai.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.DriverSettingPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.UserProfilePage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class UsreProfileTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static UserProfilePage userProfilePage;
	private static DriverSettingPage driverSettingPage;
	private static LoginPage loginPage;
	private static DashboardPage dashboardPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		userProfilePage = new UserProfilePage(empAppDevice);
		driverSettingPage = new DriverSettingPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
		
	}

	@Test(description = "Test User Profile")
	public void testUserProfile() {
		String strUserName = "oskona@yopmail.com";
		String strPassword = "test1234";
		String strVIN = "KM8K51A51JU000458";

		_userProfile(strUserName, strPassword, strVIN);
	}

	@Test(description = "Verify User profile page for DN8 primary user")
	public void testDNPrimaryUserProfile() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");
	}

	@Test(description = "Verify User profile page for DN8 Secondary user")
	public void testDNSecondryUserProfile() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");
		if (userProfilePage.isSecondaryDriverDisplay()) {
			Assert.assertTrue(userProfilePage.isSecondaryDriverDisplay(), "Secondry driver info is not displaying.");
			String strSecondaryDriverEmailID = userProfilePage.getSecondryDriverUserID();

			Assert.assertTrue(userProfilePage.isSecondaryDriverSetVehicleDisplay(strSecondaryDriverEmailID),
					"Set vehicle is not displayed.");
		}
	}

	@Test(description = "Verify DN8 My Driver Setting Page.")
	public void testMyDriverSettingPage() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");
	}

	@Test(description = "Verify driver settings for Date Or Time Setting Section when GPS setting is On, Day Light Saving is off and Hours is on")
	public void testDateOrTimeWithGPSAndHourOn() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");

		driverSettingPage.expandDateOrTimeSettingSection();

		if (!driverSettingPage.isGPSSwitchOn()) {
			driverSettingPage.clickGPSSwitch();
		}

		Assert.assertTrue(driverSettingPage.isGPSSwitchOn(), "GPS switch is not on");

		if (driverSettingPage.isDayLightSavingSwitchOn()) {
			driverSettingPage.clickDayLightSwitch();
		}

		Assert.assertFalse(driverSettingPage.isDayLightSavingSwitchOn(), "GPS switch is not off");

		if (!driverSettingPage.isHoursOnButtonSelected()) {
			driverSettingPage.clickHourOn();
		}

		Assert.assertTrue(driverSettingPage.isHoursOnButtonSelected(), "Hour button is not ON.");

		driverSettingPage.saveDriverSettings();

		Assert.assertTrue(driverSettingPage.isDriverSettingSuccessfullySavePopUpDisplay(),
				"Driver settings successfully saved is not displaying.");
		driverSettingPage.clickPopUpOk();

		Assert.assertTrue(driverSettingPage.isGPSSwitchOn(), "GPS switch is not on");
		Assert.assertFalse(driverSettingPage.isDayLightSavingSwitchOn(), "GPS switch is not off");
		Assert.assertTrue(driverSettingPage.isHoursOnButtonSelected(), "Hour button is not ON.");

	}

	@Test(description = "Verify driver settings for Date Or Time Setting Section when GPS setting is On, Day Light Saving is off and Hours is Off")
	public void testDateOrTimeWithGPSOnAndHourOff() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");

		driverSettingPage.expandDateOrTimeSettingSection();

		if (!driverSettingPage.isGPSSwitchOn()) {
			driverSettingPage.clickGPSSwitch();
		}

		Assert.assertTrue(driverSettingPage.isGPSSwitchOn(), "GPS switch is not on");

		if (driverSettingPage.isDayLightSavingSwitchOn()) {
			driverSettingPage.clickDayLightSwitch();
		}

		Assert.assertFalse(driverSettingPage.isDayLightSavingSwitchOn(), "Day Light saving switch is not off");

		if (!driverSettingPage.isHoursOffButtonSelected()) {
			driverSettingPage.clickHourOff();
		}

		Assert.assertTrue(driverSettingPage.isHoursOffButtonSelected(), "Hour button is not Off.");

		driverSettingPage.saveDriverSettings();

		Assert.assertTrue(driverSettingPage.isDriverSettingSuccessfullySavePopUpDisplay(),
				"Driver settings successfully saved is not displaying.");
		driverSettingPage.clickPopUpOk();

		Assert.assertTrue(driverSettingPage.isGPSSwitchOn(), "GPS switch is not on");
		Assert.assertFalse(driverSettingPage.isDayLightSavingSwitchOn(), "DayLight saving switch is not off");
		Assert.assertTrue(driverSettingPage.isHoursOffButtonSelected(), "Hour button is not Off.");

	}

	@Test(description = "Verify driver settings for Date Or Time Setting Section when GPS setting is Off, Day Light Saving is On and Hours is on")
	public void testDateOrTimeWithDayLightOnAndHourOn() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");

		driverSettingPage.expandDateOrTimeSettingSection();

		if (driverSettingPage.isGPSSwitchOn()) {
			driverSettingPage.clickGPSSwitch();
		}

		Assert.assertFalse(driverSettingPage.isGPSSwitchOn(), "GPS switch is On");

		if (!driverSettingPage.isDayLightSavingSwitchOn()) {
			driverSettingPage.clickDayLightSwitch();
		}

		Assert.assertTrue(driverSettingPage.isDayLightSavingSwitchOn(), "GPS switch is not off");

		if (!driverSettingPage.isHoursOnButtonSelected()) {
			driverSettingPage.clickHourOn();
		}

		Assert.assertTrue(driverSettingPage.isHoursOnButtonSelected(), "Hour button is not ON.");

		driverSettingPage.saveDriverSettings();

		Assert.assertTrue(driverSettingPage.isDriverSettingSuccessfullySavePopUpDisplay(),
				"Driver settings successfully saved is not displaying.");
		driverSettingPage.clickPopUpOk();

		Assert.assertFalse(driverSettingPage.isGPSSwitchOn(), "GPS switch is not on");
		Assert.assertTrue(driverSettingPage.isDayLightSavingSwitchOn(), "GPS switch is not off");
		Assert.assertTrue(driverSettingPage.isHoursOnButtonSelected(), "Hour button is not ON.");

	}

	@Test(description = "Verify driver settings for Date Or Time Setting Section when GPS setting is Off, Day Light Saving is On and Hours is Off")
	public void testDateOrTimeWithDayLightSavingOnAndHourOff() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");

		driverSettingPage.expandDateOrTimeSettingSection();

		if (driverSettingPage.isGPSSwitchOn()) {
			driverSettingPage.clickGPSSwitch();
		}

		Assert.assertFalse(driverSettingPage.isGPSSwitchOn(), "GPS switch is On");

		if (!driverSettingPage.isDayLightSavingSwitchOn()) {
			driverSettingPage.clickDayLightSwitch();
		}

		Assert.assertTrue(driverSettingPage.isDayLightSavingSwitchOn(), "Day Light saving switch is not off");

		if (!driverSettingPage.isHoursOffButtonSelected()) {
			driverSettingPage.clickHourOff();
		}

		Assert.assertTrue(driverSettingPage.isHoursOffButtonSelected(), "Hour button is not Off.");

		driverSettingPage.saveDriverSettings();

		Assert.assertTrue(driverSettingPage.isDriverSettingSuccessfullySavePopUpDisplay(),
				"Driver settings successfully saved is not displaying.");
		driverSettingPage.clickPopUpOk();

		Assert.assertFalse(driverSettingPage.isGPSSwitchOn(), "GPS switch is not on");
		Assert.assertTrue(driverSettingPage.isDayLightSavingSwitchOn(), "DayLight saving switch is not off");
		Assert.assertTrue(driverSettingPage.isHoursOffButtonSelected(), "Hour button is not Off.");

	}

	@Test(description = "Verify driver settings for Date Or Time Setting Section when GPS setting is On, Day Light Saving is On and Hours is Off")
	public void testDateOrTimeWithGPSTimrOnDayLightSavingOnAndHourOff() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");

		driverSettingPage.expandDateOrTimeSettingSection();

		if (!driverSettingPage.isGPSSwitchOn()) {
			driverSettingPage.clickGPSSwitch();
		}

		Assert.assertTrue(driverSettingPage.isGPSSwitchOn(), "GPS switch is not on");

		if (!driverSettingPage.isDayLightSavingSwitchOn()) {
			driverSettingPage.clickDayLightSwitch();
		}

		Assert.assertTrue(driverSettingPage.isDayLightSavingSwitchOn(), "Day Light saving switch is not off");

		if (!driverSettingPage.isHoursOffButtonSelected()) {
			driverSettingPage.clickHourOff();
		}

		Assert.assertTrue(driverSettingPage.isHoursOffButtonSelected(), "Hour button is not Off.");

		driverSettingPage.saveDriverSettings();

		Assert.assertTrue(driverSettingPage.isDriverSettingSuccessfullySavePopUpDisplay(),
				"Driver settings successfully saved is not displaying.");
		driverSettingPage.clickPopUpOk();

		Assert.assertTrue(driverSettingPage.isGPSSwitchOn(), "GPS switch is not on");
		Assert.assertTrue(driverSettingPage.isDayLightSavingSwitchOn(), "DayLight saving switch is not off");
		Assert.assertTrue(driverSettingPage.isHoursOffButtonSelected(), "Hour button is not Off.");

	}

	@Test(description = "Verify driver settings for Date Or Time Setting Section when GPS setting is On, Day Light Saving is On and Hours is On")
	public void testDateOrTimeWithGPSTimrOnDayLightSavingOnAndHourOn() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");

		driverSettingPage.expandDateOrTimeSettingSection();

		if (!driverSettingPage.isGPSSwitchOn()) {
			driverSettingPage.clickGPSSwitch();
		}

		Assert.assertTrue(driverSettingPage.isGPSSwitchOn(), "GPS switch is not Off");

		if (!driverSettingPage.isDayLightSavingSwitchOn()) {
			driverSettingPage.clickDayLightSwitch();
		}

		Assert.assertTrue(driverSettingPage.isDayLightSavingSwitchOn(), "Day Light saving switch is not off");

		if (!driverSettingPage.isHoursOnButtonSelected()) {
			driverSettingPage.clickHourOn();
		}

		Assert.assertTrue(driverSettingPage.isHoursOnButtonSelected(), "Hour button is not On.");

		driverSettingPage.saveDriverSettings();

		Assert.assertTrue(driverSettingPage.isDriverSettingSuccessfullySavePopUpDisplay(),
				"Driver settings successfully saved is not displaying.");
		driverSettingPage.clickPopUpOk();

		Assert.assertTrue(driverSettingPage.isGPSSwitchOn(), "GPS switch is not On");
		Assert.assertTrue(driverSettingPage.isDayLightSavingSwitchOn(), "DayLight saving switch is not On");
		Assert.assertTrue(driverSettingPage.isHoursOnButtonSelected(), "Hour button is not On.");
	}

	@Test(description = "Verify driver settings for kilometer on")
	public void testUnitSettingsKilometerOn() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");
		
		driverSettingPage.expandUnitSettings();
		
		if(!driverSettingPage.isUnitSettingsKilometerOn())
		{
			driverSettingPage.clickKilometerUnitSetting();
		}
		
		Assert.assertTrue(driverSettingPage.isUnitSettingsKilometerOn(), "Kilometer button is not selected");
		Assert.assertFalse(driverSettingPage.isUnitSettingsMileOn(), "Miles is selected.");
		
		driverSettingPage.saveDriverSettings();

		Assert.assertTrue(driverSettingPage.isDriverSettingSuccessfullySavePopUpDisplay(),
				"Driver settings successfully saved is not displaying.");
		driverSettingPage.clickPopUpOk();
		
		Assert.assertTrue(driverSettingPage.isUnitSettingsKilometerOn(), "Kilometer button is not selected");
		Assert.assertFalse(driverSettingPage.isUnitSettingsMileOn(), "Miles is selected.");
		
	}
	
	
	@Test(description = "Verify driver settings for miles On")
	public void testUnitSettingsMilesOn() {
		String strUserName = "hatadeveloper@hyundai-hata.com";
		String strPassword = "Hata@2015 ";
		String strVIN = "";

		_userProfile(strUserName, strPassword, strVIN);
		Assert.assertTrue(userProfilePage.isPrimaryDriverInfoDisplay(), "Primary driver info is not displaying.");

		userProfilePage.navigateToMyDriverSettings();
		Assert.assertTrue(driverSettingPage.isDriverSettingPageDisplayed(), "Driver setting page is not displaying.");
		
		driverSettingPage.expandUnitSettings();
		
		if(!driverSettingPage.isUnitSettingsMileOn())
		{
			driverSettingPage.clcikMileUnitSetting();
		}
		
		Assert.assertFalse(driverSettingPage.isUnitSettingsKilometerOn(), "Kilometer button is selected");
		Assert.assertTrue(driverSettingPage.isUnitSettingsMileOn(), "Miles is not selected.");
		
		driverSettingPage.saveDriverSettings();

		Assert.assertTrue(driverSettingPage.isDriverSettingSuccessfullySavePopUpDisplay(),
				"Driver settings successfully saved is not displaying.");
		driverSettingPage.clickPopUpOk();
		
		Assert.assertFalse(driverSettingPage.isUnitSettingsKilometerOn(), "Kilometer button is selected");
		Assert.assertTrue(driverSettingPage.isUnitSettingsMileOn(), "Miles is not selected.");
		
	}


	private void _userProfile(String strUserName, String strPassword, String strVIN) {
		loginPage.validLogin(strUserName, strPassword, strVIN);
		try {
			consoleLog("Log in with : " + strUserName + "/" + strPassword + "/" + strVIN);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dashboardPage.openMenu();

		// Verify Menu option and User profile section
		Assert.assertTrue(dashboardPage.isMenuPanelDisplayed(), "Menu Panel is displaying");
		Assert.assertTrue(dashboardPage.isUserProfileMenuSectionDisplayed(),
				"User profile VIN section is not displaying at menu panel.");

		dashboardPage.navigateToUserProfile();
		Assert.assertTrue(userProfilePage.isUserProfilePageDisplayed(), "User profile page is not displaying");
	}

	@AfterMethod
	public void tearDown() {
		driver.quit(); 
	}

}
