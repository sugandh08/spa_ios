package com.spa.hyundai.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.AlertSettingsPage;
import com.spa.hyundai.pageObjects.LoginPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LoginTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static AlertSettingsPage alertSettingsPage;
	private static LoginPage loginPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		alertSettingsPage = new AlertSettingsPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");
	}

	/*****************************************************************************************
	 * @Description : Set Up Charge Management Schedule and go back without
	 *              editing/saving schedule
	 * @Steps : Login and go to the Charge Management screen. Go back to the
	 *        dashboard without saving any schedule.
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException
	 *
	 * @Date : 09-July-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule")
	public void testInvalidUsername() throws IOException {
		Reporter.log("Login With :" + "hata412nator.com");
		loginPage.enterLoginCredentialPerformLogin("hata412nator.com", "test1234");

		String REMOTE_COMMAND_MESSAGE = alertSettingsPage.getPopupSuccessMessageText("Please enter");

		System.out.println("Alert Save Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("Please enter your email used for your MyHyundai account."),
				"Enter valid email id alert not appears");
	}

	/*****************************************************************************************
	 * @Description : Set Up 2 Start Driving schedules
	 * @Steps : Login and go to the Charge Management screen. Go to Start Driving
	 *        Settings Page and set both the schedules without off Peak Charging and
	 *        climate schedule
	 *
	 * @author : Rashmi Sharma
	 * @throws IOException
	 *
	 * @Date : 10-July-2019
	 *
	 ******************************************************************************************/

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule")
	public void testInvalidPassword() throws IOException {
		Reporter.log("Login With :" + "hata412@mailinator.com");
		loginPage.enterLoginCredentialPerformLogin("hata412@mailinator.com", "test14");

		String REMOTE_COMMAND_MESSAGE = alertSettingsPage.getPopupSuccessMessageText("Incorrect");

		System.out.println("Alert Save Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("Incorrect username or password"),
				"Incorrect Username And Password alert not appears");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testTermsAndConditionsPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVin) throws IOException {
		Reporter.log("Login With :" + strEmail);
		loginPage.navigateTermsAndConditionsPage(strEmail, strPassword);

		Assert.assertTrue(loginPage.labelTermsAndConditionsDisplayed(), "Terms And Conditions page not displayed");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testRedirectPrompt(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVin) throws IOException {
		Reporter.log("Login With :" + "HI093@yopmail.com");
		loginPage.enterLoginCredentialPerformLogin("HI093@yopmail.com", "test1234");

		Assert.assertTrue(loginPage.isGenesisiAppAlertDisplayed(), "Genesis App alert not appears");

		loginPage.clickCloseBtnGenesisApp();

		Assert.assertTrue(!loginPage.isGenesisiAppAlertDisplayed(), "Genesis App alert not closed");
		Reporter.log("Login With :" + strEmail);
		loginPage.enterLoginCredentialPerformLogin(strEmail, strPassword);

		loginPage.clickOpenBtnGenesisApp();

		Assert.assertTrue(!loginPage.isLabelMyHyundaiDisplayed(), "Genesis App not open");

	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testLoginPageElements(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVin) throws IOException {

		Assert.assertTrue(loginPage.isHyundaiLogoDisplay(), "Hyundai logo not appears");
		Assert.assertTrue(loginPage.isLabelMyHyundaiDisplayed(), "Hyundai label not appears");
		Assert.assertTrue(loginPage.isChkKeepMeLoginDisplay(), "Keep me login checkbox not appears");
		Assert.assertTrue(loginPage.isChkEnableFaceIdDisplay(), "Enable face id checkbox not appears");
		Assert.assertTrue(loginPage.isLinkForgotPasswordDisplay(), "Forgot password link not appears");
		Assert.assertTrue(loginPage.isLinkRegisterDisplay(), "Register link not appears");
		Assert.assertTrue(loginPage.isLinkGuestLoginDisplay(), "Guest link not appears");

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
