package com.spa.hyundai.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.AlertSettingsPage;
import com.spa.hyundai.pageObjects.CarCarePage;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.LoginPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class CarCareTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static AlertSettingsPage alertSettingsPage;
	private static LoginPage loginPage;
	private static DashboardPage dashboardPage;
	private static CarCarePage carCarePage;

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");
	}

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		alertSettingsPage = new AlertSettingsPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
		carCarePage = new CarCarePage(empAppDevice);
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testBluetoothDealersServiceOffersAndOwnersModuler(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strPin);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();
		dashboardPage.clickCarCareOption();

		Assert.assertTrue(carCarePage.isOwnersManualLinkDisplayed(), "Owner's Manual Link not appears");
		Assert.assertTrue(carCarePage.islinkDealerServiceOffersDisplayed(), "Dealer Service Offers Link not appears");
		carCarePage.clickBluetoothLink();
		Assert.assertTrue(carCarePage.isBluetoothLabelDisplayed(), "Bluetooth Page not appears");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testScheduleServicePage(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strPin);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();
		dashboardPage.clickCarCareOption();

		carCarePage.clickScheduleServiceLink();
		empAppDevice.sleepFor(10000);
		Assert.assertTrue(carCarePage.isScheduleServiceLabelDisplayed(), "Schedule Service Page not appears");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testAllSystemNormalDisplayedForDiagnostic(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strPin);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();
		dashboardPage.clickCarCareOption();

		carCarePage.clickDiagnosticsLink();
		Assert.assertTrue(carCarePage.islblAllSystemNormalDisplayed(), "All System Normal not appears");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testMothlyVehicleHealthReportHistoryPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strPin);
		dashboardPage.isbtnRefreshVehicleStatusDisplay();
		dashboardPage.clickCarCareOption();

		carCarePage.clickMonthlyReportLink();
		Assert.assertTrue(carCarePage.islblReprtDateDisplayed(), "Report Date not appears");
		Assert.assertTrue(carCarePage.isbtnScheduleServiceDisplayed(), "Schedule Service button not appears");
		Assert.assertTrue(carCarePage.islblMonthlyVehicleHealthReportDisplayed(),
				"Monthly Vehicle Health Report not appears");
	}

	@Test(description = "Set Up Charge Management Schedule and go back without editing/saving schedule", dataProvider = "TestLoginData")
	public void testElectricalVehicleStateInMVR(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN) throws IOException {

		if (strVehicleType.equalsIgnoreCase("PHEV")) {
			loginPage.validLogin(strEmail, strPassword, strPin);
			dashboardPage.isbtnRefreshVehicleStatusDisplay();
			dashboardPage.clickCarCareOption();

			carCarePage.clickMonthlyReportLink();
			Assert.assertTrue(carCarePage.islblElectricalVehicleStateDisplayed(),
					"Electrical Vehicle state not appears");
		} else {
			System.out.println("This test scenario not running for " + strVehicleType + ": " + strEmail);

		}
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
