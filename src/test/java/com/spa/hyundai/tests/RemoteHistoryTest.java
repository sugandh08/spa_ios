package com.spa.hyundai.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.auto.framework.base.BaseTest;
import com.auto.framework.base.ExtentManager;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.hyundai.pageObjects.DashboardPage;
import com.spa.hyundai.pageObjects.EnterPinPage;
import com.spa.hyundai.pageObjects.LoginPage;
import com.spa.hyundai.pageObjects.RemoteActionsPage;
import com.spa.hyundai.pageObjects.RemoteHistoryPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class RemoteHistoryTest extends BaseTest{ 

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static EnterPinPage enterPinPage;
	private static RemoteActionsPage remoteActionsPage;
	private static RemoteHistoryPage remoteHistoryPage;
	private static LoginPage loginPage;
	private static DashboardPage dashboardPage;
	
	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData .xlsx", "Users");
	}
	
	@BeforeMethod(alwaysRun = true) 
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		enterPinPage = new EnterPinPage(empAppDevice);
		remoteActionsPage = new RemoteActionsPage(empAppDevice);
		remoteHistoryPage = new RemoteHistoryPage(empAppDevice);
		loginPage = new LoginPage(empAppDevice);
		dashboardPage = new DashboardPage(empAppDevice);
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
	/*****************************************************************************************
	 * @Description : Remote History- A remote command is displayed Test
	 * @Steps : Login and send a remote command. and go to remote history  
	 *
	 * @author : Pushpa Kumari
	 * @throws IOException
	 *
	 * @Date : 02-Jan-2020
	 *
	 ******************************************************************************************/
	@Test(description = "Remote History- A remote command is displayed", dataProvider = "TestLoginData")
	public void testRemoteCommandIsDisplayed(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVin) {
		
		Reporter.log("Login With :" + strEmail);
		
		loginPage.validLogin(strEmail, strPassword, strVin);
		System.out.println("Logged In using : " + strEmail);
		
		dashboardPage.clickRemoteActionsOption();
		remoteActionsPage.clickOnRemoteStopButton();
		//enterPinPage.enterPin(strPin);
		dashboardPage.clickOnPopupOKButton();
		dashboardPage.clickOnRemoteHistoryIcon();
		remoteHistoryPage.isPresent();
		Assert.assertEquals(remoteHistoryPage.getMostRecentCommandName(), "Stop");
	}
	
	/*****************************************************************************************
	 * @Description : Remote History- A Failed Remote Command Displays Question Mark
	 * @Steps : Login and send a remote command and go to remote history 
	 *
	 * @author : Pushpa Kumari
	 * @throws IOException
	 *
	 * @Date : 02-Jan-2020
	 *
	 ******************************************************************************************/
	@Test(description = "Remote History- A Failed Remote Command Displays Question Mark", dataProvider = "TestLoginData")
	public void testFailedRemoteCommandDisplaysQuestionMark(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVin) {
		Reporter.log("Login With :" + strEmail);
		loginPage.validLogin(strEmail, strPassword, strVin);
		System.out.println("Logged In using : " + strEmail);
		
		//Testing with remote stop, as it will always fail
		dashboardPage.clickRemoteActionsOption();
		remoteActionsPage.clickOnRemoteStopButton();
		//enterPinPage.enterPin(strPin);
		empAppDevice.sleepFor(3000);
		
		String resultText = dashboardPage.getRemoteResultPopUpMessageText("Remote Stop");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("cannot be processed"), "The Remote Stop command was not proceseed");
		
		dashboardPage.clickOnPopupOKButton();
		dashboardPage.clickOnRemoteHistoryIcon();
		remoteHistoryPage.isPresent();
		
		Assert.assertEquals(remoteHistoryPage.getMostRecentCommandName(), "Stop");
		Assert.assertTrue(remoteHistoryPage.getMostRecentFailedCommandStatus().contains("question"));
	}
	
	/*****************************************************************************************
	 * @Description : Remote History- A Pending Remote Command Displays Pending Status
	 * @Steps : Login and send a remote command and go to remote history 
	 *
	 * @author : Pushpa Kumari
	 * @throws IOException
	 *
	 * @Date : 02-Jan-2020
	 *
	 ******************************************************************************************/
//	@Test(description = "Remote History- A Pending Remote Command Displays Pending Status", dataProvider = "TestLoginData")
//	public void testPendingRemoteCommandDisplaysPendingStatus(String strVehicleType, String strTestRun, String strEmail,
//			String strPassword, String strPin, String strVin) {
//		Reporter.log("Login With :" + strEmail);
//		loginPage.validLogin(strEmail, strPassword, strVin);
//		System.out.println("Logged In using : " + strEmail);
//		
//		dashboardPage.clickRemoteActionsOption();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin(strPin);
//		dashboardPage.clickOnPopupOKButton();
//		
//		dashboardPage.clickOnRemoteHistoryIcon();
//		remoteHistoryPage.isPresent();
//		
//		Assert.assertEquals(remoteHistoryPage.getMostRecentCommandName(), "Lights"); 
//		
//		Assert.assertTrue(remoteHistoryPage.getMostRecentPendingOrSuccessCommandStatus("Lights","Pending").equals("Pending")); 
//	}
//	

	/*****************************************************************************************
	 * @Description : Remote History- A Successful Remote Command Displays Success Status
	 * @Steps : Login and send a remote command and go to remote history 
	 *
	 * @author : Pushpa Kumari
	 * @throws IOException
	 *
	 * @Date : 02-Jan-2020
	 *
	 ******************************************************************************************/
//	@Test(description = "Remote History- A Successful Remote Command Displays Success Status", dataProvider = "TestLoginData")
//	public void testSuccessfulRemoteCommandDisplaysSuccessStatus(String strVehicleType, String strTestRun, 
//			String strEmail,String strPassword, String strPin, String strVin) {
//		Reporter.log("Login With :" + strEmail);
//		loginPage.validLogin(strEmail, strPassword, strVin);
//		System.out.println("Logged In using : " + strEmail);
//		
//		dashboardPage.clickRemoteActionsOption();
//		remoteActionsPage.clickOnFlashLightsButton();
//		enterPinPage.enterPin(strPin);
//		empAppDevice.sleepFor(2000);
//		dashboardPage.clickOnPopupOKButton();
//		
//		String resultText = dashboardPage.getRemoteResultPopUpMessageText("Remote Lights Only");
//		System.out.println("Remote Result Text:" + resultText);
//		Assert.assertTrue(resultText.contains("sent to vehicle"), "The Remote lights command was not successful");
//		
//		dashboardPage.clickOnPopupOKButton();
//		dashboardPage.clickOnRemoteHistoryIcon();
//		remoteHistoryPage.isPresent();
//		
//		Assert.assertEquals(remoteHistoryPage.getMostRecentCommandName(), "Lights");
//		Assert.assertTrue(remoteHistoryPage.getMostRecentPendingOrSuccessCommandStatus("Lights","Success").equals("Success"));
//	}
	
}
 











