package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class ScheduledClimateSettingsPage {
	
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;
	
	@iOSFindBy(accessibility = "Scheduled Climate Settings")
	private MobileElement _txtTitle;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Front Defrost']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _switchFrontDefrost;
	
	@iOSFindBy(accessibility = "sdm btn save default")
	private MobileElement _btnSave;
	
	
	public ScheduledClimateSettingsPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	public boolean isPresent() {
		appDevice.waitForElementToLoad(_txtTitle);

		return _txtTitle.isDisplayed();

	}
	public void setTemperature()
	{
	
		appDevice.swipeScrollWheelDown("(//XCUIElementTypeTable)[1]", 4);
	}
	
	public String getToggleValue(MobileElement element)
	{
		return element.getAttribute("value");
	}
	
	public void setFrontDefrostToggle(String value) {
		if (!(getToggleValue(_switchFrontDefrost).equals(value))) {
			_switchFrontDefrost.click();

		}
	}
	public void clickOnSavebutton()
	{
		_btnSave.click();
	}

}
