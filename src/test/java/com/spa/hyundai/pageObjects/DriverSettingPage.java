package com.spa.hyundai.pageObjects;

import java.time.Duration;
import org.openqa.selenium.support.PageFactory;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class DriverSettingPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name[contains(.,'Driver Settings')]]")
	private MobileElement _labelDriverSettings;

	@iOSFindBy(accessibility = "SETTINGS")
	private MobileElement _tabSettings;

	@iOSFindBy(accessibility = "FAVORITE PLACES")
	private MobileElement _tabFavoritePlaces;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Date / Time Settings']/following-sibling:: XCUIElementTypeImage")
	private MobileElement _arrowDateOrTimeSettings;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Sound Settings']/following-sibling:: XCUIElementTypeImage")
	private MobileElement _arrowSoundSettings;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Vehicle Settings']/following-sibling:: XCUIElementTypeImage")
	private MobileElement _arrowVehicleSettings;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Unit Settings']/following-sibling:: XCUIElementTypeImage")
	private MobileElement _arrowUnitSettings;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='GPS Time']/following-sibling:: XCUIElementTypeSwitch")
	private MobileElement _switchGPSTime;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Daylight Saving Time']/following-sibling:: XCUIElementTypeSwitch")
	private MobileElement _switchDayLightSavingTime;

	@iOSFindBy(accessibility = "on")
	private MobileElement _btnHoursOn;

	@iOSFindBy(accessibility = "off")
	private MobileElement _btnHoursOff;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name='Navigation Guidance']")
	private MobileElement _switchNavigateGuidance;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name='Proximity Warning']")
	private MobileElement _switchProximityWarning;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name='Navigation During Calls']")
	private MobileElement _switchNavigationDuringCalls;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name='Start - up Volume Limit']")
	private MobileElement _switchStartUpVolumeLimit;

	@iOSFindBy(accessibility = "Use Map Screen Volume Buttons")
	private MobileElement _btnUseMapScreenVolume;

	@iOSFindBy(accessibility = "Use Map Screen Volume Buttons or Volume Knob")
	private MobileElement _btnVolumeKnob;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name='Gear Position Pop - Up']")
	private MobileElement _switchGearPositionUp;

	@iOSFindBy(xpath = "/XCUIElementTypeSwitch[@name='Wiper / Lights Display']")
	private MobileElement _switchWiperOrLightDisplay;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name='Icy Road Warning']")
	private MobileElement _switchIceRoadWarning;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name='Welcome Sound']")
	private MobileElement _switchWelComeSound;

	@iOSFindBy(accessibility = "Extended")
	private MobileElement _btnExtended;

	@iOSFindBy(accessibility = "Normal")
	private MobileElement _btnNormal;

	@iOSFindBy(accessibility = "Off")
	private MobileElement _btnOff;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name='Wireless Charging System']")
	private MobileElement _switchWireLessChargingSystem;

	@iOSFindBy(accessibility = "Kilometer (Km)")
	private MobileElement _btnKilometer;

	@iOSFindBy(accessibility = "Mile (mi)")
	private MobileElement _btnMile;

	@iOSFindBy(accessibility = "SAVE")
	private MobileElement _btnSave;

	@iOSFindBy(accessibility = "Cancel")
	private MobileElement _linkCancel;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnPopUpOk;

	public DriverSettingPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isDriverSettingPageDisplayed() {

		return _labelDriverSettings.isDisplayed() && _tabSettings.getAttribute("value").contains("1");
	}
	
	
	//Settings arrow
	public boolean isDateOrTimeSettingSectionDisplay() {
		return _arrowDateOrTimeSettings.isDisplayed();
	}

	public boolean isSoundSettingSectionDisplay() {
		return _arrowSoundSettings.isDisplayed();
	}

	public boolean isVehicleSettingsSectionDisplay() {
		return _arrowVehicleSettings.isDisplayed();
	}

	public boolean isUnitSettingSectionDisplay() {
		return _arrowUnitSettings.isDisplayed();
	}
	
	//Date or time settings

	public boolean isGPSSwitchOn() {
		return _switchGPSTime.getAttribute("value").contains("1");
	}

	public boolean isDayLightSavingSwitchOn() {
		return _switchDayLightSavingTime.getAttribute("value").contains("1");
	}

	public boolean isHoursOnButtonSelected() {
		try {
			return _btnHoursOn.getAttribute("value").contains("1");
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isHoursOffButtonSelected() {
		try {
			return _btnHoursOff.getAttribute("value").contains("1");
		} catch (Exception e) {
			return false;
		}
	}
	
	//Priority settings
	public boolean isNavigateGuidanceSwitchOn() {
		return _switchNavigateGuidance.getAttribute("value").contains("1");
	}

	public boolean isProximityWarningOn() {
		return _switchProximityWarning.getAttribute("value").contains("1");

	}

	public boolean isNavigationDuringCallOn() {
		return _switchNavigationDuringCalls.getAttribute("value").contains("1");

	}

	public boolean isStartUpVolumeLimitOn() {
		return _switchStartUpVolumeLimit.getAttribute("value").contains("1");

	}

	public boolean isUseMapScreenVolumeButtonSelected() {
		try {
			return _btnUseMapScreenVolume.getAttribute("value").contains("1");
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isUseMapScreenVolumeKnobButtonSelected() {
		try {
			return _btnVolumeKnob.getAttribute("value").contains("1");
		} catch (Exception e) {
			return false;
		}
	}
	
	//Popup displayed after saving the changes in driver settings.
	public boolean isDriverSettingSuccessfullySavePopUpDisplay() {
		return _btnPopUpOk.isDisplayed();
	}
	
	//Unit settings
	
	public boolean isUnitSettingsKilometerOn() {
		try {
			return _btnKilometer.getAttribute("value").contains("1");
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isUnitSettingsMileOn() {
		try {
			return _btnMile.getAttribute("value").contains("1");
		} catch (Exception e) {
			return false;
		}
	}

	public void expandDateOrTimeSettingSection() {
		_arrowDateOrTimeSettings.click();
	}

	public void expandVehicleSettingsSection() {
		_arrowVehicleSettings.click();
	}


	public void clickHourOn() {

		_btnHoursOn.click();

	}

	public void clickHourOff() {

		_btnHoursOff.click();
	}

	public void clickGPSSwitch() {

		_switchGPSTime.click();

	}

	public void clickDayLightSwitch() {
		_switchDayLightSavingTime.click();
	}
		
	//Sound driver settings.

	public void expandSoundSettingSection() {
		_arrowSoundSettings.click();
	}

	public void clickNavigationGuidance() {
		if (!isNavigateGuidanceSwitchOn()) {
			_switchNavigateGuidance.click();
		}
	}

	public void clickProximityWarning() {
		if (!isProximityWarningOn()) {
			_switchProximityWarning.click();
		}
	}

	public void clickNavidationDuringCall() {
		if (!isNavigationDuringCallOn()) {
			_switchNavigationDuringCalls.click();
		}
	}

	public void clickStartUpVolumeLimit() {
		if (!isStartUpVolumeLimitOn()) {
			_switchStartUpVolumeLimit.click();
		}
	}

	public void clickUserMapScreenVolumeButton() {
		if (!isUseMapScreenVolumeButtonSelected()) {
			_btnUseMapScreenVolume.click();
		}
	}

	public void clickUserMapScreenVolumeButtonsOrKnob() {
		if (!isUseMapScreenVolumeKnobButtonSelected()) {
			_btnVolumeKnob.click();
		}
	}
	
	//Unit Settings
	
	public void expandUnitSettings()
	{
		_arrowUnitSettings.click();
	}
	
	public void clickKilometerUnitSetting()
	{
		_btnKilometer.click();
	}
	
	public void clcikMileUnitSetting()
	{
		_btnMile.click();
	}

	
	//Save and cancel driver settings
	
	public void saveDriverSettings() {
		_btnSave.click();
	}

	public void cancelDriverSettings() {
		_linkCancel.click();
	}
	
	//Popup after saving driver settings.

	public void clickPopUpOk() {
		_btnPopUpOk.click();
	}

}
