package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindAll;
import io.appium.java_client.pagefactory.iOSFindBy;

public class MapPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	// Map Options

	@iOSFindBy(accessibility = "Favorites")
	private MobileElement _linkMyPoi;

	@iOSFindBy(accessibility = "Dealers")
	private MobileElement _linkHyundaiDealers;

	@iOSFindBy(accessibility = "Nearby Fuel")
	private MobileElement _linkNerebyFuel;

	@iOSFindBy(accessibility = "Car Finder")
	private MobileElement _linkCarFinder;

	@iOSFindBy(accessibility = "Collision Repair Center")
	private MobileElement _linkCollsionRepairCenter;

	@iOSFindBy(accessibility = "Charge Stations")
	private MobileElement _linkChargeStations;

	@iOSFindBy(accessibility = "H2 Stations")
	private MobileElement _linkH2Stations;

	// FaceId enable Popup for future Remote Actions

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Face ID')]")
	private MobileElement _txtFaceId;

	@iOSFindBy(accessibility = "Not now")
	private MobileElement _btnNotNowFaceId;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[contains(@name,'sdm popupmenu map')]")
	private MobileElement _popUpMenuMap;

	@iOSFindAll({
	@iOSBy(accessibility = "sdm navibar back"),@iOSBy(xpath="//XCUIElementTypeNavigationBar[@name='SDMMapView']/XCUIElementTypeButton"),@iOSBy(xpath="//XCUIElementTypeNavigationBar[@name='SDMWebBrowserView']/XCUIElementTypeButton")})
	private MobileElement _btnBack;	
	

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='sdm_search_bar']/following-sibling:: XCUIElementTypeTextField")
	private MobileElement _searchBar;

	@iOSFindBy(accessibility = "sdm search icn")
	private MobileElement _btnSearch;

	@iOSFindBy(accessibility = "sdm_popupmenu_poisearch")
	private MobileElement _linkSearchPoi;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[starts-with(@name,'No')]")
	private MobileElement _txtNotfound;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindBy(accessibility = "sdm btn share default")
	private MobileElement _linkShare;

	@iOSFindBy(accessibility = "map btn mypoi unsaved")
	private MobileElement _linkSave;

	@iOSFindBy(accessibility = "sdm btn sendToCar default")
	private MobileElement _linkSend;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='bl_poi_tooltip_arrow']/preceding-sibling:: XCUIElementTypeButton")
	private MobileElement _tooltipArrow;

	@iOSFindBy(accessibility = "POI successfully added.")
	private MobileElement _txtPOISuccessfullyAdded;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'This location has already been saved to your favorites.')]")
	private MobileElement _txtPOIAlreadySaved;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='OK']/ancestor::XCUIElementTypeAlert//XCUIElementTypeStaticText")
	private MobileElement _labelMesssage;

	@iOSFindBy(accessibility = "bl map btn search view list de")
	private MobileElement _poiViewList;

	@iOSFindBy(accessibility = "sdm btn delete default")
	private MobileElement _linkDelete;

	@iOSFindBy(accessibility = "map new car finder")
	private MobileElement iconMapNewCarFinder;

	@iOSFindBy(accessibility = "map new location")
	private MobileElement iconMapNewLocation;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='map_icn_address']/following-sibling:: XCUIElementTypeStaticText")
	private MobileElement txtMapLocationAddress;

	@iOSFindBy(xpath = "//XCUIElementTypeAlert[@name='Location Services Disabled']")
	private MobileElement _alertLocationServiceDisabled;

	@iOSFindBy(accessibility = "Cancel")
	private MobileElement buttonCancelAlert;

	@iOSFindBy(accessibility = "Favorites")
	private MobileElement linkFavorites;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='map btn mypoi unsaved']/following-sibling:: XCUIElementTypeStaticText)[1]")
	private MobileElement labelAddress;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Send to Car']")
	private MobileElement btnSendToCar;

	@iOSFindBy(accessibility = "POI successfully sent to car.")
	private MobileElement labelSuccessfullySentToCar;

	@iOSFindBy(accessibility = "Dealer Locator")
	private MobileElement _mapDealerLocator;

	@iOSFindBy(accessibility = "Hyundai Parts & Collision Info")
	private MobileElement labelCollisionRepairCenter;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Preferred Dealer']/following-sibling:: XCUIElementTypeStaticText)[1]")
	private MobileElement dealerNameTxt;

	@iOSFindBy(accessibility = "map btn mypoi star")
	private MobileElement startIcon;

	@iOSFindBy(accessibility = "Schedule Service")
	private MobileElement btnScheduleService;

	@iOSFindBy(accessibility = "Services Page")
	private MobileElement pageScheduleService;

	@iOSFindBy(xpath = "//XCUIElementTypeImage/following-sibling::XCUIElementTypeStaticText[@name='Schedule Service']")
	private MobileElement labelScheduleService;

	
	
	@iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='banner']//XCUIElementTypeStaticText)[1]|//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeButton/following-sibling::XCUIElementTypeLink|(//XCUIElementTypeOther[@name='ServiceEdge Appointments Mobile']//XCUIElementTypeStaticText)[1]|//XCUIElementTypeStaticText[contains(@name,'SCHEDULE A SERVICE')]|//XCUIElementTypeOther[@name='West Broad Hyundai']")
	private MobileElement txtDealerNameOnScheduleServicePage;

	public MapPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public String getPreferredDealerName() {
		return dealerNameTxt.getAttribute("name");
	}

	public void clickHyundaiDealersOption() {
		appDevice.sleepFor(5000);
		_linkHyundaiDealers.click();
		if (appDevice.isElementDisplayed(_btnOkPopUp)) {
			_btnOkPopUp.click();
		}

	}

	public void clickCarFinderOption() {
		_linkCarFinder.click();

	}

	public void clickChargeStations() {
		_linkChargeStations.click();
		if (appDevice.isElementDisplayed(_txtNotfound)) {
			_btnOkPopUp.click();
		}

	}

	public void clickNearByFuel() {
		_linkNerebyFuel.click();

	}

	public void clickOnBackButton() {
		_btnBack.click();
	}

	public boolean isBackButtonDisplay() {
		return appDevice.isElementDisplayed(_btnBack);
	}

	public void navigateToMyPoi() {
		_linkMyPoi.click();
		appDevice.sleepFor(10000);
		if (appDevice.isElementDisplayed(_txtNotfound)) {
			_btnOkPopUp.click();
		}

	}

	public boolean txtNotfoundPopupDisplay() {
		try {
			return _txtNotfound.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}

	}

	public void navigateToHyundaiDealer() {
		_linkHyundaiDealers.click();
		appDevice.sleepFor(10000);
		if (txtNotfoundPopupDisplay()) {
			_btnOkPopUp.click();
		}
	}

	public void navigateToNearbyGas() {
		_linkNerebyFuel.click();
		appDevice.sleepFor(10000);
		if (appDevice.isElementDisplayed(_txtNotfound)) {
			_btnOkPopUp.click();
		}
	}

	public void enterSearchText(String zip) {
		_searchBar.click();
		_searchBar.sendKeys(zip);      
		_searchBar.sendKeys(Keys.ENTER);
		if (appDevice.isElementDisplayed(_btnOkPopUp)) {
			_btnOkPopUp.click();
		}
		appDevice.sleepFor(2000);

	}

	public String searchResultGetText(String searchText) {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + searchText + "']")).getText();
	}

	public String clickOnSaveIcon() {
		_tooltipArrow.click();
		_linkSave.click();
		appDevice.sleepFor(10000);

		if (_btnOkPopUp.isDisplayed()) {
			String strMsg = _labelMesssage.getText();
			_btnOkPopUp.click();
			return strMsg;
		} else {
			return null;
		}
	}

	public boolean clickSavedPOI(String savedPOIText) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		System.out.println("ID " + parentID);
		HashMap<String, String> scrollObject = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 20; i++) {
			MobileElement poiName = appDevice.getDriver()
					.findElement((By.xpath("//XCUIElementTypeStaticText[@name='" + savedPOIText + "']")));
			if (poiName.isDisplayed()) {
				poiName.click();
				flag = true;
				break;

			} else {
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

//		_tooltipArrow.click();
//		appDevice.sleepFor(5000);
//		_linkSave.isDisplayed();
//		_linkSave.click();
//		appDevice.sleepFor(5000);
//		_btnOkPopUp.click();

		return flag;
	}

	public boolean isIconMapNewCarFinderDisplay() {

		return iconMapNewCarFinder.isDisplayed();

	}

	public boolean isIconMapNewLocationDisplay() {

		return iconMapNewLocation.isDisplayed();

	}

	public boolean isTxtDisplayedUnderSearchTextBox() {
		return _searchBar.getAttribute("value").contains("Enter Zip or City");
	}

	public boolean verifyAddressInExpandableList(String savedPOIText) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		System.out.println("ID " + parentID);
		HashMap<String, String> scrollObject = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 50; i++) {
			try {
				MobileElement poiName = appDevice.getDriver()
						.findElement((By.xpath("//XCUIElementTypeStaticText[@name='" + savedPOIText + "']")));

				if (poiName.isDisplayed()) {
					poiName.click();
					flag = true;
					break;

				} else {
					appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
				}
			} catch (NoSuchElementException ex) {
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

		return flag;
	}

	public boolean verifyAddressInExpandableList(String strAddressLabel, String strZip, String strAddressFirstLetter) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		System.out.println("ID " + parentID);
		HashMap<String, String> scrollObject = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 50; i++) {
			try {
				MobileElement poiName = appDevice.getDriver()
						.findElement((By.xpath("//XCUIElementTypeStaticText[@name='" + strAddressLabel
								+ "']/following-sibling::XCUIElementTypeStaticText[contains(@name,'" + strZip
								+ "') and contains(@name,'" + strAddressFirstLetter + "')]")));

				if (poiName.isDisplayed()) {
					poiName.click();
					flag = true;
					break;
				} else {
					appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
				}

			} catch (NoSuchElementException ex) {
				System.out.println("test3");
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

		return flag;
	}

	public String getMapLocationAddress() {
		appDevice.waitForElementToLoad(txtMapLocationAddress);
		return txtMapLocationAddress.getAttribute("value");
	}

	public boolean isSearchBarDisplay() {
		appDevice.sleepFor(10000);
		return _searchBar.isDisplayed();

	}

	public boolean isAlertLocationServiceDisabledDisplay() {

		appDevice.waitForElementToLoad(buttonCancelAlert);

		driver.switchTo().alert();
		return appDevice.isElementDisplayed(_alertLocationServiceDisabled);

	}

	public void clickCancelAlertButton() {
		buttonCancelAlert.click();
	}

	public void clickGPSIcon() {
		iconMapNewLocation.click();

	}

	public String getMapLocationAddressLabel() {
		return labelAddress.getAttribute("value");
	}

	public void clickFavorites() {
		linkFavorites.click();
	}

	public void clickH2Stations() {
		_linkH2Stations.click();
		if (appDevice.isElementDisplayed(_txtNotfound)) {
			_btnOkPopUp.click();
		}

	}

	public void clickSendToCar() {
		appDevice.waitForElementToLoad(btnSendToCar);
		btnSendToCar.click();
	}

	public boolean isPoiSuccessfullySentToCar() {
		appDevice.waitForElementToLoad(labelSuccessfullySentToCar);
		return appDevice.isElementDisplayed(labelSuccessfullySentToCar);
	}

	public boolean isDealerLocatorPageDisplayed() {
		appDevice.waitForElementToLoad(_mapDealerLocator);
		return _mapDealerLocator.isDisplayed();
	}

	public void clickCollisionRepairCenter() {
		appDevice.waitForElementToLoad(_linkCollsionRepairCenter);
		_linkCollsionRepairCenter.click();
	}

	public boolean isCollisionRepairCenterPageDisplayed() {
		appDevice.waitForElementToLoad(labelCollisionRepairCenter);
		return labelCollisionRepairCenter.isDisplayed();
	}

	public void clickOnStartIcon() {
		startIcon.click();
	}

	public String setDealerAsPreferred() {
		clickOnStartIcon();
		String strPopupMessage = _labelMesssage.getAttribute("name");
		_btnOkPopUp.click();
		_btnOkPopUp.click();
		return strPopupMessage;
	}

	public String setDealerAsPreferred_ScheduleUI() {
		try {
			clickOnStartIcon();
			String strPopupMessage = _labelMesssage.getAttribute("name");
			_btnOkPopUp.click();
			_btnOkPopUp.click();
			return strPopupMessage;
		} catch (Exception e) {
			return "Exception Occur";
		}
	}

	public boolean isScheduleServiceBtnVisible() {
		return appDevice.isElementDisplayed(btnScheduleService);
	}

	public void clicScheduleServiceBtn() {
		btnScheduleService.click();
	}

	public boolean isScheduleservicePageVisible() {
		appDevice.waitForElementToLoadWithProvidedTime(pageScheduleService,2);
		return appDevice.isElementDisplayed(pageScheduleService);
	}

	public boolean isLabelScheduleServiceVisible() {
		appDevice.waitForElementToLoadWithProvidedTime(labelScheduleService, 3);

		return appDevice.isElementDisplayed(labelScheduleService);
	}

	public String getScheduleServiceNameFromScheduleServicePage() {
		return txtDealerNameOnScheduleServicePage.getAttribute("name");
	}
}
