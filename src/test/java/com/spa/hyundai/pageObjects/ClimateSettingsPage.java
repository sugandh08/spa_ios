package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class ClimateSettingsPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "Remote Start")
	private MobileElement _txtRemoteStart;

	@iOSFindBy(accessibility = "Start")
	private MobileElement _btnStart;

	@iOSFindBy(accessibility = "Stop")
	private MobileElement _btnStop;

	@iOSFindBy(accessibility = "(//XCUIElementTypeTable)[1]")
	private MobileElement _elementTemperatureScroll;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Front Defroster']/following-sibling:: XCUIElementTypeSwitch")
	private MobileElement _switchFrontDefroster;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Heated Features']/following-sibling:: XCUIElementTypeSwitch")
	private MobileElement _switchHeatedFeatures;

	@iOSFindBy(accessibility = "sdm btn submit default")
	private MobileElement _btnSubmit;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOKRemoteMessage;
	
	@iOSFindBy(accessibility="icnCollapsed down")
	private MobileElement _accordianEngineDuration;
	
	@iOSFindBy(xpath="//XCUIElementTypeStaticText[@name='10']")
	private MobileElement _elementEngineScroll;
	
	@iOSFindBy(xpath="//XCUIElementTypeStaticText[@name='Temperature']/following-sibling::XCUIElementTypeSwitch")
	private MobileElement _switchTemperature;
	
	@iOSFindBy(xpath="//XCUIElementTypeStaticText[@name='Start Vehicle Without Presets']/preceding-sibling:: XCUIElementTypeButton")
	private MobileElement btnStartVehicleWithoutPresets;
	
	@iOSFindBy(accessibility="View pre-conditions for remote start")
	private MobileElement labelViewPreCondition;


	public ClimateSettingsPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	public void clickStartVehicleWithoutPresets() {
		if(appDevice.isElementDisplayed(btnStartVehicleWithoutPresets)) {
			btnStartVehicleWithoutPresets.click();
		}
	}
	
	public boolean isViewPreConditionLabelDisplay() {
		appDevice.waitForElementToLoad(labelViewPreCondition);
		return labelViewPreCondition.isDisplayed();
	}

	public boolean isRemoteStartTextDisplay() {
		appDevice.waitForElementToLoadWithProvidedTime(_txtRemoteStart, 2);;
		return _txtRemoteStart.isDisplayed();

	}

	public void clickOnStartButton() {
		appDevice.waitForElementToLoad(_btnStart);
		_btnStart.click();
	}

	public void clickOnStopButton() {
		appDevice.waitForElementToLoad(_btnStop);
		_btnStop.click();
		_btnSubmit.click();
	}

	public void setTemperatureScrollValue(String temperature) {

		driver.findElementByAccessibilityId(temperature).click();

	}

	public void clickFrontDefrosterSwitch() {
		appDevice.waitForElementToLoad(_switchFrontDefroster);
		_switchFrontDefroster.click();
	}

	public void clickHeatedFeaturesSwitch() {
		appDevice.waitForElementToLoad(_switchHeatedFeatures);
		_switchHeatedFeatures.click();
	}

	public void clickSubmitButton() {
		appDevice.waitForElementToLoad(_btnSubmit);
		_btnSubmit.click();
	}

	public void setStartSettingsStates(String frontDefrosterToggle, String heatedFeaturesToggle) {

		if (!(_switchFrontDefroster.getAttribute("value").equals(frontDefrosterToggle))) {
			clickFrontDefrosterSwitch(); 
		}

		if (!(_switchHeatedFeatures.getAttribute("value").equals(heatedFeaturesToggle))) {
			clickHeatedFeaturesSwitch();
		}

		clickSubmitButton();
	}

	public void clickOnRemoteCommandOKButton() {
		appDevice.waitForElementToLoad(_btnOKRemoteMessage);
		_btnOKRemoteMessage.click();
	}
	
	public void clickEngineDurationAccordian()
	{
		appDevice.waitForElementToLoad(_accordianEngineDuration);
		_accordianEngineDuration.click();
	}
	
	public void clickTemperatureSwitch()
	{
		appDevice.waitForElementToLoad(_switchTemperature);
		_switchTemperature.click();
	}
	
	public void setStartSettingsStates(String temperatureToggle, String frontDefrosterToggle, String heatedFeaturesToggle){
		
				
        if(!(_switchTemperature.getAttribute("value").equals(temperatureToggle))){
        	clickTemperatureSwitch();
        }

        if(!(_switchFrontDefroster.getAttribute("value").equals(frontDefrosterToggle))){
        	clickFrontDefrosterSwitch();
        }

        if(!(_switchHeatedFeatures.getAttribute("value").equals(heatedFeaturesToggle))){
        	clickHeatedFeaturesSwitch();
        }
        
        clickSubmitButton();
    }
	
	public void swipeEngineScrollWheelUp(int entriesToScroll)
	{
		Rectangle dimensions=_elementEngineScroll.getRect();
		
		int xCoordinate = dimensions.x + (int)(dimensions.width * 0.25);
        int yCoordinateStart = dimensions.y + (int)(dimensions.height * 0.25);
        int yCoordinateEnd = dimensions.y + (int)(dimensions.height * 0.50);
        
        PointOption pointStart = new PointOption().withCoordinates(xCoordinate, yCoordinateStart);
        PointOption pointEnd = new PointOption().withCoordinates(0, yCoordinateEnd);
        WaitOptions waitOptions = new WaitOptions().withDuration(Duration.ofMillis(1000));

        
        TouchAction touchAction = new TouchAction(driver);

        for (int i = 0; i < entriesToScroll; i++) {
            touchAction
                    .press(pointStart)
                    .waitAction(waitOptions)
                    .moveTo(pointEnd)
                    .release()
                    .perform();
            try {
                Thread.sleep(250);
            } catch (InterruptedException ie) {
                //
            }
        }
	}

}
