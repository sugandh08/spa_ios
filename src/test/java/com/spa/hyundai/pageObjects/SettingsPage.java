package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Element;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class SettingsPage {
	
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	
	private final int IMPLICIT_WAIT = 10;
	
	@iOSFindBy(accessibility = "Settings")
	private MobileElement _txtSettings;
	
	@iOSFindBy(accessibility = "Change PIN")
	private MobileElement _btnChangePin;
	
	@iOSFindBy(accessibility = "AUTOMATIC COLLISION NOTIFICATION (ACN)")
	private MobileElement _txtACN;
	
	@iOSFindBy(accessibility = "SOS EMERGENCY ASSISTANCE")
	private MobileElement _txtSosEmergencyAssitance;
	
	@iOSFindBy(accessibility = "AUTOMATIC DTC")
	private MobileElement _txtAutomaticDtc;
	
	@iOSFindBy(accessibility = "MONTHLY VEHICLE HEALTH REPORT")
	private MobileElement _txtMonthlyVehicleHealthReport;

	@iOSFindBy(accessibility = "MAINTENANCE ALERT")
	private MobileElement _txtMaintenanceAlert;
	
	@iOSFindBy(accessibility = "sdm navibar back")
	private MobileElement _iconNavigateBack;
	
	
	public SettingsPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	public boolean isSettingsPageVisible()
	{
		return appDevice.isElementDisplayed(_txtSettings);
	}
	
	
	public boolean isSettingsPageDisplayed()
	{
		appDevice.waitForElementToLoad(_txtSettings);
		return _txtSettings.isDisplayed();
	}

	public void clickOnExpandButton(String notificationText) {
		driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+notificationText+"']/following-sibling::XCUIElementTypeButton[@name='btn collapse']")).click();
		appDevice.waitForElementToLoad(driver.findElement(By.xpath("//XCUIElementTypeOther[@name='"+notificationText+"']//XCUIElementTypeStaticText[@name='Receive all notifications on this device']")));
	}
	
	
	public  void toggleAllNotification(String notificationType, int notificationIndex) {
		
		//notificationType = "Remote" or "Connected Care" , notificationIndex= 1 for Text, 2 for Email,3 for App
		MobileElement toggleAllXpath= driver.findElement(By.xpath("//XCUIElementTypeOther[@name='"+notificationType+"']"
				+ "//XCUIElementTypeButton[@name='empty box']["+notificationIndex+"]"));
	
	  	toggleAllXpath.click();
	    appDevice.sleepFor(7000);
		
	}
	
     public String getNotifictaionStateOfSetting(String notificationText, String notificationType)
     {
    	 //notificationText=" Element text to be passed" and notificationType = "text" or "email or "phone"
    	 
    	 return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+notificationText+"']/following-sibling::XCUIElementTypeButton[@name='grey "+notificationType+" icon']")).getAttribute("enabled");
     }
     
     public List<MobileElement> xpathOfAllNotificationTexts(String notificationType){
    	  return driver.findElements(By.xpath("//XCUIElementTypeOther[@name='"+notificationType+"']/following-sibling::XCUIElementTypeCell[@visible='true']/XCUIElementTypeStaticText"));
     }

     public void clickOnToggleButtonForNotification(String notificationText, String notificationType){
    	
    	 appDevice.scrollDown();
    	 //notificationText=" Element text to be passed" and notificationType = "text" or "email or "phone"
    	 MobileElement notificationElement = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+notificationText+"']/following-sibling::XCUIElementTypeButton[@name='grey "+notificationType+" icon']")); 
    	Assert.assertTrue(notificationElement.isDisplayed());
    	 
    	 String oldToggleStatus= notificationElement.getAttribute("enabled");
    	 notificationElement.click();
    	 
    	 appDevice.sleepFor(7000);
    	 String newToggleStatus= notificationElement.getAttribute("enabled");
    	 
    	 Assert.assertTrue(oldToggleStatus!=newToggleStatus);
     }
     
     public void navigateBack () {
    	 _iconNavigateBack.click();
    	 
     }
     
 	public boolean isBackButtonDisplay() {
		return appDevice.isElementDisplayed(_iconNavigateBack);
	}
     
}
