package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindAll;
import io.appium.java_client.pagefactory.iOSFindBy;

public class UserProfilePage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "MyHyundai Profile")
	private MobileElement _labelUserProfile;

	@iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='MyHyundai']/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[5]")
	private MobileElement _panelDriver;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Primary Driver']/following-sibling:: XCUIElementTypeStaticText)[1]")
	private MobileElement _labelPrimaryDriverName;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Primary Driver']/following-sibling:: XCUIElementTypeStaticText)[2]")
	private MobileElement _labelPrimaryDriverAddress;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Secondary Driver']/following-sibling:: XCUIElementTypeStaticText")
	private MobileElement _labelSecondaryDriverName;

	@iOSFindBy(accessibility = "My Driver Settings")
	private MobileElement _btnMyDriverSettings;

	@iOSFindBy(accessibility = "INVITE ADDITIONAL DRIVER")
	private MobileElement _btnInviteAdditionalDriver;

	@iOSFindAll({ @iOSBy(xpath = "//XCUIElementTypeStaticText[@name[contains(.,'First name')]]") })
	private List<MobileElement> _listAdditionalDrivers;

	@iOSFindBy(accessibility = "You have reached your Additional Driver limit of 4. Please remove an Additional Driver if you wish to continue.")
	private MobileElement _popUpAdditionalDriverLimit;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnPopUpOk;

	@iOSFindAll({ @iOSBy(xpath = "//XCUIElementTypeStaticText[@name='SELECT FROM CONTACTS']") })
	private MobileElement linkSelectFromContacts;
	
	@iOSFindAll({ @iOSBy(xpath = "//XCUIElementTypeStaticText[@name='“MyHyundai” Would Like to Access Your Contacts']") })
	private MobileElement alertMyHyundaiAlertDisplayed;
	


	public UserProfilePage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isUserProfilePageDisplayed() {
		appDevice.sleepFor(10000);
		appDevice.waitForElementToLoad(_labelUserProfile);
		return _labelUserProfile.isDisplayed();
	}

	public boolean isDriverPanelDisplay() {
		appDevice.waitForElementToLoad(_panelDriver);
		return _panelDriver.isDisplayed();
	}

	public boolean isPrimaryDriverInfoDisplay() {
		boolean bFlag = false;

		if (_labelPrimaryDriverAddress.isDisplayed() && _labelPrimaryDriverName.isDisplayed()) {
			return bFlag = true;
		} else {
			return bFlag;
		}
	}

	public boolean isSecondaryDriverDisplay() {
		return _labelSecondaryDriverName.isDisplayed();
	}

	public boolean isMaxAdditionalDriverLimitReached() {
		boolean bFlag = false;
		if (_listAdditionalDrivers.size() == 4) {
			_btnInviteAdditionalDriver.click();
			if (_popUpAdditionalDriverLimit.isDisplayed()) {
				bFlag = true;
				_btnPopUpOk.click();
				// return bFlag;
			}
		}
		return bFlag;
	}

	public boolean isSecondaryDriverSetVehicleDisplay(String strEmailID) {
		return driver.findElementByXPath("(//XCUIElementTypeStaticText[@name[contains(.,'" + strEmailID
				+ "')]])[2]/following-sibling::XCUIElementTypeStaticText").isDisplayed();
	}

	public void navigateToMyDriverSettings() {
		if (_btnMyDriverSettings.isDisplayed()) {
			_btnMyDriverSettings.click();
		}
	}

	public void navigateToInviteAdditionalDriver() {
		for (int i = 0; i <= 10; i++) {
			appDevice.scrollDown();
			if (appDevice.isElementDisplayed(_btnInviteAdditionalDriver)) {
				_btnInviteAdditionalDriver.click();
				break;
			}
		}

	}

	public String getSecondryDriverUserID() {
		return _labelSecondaryDriverName.getText();
	}

	public void clickSelectFromContactsLink() {
		appDevice.waitForElementToLoadWithProvidedTime(linkSelectFromContacts, 5);
		linkSelectFromContacts.click();
	}
	
	public boolean isMyHyundaiAlertDisplayed() {
		appDevice.waitForElementToLoadWithProvidedTime(alertMyHyundaiAlertDisplayed, 5);
		return appDevice.isElementDisplayed(alertMyHyundaiAlertDisplayed); 
	}

}
