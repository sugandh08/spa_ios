package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class LoginPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private static SwitchVehiclePage switchVehiclePage;

	private final int IMPLICIT_WAIT = 10;

	@AndroidFindBy(id = "login_username_edit")
	@iOSFindBy(xpath = "//XCUIElementTypeTextField")
	private MobileElement _txtUsername;

	@AndroidFindBy(id = "login_password_edit")
	@iOSFindBy(xpath = "//XCUIElementTypeSecureTextField")
	private MobileElement _txtPassword;

	@AndroidFindBy(id = "login_ll")
	@iOSFindBy(accessibility = "Login")
	private MobileElement _btnLogin;

	@iOSFindBy(accessibility = "Accept")
	private MobileElement _btnAccept;

	@AndroidFindBy(id = "permission_allow_button")
	private MobileElement _btnAllow;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Allow While Using App']")
	private MobileElement _btnAllowUsingAppPopup;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Don’t Allow']")
	private MobileElement _btnDontUsingAppPopup;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[contains(@name,'Allow')]")
	private List<MobileElement> _btnAllow1d;

	@iOSFindBy(accessibility = "View Details")
	private MobileElement _btnViewDetails;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Terms & Conditions'])[1]")
	private MobileElement _labelTermsAndConditions;

	@iOSFindBy(xpath = "//XCUIElementTypeAlert[@name='Genesis App']")
	private MobileElement alertGenesisiApp;

	@iOSFindBy(accessibility = "Close")
	private MobileElement btnCloseGenesisApp;

	@iOSFindBy(accessibility = "Open")
	private MobileElement btnOpenGenesisApp;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='MyHyundai']")
	private MobileElement labelMyHyundai;

	@iOSFindBy(accessibility = "login_icon")
	private MobileElement iconLogo;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Keep me logged in']/preceding-sibling:: XCUIElementTypeOther/XCUIElementTypeImage")
	private MobileElement chkKeepMeLogin;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Enable Face ID']/preceding-sibling:: XCUIElementTypeOther/XCUIElementTypeImage")
	private MobileElement chkEnableFaceId;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Forgot Password?']")
	private MobileElement linkForgotPassword;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Register']")
	private MobileElement linkRegister;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Guest Login']")
	private MobileElement linkGuestLogin;

	@iOSFindBy(accessibility = "Dismiss")
	private MobileElement _btnDismiss;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='LOGIN']")
	private MobileElement _linkLoginGenesis;

	public LoginPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
		switchVehiclePage = new SwitchVehiclePage(empAppDevice);
	}

	public boolean isHyundaiLogoDisplay() {
		appDevice.waitForElementToLoad(iconLogo);
		return iconLogo.isDisplayed();
	}

	public boolean isChkKeepMeLoginDisplay() {
		appDevice.waitForElementToLoad(chkKeepMeLogin);
		return chkKeepMeLogin.isEnabled();
	}

	public boolean isChkEnableFaceIdDisplay() {
		appDevice.waitForElementToLoad(chkEnableFaceId);
		return chkEnableFaceId.isEnabled();
	}

	public boolean isLinkForgotPasswordDisplay() {
		appDevice.waitForElementToLoad(linkForgotPassword);
		return linkForgotPassword.isDisplayed();
	}

	public boolean isLinkRegisterDisplay() {
		appDevice.waitForElementToLoad(linkRegister);
		return linkRegister.isDisplayed();
	}

	public boolean isLinkGuestLoginDisplay() {
		appDevice.waitForElementToLoad(linkGuestLogin);
		return linkGuestLogin.isDisplayed();
	}

	public void clickBtnViewDetails() {
		_btnViewDetails.click();
	}

	public void setUsername(String username) {
		appDevice.waitForElementToLoad(_txtUsername);
		_txtUsername.clear();
		_txtUsername.sendKeys(username);
	}

	public void setPassword(String password) {
		appDevice.waitForElementToLoad(_txtPassword);

		_txtPassword.clear();
		_txtPassword.sendKeys(password);

	}

	public void clickOnLoginButton() {
		appDevice.waitForElementToLoad(_btnLogin);
		_btnLogin.click();
	}

	public void clickOnPerimssionAllowButton() {
		appDevice.waitForElementToLoad(_btnAllow);
		_btnAllow.click();
	}

	public void clickOnAcceptButton() {
		appDevice.waitForElementToLoad(_btnAccept);
		_btnAccept.click();
	}

	public void AcceptPopop() {
		driver.switchTo().alert().accept();
	}

	public void clickDismissButton() {
		System.out.println("TEST!@#$%");
//		System.out.println(appDevice.isElementDisplayed(_btnDismiss));
//		if (appDevice.isElementDisplayed(_btnDismiss)) {
//			_btnDismiss.click();
//		}
	}

	public void validLogin(String username, String password, String VIN) {

			driver.switchTo().alert().accept(); 
			setUsername(username);
			setPassword(password);
			chkKeepMeLogin.click();
			clickOnLoginButton();
			appDevice.waitForElementToLoadWithProvidedTime(_btnAccept, 2);
			if (appDevice.isElementDisplayed(_btnAccept)) {
				_btnAccept.click();
				appDevice.sleepFor(5000);
			//	Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3 ) {
					_btnAllowUsingAppPopup.click();
				} else {

				//	ele.accept();
				}
				appDevice.sleepFor(7000);
				//clickDismissButton();

			} else if (switchVehiclePage.isPresent()) {
				switchVehiclePage.selectVehicleByVIN(VIN);
				appDevice.sleepFor(4000);
				switchVehiclePage.clickOnAcceptButton();
				appDevice.sleepFor(4000);
				Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3) {
					_btnAllowUsingAppPopup.click();
				} else {

					ele.accept();
				}
				appDevice.sleepFor(7000);
				clickDismissButton();

			}
		System.out.println("Logged In with the user : " + username);
	}

	public void validLoginSelectDenyPopup(String username, String password, String VIN) {

			driver.switchTo().alert().accept();

			setUsername(username);
			setPassword(password);
			clickOnLoginButton();
			appDevice.waitForElementToLoadWithProvidedTime(_btnAccept, 2);
			if (appDevice.isElementDisplayed(_btnAccept)) {
				_btnAccept.click();
				appDevice.sleepFor(5000);
				Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3) {
					_btnDontUsingAppPopup.click();
				} else {

					ele.dismiss();
				}
				appDevice.sleepFor(5000);
				clickDismissButton();
			} else if (switchVehiclePage.isPresent()) {
				switchVehiclePage.selectVehicleByVIN(VIN);
				appDevice.sleepFor(4000);
				switchVehiclePage.clickOnAcceptButton();
				appDevice.sleepFor(4000);
				Alert ele = driver.switchTo().alert();
				System.out.println(_btnAllow1d.size());
				if (_btnAllow1d.size() == 3) {
					_btnDontUsingAppPopup.click();
				} else {
					ele.dismiss();
				}
				appDevice.sleepFor(5000);
				clickDismissButton();

			}
		System.out.println("Logged In with the user : " + username);
	}

	public void enterLoginCredentialPerformLogin(String username, String password) {

		try {

			driver.switchTo().alert().accept();

			setUsername(username);
			setPassword(password);

			clickOnLoginButton();

			appDevice.sleepFor(15000);

		} catch (NoSuchElementException e) {
			setUsername(username);
			setPassword(password);

			clickOnLoginButton();

			appDevice.sleepFor(9000);

		}

		catch (NoAlertPresentException ex) {
			ex.getStackTrace();
		}
	}

	public void navigateTermsAndConditionsPage(String username, String password) {

		try {

			driver.switchTo().alert().accept();

			setUsername(username);
			setPassword(password);

			clickOnLoginButton();

			appDevice.sleepFor(15000);
			clickBtnViewDetails();

		} catch (NoSuchElementException e) {
			setUsername(username);
			setPassword(password);

			clickOnLoginButton();

			appDevice.sleepFor(9000);
			clickBtnViewDetails();

		}

		catch (NoAlertPresentException ex) {
			ex.getStackTrace();
		}
	}

	public boolean labelTermsAndConditionsDisplayed() {
		appDevice.waitForElementToLoad(_labelTermsAndConditions);
		return _labelTermsAndConditions.isDisplayed();
	}

	public boolean isGenesisiAppAlertDisplayed() {
		appDevice.sleepFor(10000);
		try {
			driver.switchTo().alert();
			return alertGenesisiApp.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isLabelMyHyundaiDisplayed() {
		try {
			return labelMyHyundai.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public void clickCloseBtnGenesisApp() {
		btnCloseGenesisApp.click();

	}

	public void clickOpenBtnGenesisApp() {
		_btnLogin.click();
		btnOpenGenesisApp.click();

	}

}