package com.spa.hyundai.pageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class DashboardPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private static SwitchVehiclePage switchVehiclePage;

	private final int IMPLICIT_WAIT = 10;

	// Header Locators
	@iOSFindBy(accessibility = "home remote histroy")
	private MobileElement _iconRemoteHistory;

	@iOSFindBy(accessibility = "home notication")
	private MobileElement _iconNotification;

	@iOSFindBy(accessibility = "sdm navibar menu")
	private MobileElement _btnMenu;

	@iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='MyHyundai']//XCUIElementTypeTable")
	private MobileElement _panelMenu;

	@iOSFindBy(accessibility = "View Profile")
	private MobileElement _linkUserProfile;

	// Navigation Bars locators
	@iOSFindBy(accessibility = "Remote Actions")
	private MobileElement _btnRemoteActionsOption;

	@iOSFindBy(accessibility = "Maps")
	private MobileElement _btnMapOption;

	@iOSFindBy(accessibility = "Car Care")
	private MobileElement _btnCarCareOption;

	// Popup locators
	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOKRemoteMessage;

	@iOSFindBy(accessibility = "Message Center")
	private MobileElement lblMessageCenter;

	@iOSFindBy(accessibility = "home notication")
	private MobileElement iconHomeNotication;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Good')]")
	private MobileElement _labelWelcomeMessage;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'°')]/preceding-sibling:: XCUIElementTypeImage")
	private MobileElement _imgWeatherIcon;

	@iOSFindBy(accessibility = "home search icon")
	private MobileElement _btnSearch;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[starts-with(@name,'No')]")
	private MobileElement _txtNotfound;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='remote_statu_icon']/parent::XCUIElementTypeOther | //XCUIElementTypeStaticText[@name='Vehicle Status']/parent::XCUIElementTypeOther")
	private MobileElement _btnVehicleStatus;

	@iOSFindBy(accessibility = "home remote histroy")
	private MobileElement _btnRemoteHistory;

	@iOSFindBy(accessibility = "sdm navibar back")
	private MobileElement _btnNavigateBack;

	@iOSFindBy(accessibility = "POI Search")
	private MobileElement _linkSearchPoi;

	@iOSFindBy(accessibility = "Favorites")
	private MobileElement _linkMyPoi;

	@iOSFindBy(xpath = "//XCUIElementTypeTextField[@value='Search for a point of interest']")
	private MobileElement txtSearchPOI;

	@iOSFindBy(accessibility = "vehicle info refresh")
	private MobileElement _btnRefreshVehicleStatus;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='vehicle_schedule_time']/following-sibling::XCUIElementTypeStaticText")
	private MobileElement _txtDepartureTimeSet;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Remote Start']")
	private MobileElement _btnRemoteStart;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Plugged In')]")
	private MobileElement _txtPluggedInStatus;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Charging')]")
	private MobileElement _txtChargingStatus;

	@iOSFindBy(accessibility = "vehicle start charge")
	private MobileElement _btnStartCharge;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Last Updated')]")
	private MobileElement _txtLastUpdateStatusStatus;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Cancel']/ancestor::XCUIElementTypeAlert//XCUIElementTypeStaticText")
	private MobileElement txtGetPopupMessage;

	@iOSFindBy(accessibility = "Cancel")
	private MobileElement btnCancel;

	public DashboardPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
		switchVehiclePage = new SwitchVehiclePage(empAppDevice);
	}

	public boolean isMenuPanelDisplayed() {
		return _panelMenu.isDisplayed();
	}

	public boolean isUserProfileMenuSectionDisplayed() {
		return _linkUserProfile.isDisplayed();
	}

	// Menu functions

	public void openMenu() {

		appDevice.waitForElementToLoad(_btnMenu);
		_btnMenu.click();
		appDevice.waitForElementToLoad(_panelMenu);
	}

	public void navigateToUserProfile() {
		_linkUserProfile.click();
	}

	// Navigation bar methods
	public void clickRemoteActionsOption() {
		_btnRemoteActionsOption.click();

	}

	public void clickCarCareOption() {
		_btnCarCareOption.click();
	}

	public void clickMapsOption() {
		_btnMapOption.click();
	}

	// Popup method
	public String getRemoteResultPopUpMessageText(String remoteCommand) {

		String remoteMessage = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[contains(@name,'" + remoteCommand
				+ "')]";

		appDevice.sleepFor(10000);

//		if (_txtFaceId.isDisplayed()) {
//			_btnNotNowFaceId.click();
//		}

		appDevice.waitForElementToLoad(_btnOkPopUp);

		return driver.findElement(By.xpath(remoteMessage)).getText();

	}

	public void clickOnPopupOKButton() {
		appDevice.waitForElementToLoad(_btnOKRemoteMessage);
		if (appDevice.isElementDisplayed(_btnOKRemoteMessage)) {
			_btnOKRemoteMessage.click();
		}
	}

	public void clickOnRemoteHistoryIcon() {
		appDevice.waitForElementToLoad(_iconRemoteHistory);
		System.out.println("RemoteHistory Page");
		_iconRemoteHistory.click();

	}

	public void clickHomeNotificationButton() {
		iconHomeNotication.click();
	}

	public boolean labelMessageCenterDisplayed() {
		appDevice.waitForElementToLoad(lblMessageCenter);
		return lblMessageCenter.isDisplayed();
	}

	public String getWelcomeText() {
		return _labelWelcomeMessage.getAttribute("value");
	}

	public boolean isWeatherIconDisplayed() {
		return _imgWeatherIcon.isEnabled();
	}

	public boolean isVehicleNameDisplayed(String strVehicleName) {

		boolean flag = false;
		try {
			MobileElement vehicleName = driver
					.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + strVehicleName + "']"));
			if (vehicleName.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			e.getStackTrace();

		}
		return flag;

	}

	public void navigateToMapPageAndClickSearchPOI() {
		_btnMapOption.click();
		_linkSearchPoi.click();
		appDevice.sleepFor(5000);
	}

	public void clickOnVehicleStatusButton() {
		appDevice.waitForElementToLoad(_btnVehicleStatus);
		_btnVehicleStatus.click();

	}

	public void navigateToRemoteHistoryPage() {
		appDevice.waitForElementToLoad(_btnRemoteHistory);
		_btnRemoteHistory.click();

	}

	public boolean isVehicleStatusButtonDisplaye() {
		appDevice.waitForElementToLoad(_btnMapOption);
		return appDevice.isElementDisplayed(_btnVehicleStatus);
	}

	public boolean isVehicleStatusButtonDisplayeWithinExpectedTime(int i) {
		appDevice.waitForElementToLoadWithProvidedTime(_btnVehicleStatus, i);
		return appDevice.isElementDisplayed(_btnVehicleStatus);
	}

	public boolean VehicleStatusButtonDisplayStatusWithoutHandelException() {
		appDevice.waitForElementToLoadWithProvidedTime(_btnVehicleStatus, 3);
		return appDevice.isElementDisplayed(_btnVehicleStatus);

	}
	public void navigateToMapOptionPage() {
		appDevice.waitForElementToLoad(_btnMapOption);
		_btnMapOption.click();
		appDevice.sleepFor(10000);
	}

	public void clickNavigateBack() {
		_btnNavigateBack.click();

	}

	public void enterSearchText(String zip) {
		txtSearchPOI.sendKeys(zip);
		_btnSearch.click();
		try {
			appDevice.sleepFor(10000);

		} catch (ElementNotVisibleException e) {
			if (_txtNotfound.isDisplayed()) {
				_btnOkPopUp.click();

			} else
				System.out.println("Please try using different zip code");
		}

	}

	public boolean isbtnRefreshVehicleStatusDisplay() {
		appDevice.waitForElementToLoad(_btnRefreshVehicleStatus);

		return _btnRefreshVehicleStatus.isDisplayed();

	}

	public void navigateToChargeManagementScreen() {
		appDevice.waitForElementToLoad(_txtDepartureTimeSet);
		_txtDepartureTimeSet.click();
	}

	public String getRemoteSuccessMessageText(String remoteCommand) {

		String remoteMessage = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[contains(@name,'" + remoteCommand
				+ "')]";

		appDevice.sleepFor(10000);

		appDevice.waitForElementToLoad(_btnOkPopUp);

		return driver.findElement(By.xpath(remoteMessage)).getText();

	}

	public void clickOnRemoteStarttButton() {
		// _btnHomeOption.click();
		System.out.println(2);
		appDevice.sleepFor(5000);
		appDevice.waitForElementToLoad(_btnRemoteStart);
		System.out.println(3);
		_btnRemoteStart.click();
		appDevice.sleepFor(5000);
	}

	public String getPluggedInStatus() {
		return _txtPluggedInStatus.getText();
	}

	public String getChargingStatus() {
		return _txtChargingStatus.getText();
	}

	public void clickOnStartChargeButton() {
		_btnStartCharge.click();

	}

	public String getLatUpdateValue() {

		appDevice.waitForElementToLoad(_txtLastUpdateStatusStatus);
		return _txtLastUpdateStatusStatus.getAttribute("value");

	}

	public void clickRefreshIcon() {
		_btnRefreshVehicleStatus.click();
	}

	public String getPopupMessage() {
		appDevice.sleepFor(2);
		if (appDevice.isElementDisplayed(txtGetPopupMessage)) {
			return txtGetPopupMessage.getAttribute("name");
		} else {
			return "No Error Popup";
		}
	}

	public void clickCancelButton() {
		if (appDevice.isElementDisplayed(txtGetPopupMessage)) {
			btnCancel.click(); 
		}
	}

}