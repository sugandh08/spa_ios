package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class StartDrivingSettingsPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "Start Driving Settings")
	private MobileElement _txtTitle;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Start Driving 1']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _switchStartDriving1;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Start Driving 2']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _switchStartDriving2;

	@iOSFindBy(accessibility = "sdm btn save default")
	private MobileElement _btnSave;

	public StartDrivingSettingsPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isPresent() {
		appDevice.waitForElementToLoad(_txtTitle);

		return _txtTitle.isDisplayed();

	}

	public String getToggleValue(MobileElement element) {
		return element.getAttribute("value");
	}

	public void setStartDriving1Toggle(String value) {
		if (!(getToggleValue(_switchStartDriving1).equals(value))) {
			_switchStartDriving1.click();

		}
	}

	public void setStartDriving2Toggle(String value) {
		if (!(getToggleValue(_switchStartDriving2).equals(value))) {
			_switchStartDriving2.click();

		}
	}

	public void setStartDriving1States(String[] days) {

		appDevice.swipeScrollWheelUp("(//XCUIElementTypeTable)[1]", 3);
		appDevice.swipeScrollWheelDown("(//XCUIElementTypeTable)[2]", 2);
		appDevice.swipeScrollWheelDown("(//XCUIElementTypeTable)[3]", 1);

		for (String day : days) {
			if (day.equalsIgnoreCase("saturday") || day.equalsIgnoreCase("thursday")) {
				driver.findElement(
						By.xpath("(//XCUIElementTypeButton[@name='bl aeev edit sched " + day.charAt(0) + " btn'])[2]"))
						.click();

			} else {
				driver.findElement(
						By.xpath("(//XCUIElementTypeButton[@name='bl aeev edit sched " + day.charAt(0) + " btn'])[1]"))
						.click();

			}
		}
	}

	public void setStartDriving2States(String[] days) {

		appDevice.swipeScrollWheelUp("(//XCUIElementTypeTable)[4]", 4);
		appDevice.swipeScrollWheelDown("(//XCUIElementTypeTable)[5]", 3);
		appDevice.swipeScrollWheelDown("(//XCUIElementTypeTable)[6]", 1);

		for (String day : days) {
			if (day.equalsIgnoreCase("saturday") || day.equalsIgnoreCase("thursday")) {
				driver.findElement(
						By.xpath("(//XCUIElementTypeButton[@name='bl aeev edit sched " + day.charAt(0) + " btn'])[2]"))
						.click();

			} else {
				driver.findElement(
						By.xpath("(//XCUIElementTypeButton[@name='bl aeev edit sched " + day.charAt(0) + " btn'])[1]"))
						.click();

			}
		}
	}

	public void clickOnSavebutton() {
		_btnSave.click();
	}

}
