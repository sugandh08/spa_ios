package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class AboutAndSupportPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "About & Support")
	private MobileElement _txtAboutAndSupport;

	@iOSFindBy(accessibility = "sdm navibar back")
	private MobileElement _btnBack;

	// location Popup locator
	@iOSFindBy(accessibility = "Don’t Allow")
	private MobileElement _btnDontAllowLocationAccess;
	
	@iOSFindBy(xpath = "//XCUIElementTypeAlert")
	private MobileElement alertPopup;

	public AboutAndSupportPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isAboutAndSupportPageDisplayed() {
		appDevice.waitForElementToLoad(_txtAboutAndSupport);
		return _txtAboutAndSupport.isDisplayed();
	}

	private boolean isSectionExists(String sectionName) {
		appDevice.waitForElementToLoadWithProvidedTime(
				driver.findElement(By.xpath("//XCUIElementTypeOther[contains(@name,'" + sectionName + "')]")), 10);
		return driver.findElement(By.xpath("//XCUIElementTypeOther[contains(@name,'" + sectionName + "')]"))
				.isDisplayed();
	}

	public String getSubTitleText(String subTitleText) {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + subTitleText + "']")).getText();
	}

	public MobileElement textLinkName(String sectionName, String linkText) {

		isSectionExists(sectionName);

		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + linkText + "']"));
	}

	public void clickOnDontAllowCurrentLocation() {

		if (appDevice.isElementDisplayed(alertPopup)) {
			appDevice.waitForElementToLoad(_btnDontAllowLocationAccess);
			_btnDontAllowLocationAccess.click();
		}

		else {
			System.out.println("Current location popup is not displayed");
		}
	}

	public void clickOnBackButton() {
		_btnBack.click();
		appDevice.sleepFor(2000);
	}

	public boolean isBackButtonDisplay() {
		return appDevice.isElementDisplayed(_btnBack);
	}

}
