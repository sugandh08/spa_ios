package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class DealerLocatorPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;



	public DealerLocatorPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}



}
