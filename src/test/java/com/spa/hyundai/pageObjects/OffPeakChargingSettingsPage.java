package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class OffPeakChargingSettingsPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "Off-Peak Charging Settings")
	private MobileElement _txtTitle;

	@iOSFindBy(accessibility = "Start")
	private MobileElement _btnStart;

	@iOSFindBy(accessibility = "Stop")
	private MobileElement _btnStop;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='osev offpeak circle default'])[1]")
	private MobileElement _radioBtnOffPeakChargingPriority;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='osev offpeak circle default'])[2]")
	private MobileElement _radioBtnOffPeakChargingOnly;

	@iOSFindBy(accessibility = "sdm btn save default")
	private MobileElement _btnSave;

	public OffPeakChargingSettingsPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isPresent() {
		appDevice.waitForElementToLoad(_txtTitle);

		return _txtTitle.isDisplayed();

	}

	public void setOffPeakChargingTimeStates() {
		appDevice.swipeScrollWheelUp("(//XCUIElementTypeTable)[1]", 4);
		appDevice.swipeScrollWheelDown("(//XCUIElementTypeTable)[2]", 4);
		appDevice.swipeScrollWheelDown("(//XCUIElementTypeTable)[3]", 1);

	}

	public void clickOffPeakChargingPriorityRadioButton() {
		_radioBtnOffPeakChargingPriority.click();
	}

	public void clickOffPeakChargingOnlyRadioButton() {
		_radioBtnOffPeakChargingOnly.click();
	}

	public void clickOnSavebutton() {
		_btnSave.click();
	}

}
