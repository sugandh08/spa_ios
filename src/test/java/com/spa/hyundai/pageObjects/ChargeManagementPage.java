package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class ChargeManagementPage {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "Set Schedule")
	private MobileElement _txtTitle;

	@iOSFindBy(accessibility = "sdm navibar back")
	private MobileElement _btnBack;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Start Driving']/ancestor:: XCUIElementTypeOther/XCUIElementTypeButton[2]")
	private MobileElement _arrowStartDriving;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Climate']/ancestor:: XCUIElementTypeOther/XCUIElementTypeButton[2]")
	private MobileElement _arrowClimate;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Off-Peak Charging']/ancestor:: XCUIElementTypeOther/XCUIElementTypeButton[2]")
	private MobileElement _arrowOffPeakCharging;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Climate']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _switchClimate;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Off-Peak Charging']/ancestor:: XCUIElementTypeOther/XCUIElementTypeSwitch")
	private MobileElement _switchOffPeakCharging;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='DC Charge Limit']/ancestor:: XCUIElementTypeOther/XCUIElementTypeButton)[2]")
	private MobileElement _arrowDcChargeLimit;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='AC Charge Limit']/ancestor:: XCUIElementTypeOther/XCUIElementTypeButton)[2]")
	private MobileElement _arrowAcChargeLimit;

	@iOSFindBy(accessibility = "sdm btn submit default")
	private MobileElement _btnSubmit;

	@iOSFindBy(accessibility = "Save")
	private MobileElement _btnSavePopUp;

	@iOSFindBy(accessibility = "Save charge schedule?")
	private MobileElement _txtSavePopUp;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOk;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"Start Driving\"])[1]")
	private MobileElement linkStartDriving;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Submit Successfully']")
	private MobileElement txtSubmitSuccessfully;

	public ChargeManagementPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isSetScheduleTitleDisplay() {
		appDevice.sleepFor(60000);
		appDevice.waitForElementToLoad(_txtTitle);

		return _txtTitle.isDisplayed();

	}

	public String getToggleValue(MobileElement element) {
		return element.getAttribute("value");
	}

	public void setClimateToggle(String value) {
		if (!(getToggleValue(_switchClimate).equals(value))) {
			_switchClimate.click();

		}
	}

	public void setOffPeakChargingToggle(String value) {
		if (!(getToggleValue(_switchOffPeakCharging).equals(value))) {
			_switchOffPeakCharging.click();

		}
	}

	public void navigateBackToDashboard() {
		_btnBack.click();
	}

	public void navigateToStartDrivingSettingsPage() {
		_arrowStartDriving.click();
	}

	public void navigateToScheduledClimateSettingsPage() {
		_arrowClimate.click();
	}

	public void navigateToOffPeakChargingSettingsPage() {
		_arrowOffPeakCharging.click();
	}

	public void navigateToDcChargeLimitSettingsPage() {
		_arrowDcChargeLimit.click();
	}

	public void navigateToAcChargeLimitSettingsPage() {
		_arrowAcChargeLimit.click();
	}

	public void clickSubmitButton() {
		appDevice.waitForElementToLoad(_btnSubmit);
		_btnSubmit.click();
		if (_txtSavePopUp.isDisplayed()) {
			_btnSavePopUp.click();
		}
		appDevice.waitForElementToLoad(txtSubmitSuccessfully);
		_btnOk.click();
	}

}
