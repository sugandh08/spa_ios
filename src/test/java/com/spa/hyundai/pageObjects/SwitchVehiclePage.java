package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class SwitchVehiclePage {
	
	private BlueLinkAppDevice appDevice;
private AppiumDriver<MobileElement> driver;
	
	private final int IMPLICIT_WAIT = 10;
	
	
	@iOSFindBy(accessibility="Accept")
	private MobileElement _btnAccept;
	
	@iOSFindBy(accessibility="Select Vehicle")
	private MobileElement _txtTitle;
	
	
	
	
	
	public SwitchVehiclePage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	} 
	
	public boolean isSwitchVehiclePageDisplay()
	{
		appDevice.waitForElementToLoad(_txtTitle);
		return _txtTitle.isDisplayed(); 
	}
	
	public void selectVehicleByVIN(String VIN)
	
	{
	
		driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='VIN: "+ VIN+"']/ancestor:: XCUIElementTypeOther/XCUIElementTypeButton")).click();
		appDevice.sleepFor(2000);
	}
	
	public void clickOnAcceptButton()
	{
		appDevice.waitForElementToLoad(_btnAccept);
		_btnAccept.click();
	}
	
	
	public boolean isPresent()
	{
		return _txtTitle.getText().equals("Select Vehicle");
	}
	

}
