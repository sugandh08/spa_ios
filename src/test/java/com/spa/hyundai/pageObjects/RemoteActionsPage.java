package com.spa.hyundai.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class RemoteActionsPage {
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "menu close")
	private MobileElement _btnMenuClose;

	@iOSFindBy(accessibility = "Flash Lights")
	private MobileElement _btnFlashLights;

	@iOSFindBy(accessibility = "Horn and Lights")
	private MobileElement _btnHornAndLights;

	@iOSFindBy(accessibility = "Remote Unlock")
	private MobileElement _btnRemoteUnlock;

	@iOSFindBy(accessibility = "Remote Lock")
	private MobileElement _btnRemoteLock;

	@iOSFindBy(accessibility = "Remote Stop")
	private MobileElement _btnRemoteStop;

	@iOSFindBy(xpath = "//XCUIElementTypeCollectionView//XCUIElementTypeStaticText[@name='Remote Start']")
	private MobileElement _btnRemoteStart;
	


	public RemoteActionsPage(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public void clickOnHornAndLightsButton() {
		appDevice.waitForElementToLoad(_btnHornAndLights);
		_btnHornAndLights.click();
	}
	


	public void clickOnFlashLightsButton() {
		appDevice.waitForElementToLoad(_btnFlashLights);
		_btnFlashLights.click();
	}

	public void clickOnRemoteUnlockButton() {
		appDevice.waitForElementToLoad(_btnRemoteUnlock);
		_btnRemoteUnlock.click();
	}

	public void clickOnRemoteLockButton() {
		appDevice.waitForElementToLoad(_btnRemoteLock);
		_btnRemoteLock.click();
	}

	public void clickOnRemoteStopButton() {
		appDevice.waitForElementToLoad(_btnRemoteStop);
		_btnRemoteStop.click();
	}

	public void clickOnRemoteStartButton() { 
		System.out.println(5);
		appDevice.waitForElementToLoad(_btnRemoteStart);
		// appDevice.sleepFor(5000);
		System.out.println(6);
		_btnRemoteStart.click();
		
	}

}
