package com.spa.genesis.pageObjects;

import java.time.Duration;

import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class DigitalKeyPage_GIA {
	
	
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Digital Key\"]")
	private MobileElement _Pagetitle;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Set up Digital Key and your smartphone can take the place of your key fob.\"]")
	private MobileElement _textHeadertext;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\" \"]")
	private MobileElement _iconiInformation;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Easy to Use\"]")
	private MobileElement _textEasytoUse;
	
	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name=\"digitalkey_easyuse\"]")
	private MobileElement _iconEasytoUse;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Secure and Protected\"]")
	private MobileElement _textSecureandProtected;
	
	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name=\"digitalkey_sp\"]")
	private MobileElement _iconSecureandProtected;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Share the Convenience\"]")
	private MobileElement _textSharetheConvenience;
	
	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name=\"digital_sc\"]")
	private MobileElement _iconSharetheConvenience;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"Set up Digital Key \"]")
	private MobileElement _btnSetupDigitalKey;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Digital Key Activation\"]")
	private MobileElement _titleDigitalKeyActivation;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther//XCUIElementTypeStaticText[@name='Learn more about Digital Key technologies']")
	private MobileElement _linkLearnMore;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Cancel set up and return to Dashboard\"]")
	private MobileElement _linkCancelSetup;
	
	//Digital Key Help page
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Digital Key Help\"]")
	private MobileElement _PageTitleDigitalkeyhelp;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeOther[1]//XCUIElementTypeStaticText[@name=\"Watch Digital Key Video\"]")
	private MobileElement _txtWatchDigitalKeyVideo;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeButton")
	private MobileElement _btnWatchDigitalKeyVideo;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Phone Support\"]")
	private MobileElement _txtPhoneSupport;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeButton")
	private MobileElement _btnPhoneSupport;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Digital Key FAQ\"]")
	private MobileElement _txtDigitalKeyFAQ;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeButton")
	private MobileElement _btnDigitalKeyFAQ;
	
	
	public DigitalKeyPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	
	public boolean isPageTitleDigitalkeyhelpDisplayed() {
		return appDevice.isElementDisplayed(_PageTitleDigitalkeyhelp);
	}
	
	public boolean istxtWatchDigitalKeyVideoDisplayed() {
		appDevice.waitForElementToLoad(_txtWatchDigitalKeyVideo);
		return appDevice.isElementDisplayed(_txtWatchDigitalKeyVideo);
	}
	
	public boolean istxtPhoneSupportDisplayed() {
		return appDevice.isElementDisplayed(_txtPhoneSupport);
	}
	
	public boolean istxtDigitalKeyFAQDisplayed() {
		return appDevice.isElementDisplayed(_txtDigitalKeyFAQ);
	}
	
	public void clickWatchDigitalKeyVideo() {
		appDevice.waitForElementToLoad(_btnWatchDigitalKeyVideo);
		_btnWatchDigitalKeyVideo.click();
	}
	
	public void clickPhoneSupport() {
		appDevice.waitForElementToLoad(_btnPhoneSupport);
		_btnPhoneSupport.click();
	}
	
	public void clickDigitalKeyFAQ() {
		appDevice.waitForElementToLoad(_btnDigitalKeyFAQ);
		_btnDigitalKeyFAQ.click();
	}	

	public boolean isPageTitleDisplayed(String title) {
		appDevice.waitForElementToLoad(_Pagetitle);
		return appDevice.isElementDisplayed(_Pagetitle);
	}
	
	public boolean isPageHeaderTextDisplayed() {
		return appDevice.isElementDisplayed(_textHeadertext);
	}
	
	public boolean isIconiDisplayed() {
		return appDevice.isElementDisplayed(_iconiInformation);
	}
	
	
	public boolean isLearnMoreaboutDigitalKeyisDisplayed() {
		appDevice.waitForElementToLoad(_linkLearnMore);
		return appDevice.isElementDisplayed(_linkLearnMore);
	}
	
	public boolean isCancelSetupandReturnToDashboardisDisplayed() {
		return appDevice.isElementDisplayed(_linkCancelSetup);
	}
	
	
	public boolean isEasyToUseIconandTextDisplayed() {
		
		if(_textEasytoUse.isDisplayed()) {
			if(_iconEasytoUse.isDisplayed()) {
				return true;
			}
			return false;
		}
		return false;	
	}
	
	public boolean isSecureandProtectedIconandTextDisplayed() {
		
		if(_textSecureandProtected.isDisplayed()) {
			if(_iconSecureandProtected.isDisplayed()) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	public boolean isSharetheConvenienceIconandTextDisplayed() {
		
		if(_textSharetheConvenience.isDisplayed()) {
			if(_iconSharetheConvenience.isDisplayed()) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	
	
	public boolean isDigitalKeyActivationPageisDisplayed(String title) {
		return appDevice.isElementDisplayed(_titleDigitalKeyActivation);
	}
	
	
	public void clickSetupDigitalKey() {
		appDevice.waitForElementToLoad(_btnSetupDigitalKey);
		_btnSetupDigitalKey.click();
		appDevice.waitForElementToLoad(_titleDigitalKeyActivation);
		appDevice.isElementDisplayed(_titleDigitalKeyActivation);
	}
	
	public void clickIconi() {
		appDevice.waitForElementToLoad(_iconiInformation);
		_iconiInformation.click();
	}

}
