package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.HowToUseLocators;
import io.appium.java_client.pagefactory.LocatorGroupStrategy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class CarCarePage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	public CarCarePage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='MVR']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='MVR']")
	private MobileElement _linkMVR;

	@AndroidFindBy(xpath = "(//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1])[1]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Diagnostic Report']")
	private MobileElement _linkDiagnosticReport;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Diagnostics']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Diagnostics']")
	private MobileElement _lnkDiagnostics;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Schedule Service']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Schedule Service']")
	private MobileElement _lnkScheduleService;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Connecting Bluetooth']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Connecting Bluetooth']")
	private MobileElement _linkBluetooth;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Owners Manual']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Owners Manual\"]")
	private MobileElement linkOwnersManual;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Roadside Assistance']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Roadside Assistance']")
	private MobileElement linkRoadSideAssistance;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='MVR']")
	@iOSFindBy(xpath = "///XCUIElementTypeStaticText[@name='MONTHLY VEHICLE REPORT']")
	private MobileElement _lblMVR;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='HISTORY']")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='HISTORY']")
	private MobileElement _tabMVRHistory;

	@AndroidFindBy(xpath = "//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]")
	private MobileElement _MVRhistorydetail;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='DIAGNOSTIC']")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='DIAGNOSTIC']")
	private MobileElement _tabMVRDiagnostic;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='SYSTEMS NORMAL!']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='systems normal!']")
	private MobileElement _allSystemNormal;

	@AndroidFindBy(id = "tvScheduleService")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"SCHEDULE SERVICE\"]")
	private MobileElement btnScheduleService;

	@AndroidFindBy(id = "tvViewVehicleHealth")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"VIEW VEHICLE HEALTH\"]")
	private MobileElement btnViewVehicleHealth;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Would you like to exit the app and call Roadside Assistance?']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Would you like to exit the app and call Roadside Assistance?']")
	private MobileElement alertRoadSide;

	@AndroidFindBy(xpath = "//android.widget.Button[@text='Cancel']")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Cancel']")
	private MobileElement btnCancel;

	@AndroidFindBy(xpath = "//android.widget.Button[@text='Call']")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Call']")
	private MobileElement btnCall;

	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Downloading')]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Downloading...\"]")
	private MobileElement alertDownloading;

	@AndroidFindBy(xpath = "//android.widget.Button[@text='OK']")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='OK']")
	private MobileElement btnOK;

	@HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE, iOSAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Connecting Bluetooth')]")
	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'GENESIS BLUETOOTH & MULTIMEDIA SUPPORT')]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Connecting Bluetooth']")
	private MobileElement lblBluetooth;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='VEHICLE HEALTH']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"VEHICLE HEALTH\"]")
	private MobileElement lblVehicleHealth;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='No Previous Vehicle Report Available']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='No Previous Vehicle Report Available']")
	private MobileElement txtNoPreviousVehicle;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='No Monthly Vehicle Report Available']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='No Monthly Vehicle Report Available']")
	private MobileElement txtNoMonthlyReport;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Done\"]")
	private MobileElement txtDone;

	public void clickMVR() {
		_linkMVR.click();

	}

	public void clickDiagnosticReport() {
		_linkDiagnosticReport.click();
	}

	public void clickCancelRaodside() {
		btnCancel.click();

	}

	public boolean isDonedisplayed() {
		appDevice.waitForElementToLoad(txtDone);
		return txtDone.isDisplayed();
	}

	public boolean isBluetoothLabelDisplayed() {
		appDevice.waitForElementToLoad(lblBluetooth);
		return lblBluetooth.isDisplayed();
	}

	public boolean isAlertRoadsideDisplayed() {
		appDevice.waitForElementToLoad(alertRoadSide);
		return alertRoadSide.isDisplayed();

	}

	public boolean isAlertBtnCallRoadsideDisplayed() {
		return btnCall.isDisplayed();

	}

	public boolean isAlertBtnCancelRoadsideDisplayed() {
		return btnCancel.isDisplayed();

	}

	public boolean isAlertOkDisplayed() {
		return btnOK.isDisplayed();
	}

	public boolean isAlertDownloadDisplayed() {
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("android")) {
			return false;
		}
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("IOS")) {
		appDevice.waitForElementToLoad(alertDownloading);
		return alertDownloading.isDisplayed();
		}
		return false;
	}

	public void clickOkDownload() {
		btnOK.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickBluetooth() {
		// appDevice.waitForElementToLoad(_linkBluetooth);
		// locateSection("Connecting Bluetooth");
		appDevice.swipeUpScreen(4);
		_linkBluetooth.click();
	}

	public void clickOwnerManual() {
		appDevice.swipeUpScreen(3);
		linkOwnersManual.click();
	}

	public void clickRoadSideAssistaence() {
		linkRoadSideAssistance.click();

	}

	public void clickScheduleService() {
		_lnkScheduleService.click();

	}

	public void clickDiagnostics() {
		_lnkDiagnostics.click();

	}

	public boolean isMonthlyVehicleReportPageDisplayed() {
		appDevice.waitForElementToLoad(_lblMVR);
		return _lblMVR.isDisplayed();

	}

	public void clickMVRHistory() {
		_tabMVRHistory.click();

	}

	public void clickMVRDiagnostics() {
		_tabMVRDiagnostic.click();

	}

	public boolean isAllSystemNormalDisplayed() {
		appDevice.waitForElementToLoad(_allSystemNormal);
		return _allSystemNormal.isDisplayed();
	}

	public boolean isMVRHistoryDisplayed() {
		if (_MVRhistorydetail.isDisplayed()) {
			return true;
		} else if (txtNoPreviousVehicle.isDisplayed()) {
			return true;
		} else
			return false;
	}

	public boolean isbtnScheduleServiceDisplayed() {
		appDevice.waitForElementToLoad(btnScheduleService);
		return btnScheduleService.isDisplayed();
	}

	public boolean isViewVehicleHealthDisplayed() {
		appDevice.waitForElementToLoad(_lblMVR);
		return btnViewVehicleHealth.isDisplayed();
	}

	public void clickBtnScheduleService() {
		btnScheduleService.click();
	}

	public void clickBtnViewVehicleHealth() {
		btnViewVehicleHealth.click();
		appDevice.sleepFor(500);
		appDevice.waitForElementToLoad(btnOK);
		// if(btnOK.isDisplayed()) { btnOK.click();}
	}

	public boolean isVehicleHealthLabelDisplayed() {
		if (isAlertOkDisplayed()) {
			btnOK.click();
		}

		appDevice.sleepFor(3000);
		appDevice.waitForElementToLoad(lblVehicleHealth);
		return lblVehicleHealth.isDisplayed();

	}

	public void locateSection(String text) {
		appDevice.scrollByText(text);
	}
}
