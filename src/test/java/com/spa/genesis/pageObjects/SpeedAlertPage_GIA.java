package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindAll;
import io.appium.java_client.pagefactory.iOSFindBy;

public class SpeedAlertPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	public SpeedAlertPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	@iOSFindBy(accessibility = "SPEED ALERT")
	private MobileElement _txtTitle; 

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'mph')]")
	private MobileElement _txtSpeedSelect;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'mph')]//parent::XCUIElementTypeOther")
	private MobileElement _txtSpeedSelectButton;

	@iOSFindBy(accessibility = "save button")
	private MobileElement _btnSave;

	@iOSFindBy(accessibility = "Successfully Saved.")
	private MobileElement _popUpSave;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	public boolean isPresent() {
		appDevice.waitForElementToLoad(_txtTitle);
		return _txtTitle.isDisplayed();
	}

	public String getSpeedValue(){
		return _txtSpeedSelect.getAttribute("value");
	}
	
	public void setSpeedValue(String value) {

		_txtSpeedSelectButton.click();
		driver.findElement(By.xpath("//XCUIElementTypePicker/XCUIElementTypePickerWheel")).setValue(value);
		driver.findElement(By.xpath("//XCUIElementTypeButton[@name='Done']")).click();

	}

	public void clickOnSaveButton() {

		_btnSave.click();
		appDevice.waitForElementToLoad(_btnOkPopUp);
		_btnOkPopUp.click();
		appDevice.sleepFor(3000);

	}

}
