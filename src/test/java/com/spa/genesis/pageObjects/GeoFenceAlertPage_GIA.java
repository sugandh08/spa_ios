package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class GeoFenceAlertPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;

	public GeoFenceAlertPage_GIA(BlueLinkAppDevice empAppDevice) { 
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	@iOSFindBy(accessibility = "GEO-FENCE ALERT")
	private MobileElement _txtTitle;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='icn_search_brown']/ancestor:: XCUIElementTypeOther/XCUIElementTypeTextField")
	private MobileElement _searchBox;

	@iOSFindBy(accessibility = "map-searchbar")
	private MobileElement _btnsearch;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Inclusive']/preceding-sibling::XCUIElementTypeTextField)[1]")
	private MobileElement _txtGeoFenceName;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Inclusive']/preceding-sibling::XCUIElementTypeTextField)[2]")
	private MobileElement _txtGeoFenceMiles;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='unselected button'])[1]")
	private MobileElement _radioButtonInclusive;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='unselected button'])[2]")
	private MobileElement _radioButtonExclusive;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='unselected button'])[3]")
	private MobileElement _radioButtonCircle;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='unselected button'])[4]")
	private MobileElement _radioButtonRectangle;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Done']")
	private MobileElement _btnDone;

	@iOSFindBy(accessibility = "save button")
	private MobileElement _btnSave;

	@iOSFindBy(accessibility = "Successfully Saved.")
	private MobileElement _popUpSave;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindBy(accessibility = "delete button")
	private MobileElement btnDelete;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Delete']")
	private MobileElement btnDeletePopUp;

	public boolean isPresent() {
		appDevice.waitForElementToLoad(_txtTitle);
		return _txtTitle.isDisplayed();
	}

	public void setGeoFenceAlert(String zipCode, String name, String miles) {
		_searchBox.clear();
		_searchBox.sendKeys(zipCode);
		appDevice.sleepFor(5000);
		_btnDone.click();
		appDevice.sleepFor(5000);

		_txtGeoFenceName.clear();
		_txtGeoFenceName.sendKeys(name);
		_btnDone.click();

		_txtGeoFenceMiles.clear();
		_txtGeoFenceMiles.sendKeys(miles);
		_btnDone.click();

		_radioButtonExclusive.click();
		_radioButtonRectangle.click();
	}

	public void editGeoFenceAlert(String name) {
		_txtGeoFenceName.clear();
		_txtGeoFenceName.sendKeys(name);
		_btnDone.click();
	}

	public void clickOnSaveButton() {

		_btnSave.click();
		appDevice.sleepFor(10000);
		_btnOkPopUp.click();
		appDevice.sleepFor(3000);

	}

	public boolean selectGeoFenceDisplay(String geoFenence) {
		boolean flag = false;
		try {
			MobileElement GeoFenanceData = driver
					.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + geoFenence + "']"));
			return GeoFenanceData.isDisplayed();
		} catch (Exception e) {
			return flag;
		}
	}

	public void selectAndDeleteGeoFence(String geoFenence) {
		if (selectGeoFenceDisplay(geoFenence)) {
			MobileElement GeoFenanceData = driver
					.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + geoFenence + "']"));
			GeoFenanceData.click();
			deleteSelectedCurfew();
		}

	}

	public void deleteSelectedCurfew() {
		btnDelete.click();
		btnDeletePopUp.click();
		_btnOkPopUp.click();
	}

}
