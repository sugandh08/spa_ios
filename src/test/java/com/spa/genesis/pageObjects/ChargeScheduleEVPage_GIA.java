package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;
import junit.framework.Assert;

public class ChargeScheduleEVPage_GIA {
	
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;
	
	public ChargeScheduleEVPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}
	
	//Ev page Genesis GV60
	@AndroidFindBy(xpath="//android.widget.ScrollView/android.view.ViewGroup/android.widget.ImageView[1]")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"ev icrefresh\"]")
	private MobileElement _linkrefreshbutton;
	
	@AndroidFindBy(xpath="//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]")
	private MobileElement _linkVehicleImage;
	
	@AndroidFindBy(xpath="//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Not Plugged In']")
	private MobileElement _linkVehicleNotPluggedin;
	
	@AndroidFindBy(xpath="//android.widget.ScrollView/android.view.ViewGroup/android.widget.LinearLayout[2]")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]")
	private MobileElement _linkBatterypercentage;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeButton")
	private MobileElement _linkStartChargeButton;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText")
	private MobileElement _linkStartStopChargetxt;
	
	@AndroidFindBy(xpath="//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[1]")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeButton")
	private MobileElement _linkChargeStation;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[4]/XCUIElementTypeOther[3]/XCUIElementTypeButton")
	private MobileElement _linkChargeLimitSettings;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeButton")
	private MobileElement _linkChargeSchedules;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='CHARGE COMPLETION ALERT']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[4]/XCUIElementTypeOther[4]/XCUIElementTypeButton")
	private MobileElement _linkChargeCompletionAlert;
	
	//Charge Limit settings page
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='DC CHARGE LIMIT']")
	private MobileElement _linkDCChargeLimit;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='AC CHARGE LIMIT']")
	private MobileElement _linkACChargeLimit;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[1]")
	private MobileElement _linkDCChargeLimitValue;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[1]")
	private MobileElement _linkACChargeLimitValue;
	
	
	//ChargeCompletion alert page
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Set Timed Alert']")
	private MobileElement _linkSetTimedAlert;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='APPLY']")
	private MobileElement _linkApplyBtn;

	//Charge Schedules Page
	@AndroidFindBy(xpath="//android.widget.TextView[@resource-id='com.stationdm.genesis:id/tvEVTitle']")
	private MobileElement _linkEVpageheadertittle;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]")
	private MobileElement _linkDepartureTimebtn;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"ev question\"])[1]")
	private MobileElement _linkDepartureTimeicon;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Departure Time is based on vehicle pre-conditioning and charge optimization for off-peak priority.']")
	private MobileElement _linkDepartureTimeicontxt;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeSwitch")
	private MobileElement _linkDeparture1toggle;

	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[2]")
	private MobileElement _linkDeparture1Days;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeSwitch")
	private MobileElement _linkDeparture2toggle;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeStaticText[2]")
	private MobileElement _linkDeparture2Days;

	//Climate Setting
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeSwitch")
	private MobileElement _linkClimatetoggle;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"ev question\"])[2]")
	private MobileElement _linkClimateicon;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Climate will be activated for vehicle pre-conditioning prior to departure time.']")
	private MobileElement _linkClimateicontxt;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]")
	private MobileElement _linkGotoClimatesettingpage;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeSwitch")
	private MobileElement _linkClimateFrontDefrostToggle;
	
	//Off-peak Charging Settings
	@AndroidFindBy(xpath="//android.widget.TextView[@text='off-peak charging']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='OFF-PEAK CHARGING']")
	private MobileElement _linkOffPeakCharging;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"ev question\"])[3]")
	private MobileElement _linkOffPeakChargingicon;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]")
	private MobileElement _linkGotoOffPeakChargingPage;

	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Off-peak charging electricity rates are times of the day you can use your energy at a reduced cost.']")
	private MobileElement _linkOffPeakChargingicontxt;
	
	@AndroidFindBy(xpath="//android.widget.Switch[@resource-id='com.stationdm.genesis:id/switchOffPeak']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeSwitch")
	private MobileElement _linkOffPeakChargingToggle;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[5]")
	private MobileElement _linkOffPeakChargingPriority;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='SUBMIT']")
	private MobileElement _linkSUBMITbtn;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='arrow back']")
	private MobileElement _linkNavigateBackbtn;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Save']")
	private MobileElement _linkSavebtn;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"1\"])[2]")
	private MobileElement _linkScheduleEndTimehourwheel;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"00\"])[2]")
	private MobileElement _linkScheduleEndTimemintwheel;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"AM\"])[2]")
	private MobileElement _linkScheduleEndTimeAMPMwheel;
			
	//Off-Peak Charging Settings Page
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"ev charge circle unselected\"])[1]")
	private MobileElement _linkOffPeakChargingPrioritybtn;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"ev charge circle unselected\"])[2]")
	private MobileElement _linkOffPeakChargingOnlybtn;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='SAVE']")
	private MobileElement _linkOffPeakSavebtn;
	
	@iOSFindBy(xpath= "//XCUIElementTypeStaticText[@name='Vehicle will immediately start charging when plugged in.']")
	private MobileElement _VehicleimmediatelyChargeMessage;
	
	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOKRemoteMessage;
	
	//Departure Time settings
	@AndroidFindBy(xpath="//android.widget.TextView[@text='departure time 1']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='DEPARTURE TIME 1']")
	private MobileElement _linkdeparturetime1;
	
	@AndroidFindBy(xpath="//android.widget.Switch[@resource-id='com.stationdm.genesis:id/switchSchedule1']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeSwitch")
	private MobileElement _linktoggleDeparture1;

	@AndroidFindBy(xpath="//android.widget.TextView[@text='departure time 2']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='departure time 2']")
	private MobileElement _linkdeparturetime2;

	@AndroidFindBy(xpath="//android.widget.Switch[@resource-id='com.stationdm.genesis:id/switchSchedule2']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeSwitch")
	private MobileElement _linktoggleDeparture2;
	

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Mon'])[1]")
	private MobileElement _linkMonDeparture1;
	
	
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Mon'])[2]")
	private MobileElement _linkMonDeparture2;
	

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Tue'])[1]")
	private MobileElement _linkTuesDeparture1;
	
	
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Tue'])[2]")
	private MobileElement _linkTuesDeparture2;
	
	
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Wed'])[1]")
	private MobileElement _linkWedDeparture1;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Wed'])[2]")
	private MobileElement _linkWedDeparture2;
	

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Thu'])[1]")
	private MobileElement _linkThuDeparture1;
	

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Thu'])[2]")
	private MobileElement _linkThuDeparture2;
	
	
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Fri'])[1]")
	private MobileElement _linkFriDeparture1;
	
	
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Fri'])[2]")
	private MobileElement _linkFriDeparture2;
	
	@AndroidFindBy(xpath="")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Sat'])[1]")
	private MobileElement _linkSatDeparture1;
	
	
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Sat'])[2]")
	private MobileElement _linkSatDeparture2;
	
	
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Sun'])[1]")
	private MobileElement _linkSunDeparture1;
	

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Sun'])[2]")
	private MobileElement _linkSunDeparture2;
	
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='SAVE']")
	private MobileElement _linkDepartureSavebtn;
	

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='SAVE']")
	private MobileElement _linkClimateSavebtn;
	
	public boolean isVehicleImageDisplayed() {
		return appDevice.isElementDisplayed(_linkVehicleImage);
	}
	
	public boolean isBatteryPercentageDisplayed() {
		return appDevice.isElementDisplayed(_linkBatterypercentage);
	}
	
	public boolean isRefreshButtonDisplayed() {
		return appDevice.isElementDisplayed(_linkrefreshbutton);
	}
	
	public boolean isChargeCompletionAlertDisplayed() {
		appDevice.swipeUpScreen(2);
		appDevice.waitForElementToLoad(_linkChargeCompletionAlert);
		return _linkChargeCompletionAlert.isDisplayed();
	}
	
	public boolean isEvPageTitleDisplayed(){
		appDevice.waitForElementToLoad(_linkEVpageheadertittle);
		return _linkEVpageheadertittle.isDisplayed();
	}
	
	public boolean isChargeStationDisplayed() {
		appDevice.waitForElementToLoad(_linkChargeStation);
		return appDevice.isElementDisplayed(_linkChargeStation);
	}
	
	public boolean isChargeLimitSettingsDisplayed() {
		appDevice.waitForElementToLoad(_linkChargeLimitSettings);
		return appDevice.isElementDisplayed(_linkChargeLimitSettings);
	}
	
	public boolean isDCChargeLimitDisplayed() {
		appDevice.waitForElementToLoad(_linkDCChargeLimit);
		return appDevice.isElementDisplayed(_linkDCChargeLimit);
	}
	
	public boolean isACChargeLimitDisplayed() {
		appDevice.waitForElementToLoad(_linkACChargeLimit);
		return appDevice.isElementDisplayed(_linkACChargeLimit);
	}

	public boolean isDepartureTimeOneDisplayed() {
		return appDevice.isElementDisplayed(_linkdeparturetime1);
	}
	
	public boolean isDepartureTimeOneToggleDisplayed() {
		return appDevice.isElementDisplayed(_linktoggleDeparture1);
	}
	
	public boolean isDepartureTimeTwoDisplayed() {
		return appDevice.isElementDisplayed(_linkdeparturetime2);
	}
	
	public boolean isDepartureTimeTwoToggleDisplayed() {
		return appDevice.isElementDisplayed(_linktoggleDeparture2);
	}
	
	public boolean isDepartureTimeicontextDisplayed() {
		return appDevice.isElementDisplayed(_linkDepartureTimeicontxt);
	}
	
	public boolean isClimateicontextDisplayed() {
		return appDevice.isElementDisplayed(_linkClimateicontxt);
	}
	
	public boolean isOffPeakChargingicontextDisplayed() {
		return appDevice.isElementDisplayed(_linkOffPeakChargingicontxt);
	}
	
	public boolean isOffpeakChargingdisplayed() {
		appDevice.swipeUpScreen(2);
		return appDevice.isElementDisplayed(_linkOffPeakCharging);
	}
	
	public boolean isOffpeakChargingtoggledisplayed() {
		return appDevice.isElementDisplayed(_linkOffPeakChargingToggle);
	}
	
	public boolean isSetTimedAlertdisplayed() {
		return appDevice.isElementDisplayed(_linkSetTimedAlert);
	}
	
	public boolean isOkbuttonDisplayed() {
		return appDevice.isElementDisplayed(_btnOKRemoteMessage);
	}
	
	public boolean isVehicleimmediatechargingDisplayed() {
		return appDevice.isElementDisplayed(_VehicleimmediatelyChargeMessage);
	}
	
	public boolean isStartStopChargeButtonDisplayed() {
		return appDevice.isElementDisplayed(_linkStartStopChargetxt);
	}
	
	public boolean isVehicleNotPluggedInDisplayed() {
		return appDevice.isElementDisplayed(_linkVehicleNotPluggedin);
	}
	
	public void ClickRefreshbutton() {
		_linkrefreshbutton.click();
	}
	
	public void ClickStartChargebutton() {
		_linkStartChargeButton.click();
	}
	
	public void ClickChargeSchedule() {
		_linkChargeSchedules.click();
	}
	
	public void ClickChargeStation() {
		_linkChargeStation.click();
	}
	
	public void ClickChargelimitSettings() {
		appDevice.swipeup(1);
		_linkChargeLimitSettings.click();
	}
	
	
	public void ClickArrowBackbtn() {
		_linkNavigateBackbtn.click();
	}
	
	public void ClickDepartureTime() {
		_linkDepartureTimebtn.click();
	}
	
	public void ClickDepartureTimeicon() {
		_linkDepartureTimeicon.click();
	}
	
	public void ClickClimateicon() {
		_linkClimateicon.click();
	}
	
	public void ClickOffPeakChargingicon() {
		_linkOffPeakChargingicon.click();
	}
	
	public void ClickChargeCompletionAlert() {
		_linkChargeCompletionAlert.click();
	}
	
	public void ClickApplyBtn() {
		_linkApplyBtn.click();
	}
	
	public void ClickSubmitBtn() {
		appDevice.swipeUpScreen(1);
		_linkSUBMITbtn.click();
	}
	
	public void ClickSaveBtn() {
		_linkSavebtn.click();
	}
	
	public void ClickDepartureSaveBtn() {
		_linkDepartureSavebtn.click();
	}
	
	public void ClickClimateSaveBtn() {
		_linkClimateSavebtn.click();
	}
	
	public void ClickoffPeakChargingToggle() {
		_linkOffPeakChargingToggle.click();
	}
	
	public void ClickDeparture1toggle() {
		_linkDeparture1toggle.click();
	}
	
	public void ClickDeparture2toggle() {
		_linkDeparture2toggle.click();
	}
	
	public void ClickClimatetoggle() {
		_linkClimatetoggle.click();  	
	}
	
	public void GotoClimateScreen() {
		_linkGotoClimatesettingpage.click();
	}
	
	public void ClickClimateFrontDefrostToggle() {
		_linkClimateFrontDefrostToggle.click();
	}
	
	public void ClickOffPeakCharging() {
		_linkGotoOffPeakChargingPage.click();
	}
	
	public void ClickOffPeakChargingPrioritybtn() {
		_linkOffPeakChargingPrioritybtn.click();
	}
	public void ClickOffPeakChargingOnlybtn() {
		_linkOffPeakChargingOnlybtn.click();
	}
	public void ClickOffPeakSavebtn() {
		appDevice.swipeUpScreen(1);
		_linkOffPeakSavebtn.click();
	}
	
	public void ClickDTime1toggle() {
		_linktoggleDeparture1.click();
	}
	
	public void ClickDTime2toggle() {
		_linktoggleDeparture2.click();
	}
	
	public void ClickSundayDTime1() {
		_linkSunDeparture1.click();
	}
	
	public void ClickMondayDTime1() {
		_linkMonDeparture1.click();
	}
	
	public void ClickTuesdayDTime1() {
		_linkTuesDeparture1.click();
	}
	
	public void ClickWeddayDTime1() {
		_linkWedDeparture1.click();
	}
	
	public void ClickThursdayDTime1() {
		_linkThuDeparture1.click();
	}
	
	public void ClickFridayDTime1() {
		_linkFriDeparture1.click();
	}
	
	public void ClickSaturdayDTime1() {
		_linkSatDeparture1.click();
	}
	
	public void ClickSundayDTime2() {
		_linkSunDeparture2.click();
	}
	
	public void ClickMondayDTime2() {
		_linkMonDeparture2.click();
	}
	
	public void ClickTuesdayDTime2() {
		_linkTuesDeparture2.click();
	}
	
	public void ClickWeddayDTime2() {
		_linkWedDeparture2.click();
	}
	
	public void ClickThursdayDTime2() {
		_linkThuDeparture2.click();
	}
	
	public void ClickFridayDTime2() {
		_linkFriDeparture2.click();
	}
	
	public void ClickSaturdayDTime2() {
		_linkSatDeparture2.click();
	}
	
	public String GetDeparture1DaysValue() {
		return _linkDeparture1Days.getAttribute("value");
	}
	
	public String GetDeparture2DaysValue() {
		return _linkDeparture2Days.getAttribute("value");
	}
	
	public String GetStartStopChargeTextValue() {
		return _linkStartStopChargetxt.getAttribute("value");
	}
	
	public String GetDTime1ToggleValue() {
		return _linktoggleDeparture1.getAttribute("value");
	}
	
	public String GetClimateFrontDefrostToggleValue() {
		return _linkClimateFrontDefrostToggle.getAttribute("value");
	}
	
	public String GetDTime2ToggleValue() {
		return _linktoggleDeparture2.getAttribute("value");
	}
	
	public String GetDepartureTime1ToggleValue() {
		appDevice.swipeUpScreen(1);
		return _linkDeparture1toggle.getAttribute("value");
	}
	
	public String GetDepartureTime2ToggleValue() {
		appDevice.swipeUpScreen(1);
		return _linkDeparture2toggle.getAttribute("value");
	}
	
	public String GetEndTimeHourValue() {
		appDevice.swipeUpScreen(1);
		return _linkScheduleEndTimehourwheel.getAttribute("value");
	}
	
	public String GetEndTimeMinuteValue() {
		appDevice.swipeUpScreen(1);
		return _linkScheduleEndTimemintwheel.getAttribute("value");
	}
	
	public String GetEndTimeAMPMValue() {
		appDevice.swipeUpScreen(1);
		return _linkScheduleEndTimeAMPMwheel.getAttribute("value");
	}
	
	public String GetOffpeakChargingPriorityValue() {
		appDevice.swipeUpScreen(1);
		return _linkOffPeakChargingPrioritybtn.getAttribute("value");
	}
	
	public String GetOffpeakChargingSettingValue() {
		appDevice.swipeUpScreen(1);
		return _linkOffPeakChargingPriority.getAttribute("value");
	}
	
	public String GetOffpeakChargingToggleValue() {
		appDevice.swipeUpScreen(1);
		return _linkOffPeakChargingToggle.getAttribute("value");
	}
	
	public String GetDeparturetime1ToggleValue() {
		appDevice.swipeDownScreen(1);
		return _linkDeparture1toggle.getAttribute("value");
	}
	
	public String GetDeparturetime2ToggleValue() {
		return _linkDeparture2toggle.getAttribute("value");
	}
	
	public String GetClimateToggleValue() {
		appDevice.swipeUpScreen(1);
		return _linkClimatetoggle.getAttribute("value");
	}
	
	public String GetDCLimitValue() {
		return _linkDCChargeLimitValue.getAttribute("value");
	}
	
	public String GetACLimitValue() {
		return _linkACChargeLimitValue.getAttribute("value");
	}
	
	@SuppressWarnings("deprecation")
	public void SetBeginTime() {
		String Mintwheel = "(//XCUIElementTypeStaticText[@name='00'])[1]";
		MobileElement BeginTimeElement = driver.findElement(By.xpath("(//XCUIElementTypeImage[@name='Remote_Picker_box'])[1]//following-sibling::XCUIElementTypeOther[1]"
				+ "//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='00']"));
        String BeginTimeValue = BeginTimeElement.getAttribute("visible");
        if (BeginTimeValue.equalsIgnoreCase("true")) {
            appDevice.swipeScrollWheelDown(Mintwheel, 3);
            appDevice.sleepFor(4000);
            MobileElement notificationElement1 = driver.findElement(By.xpath("(//XCUIElementTypeOther[1]//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='30'])[1]"));
            String newBeginTimeValue = notificationElement1.getAttribute("visible");
            Assert.assertTrue(newBeginTimeValue.equalsIgnoreCase("true"));

        } else {
        	System.out.println("else part");
        	String Mintwheel1 = "(//XCUIElementTypeStaticText[@name='30'])[1]";
        	String Mintwheel2 = "(//XCUIElementTypeStaticText[@name='40'])[1]";
        	String Mintwheel3 = "(//XCUIElementTypeStaticText[@name='50'])[1]";
        	appDevice.swipeScrollWheelUp(Mintwheel1, 4);
        	appDevice.swipeScrollWheelUp(Mintwheel2, 5);
        	appDevice.swipeScrollWheelUp(Mintwheel3, 6);
        	appDevice.sleepFor(4000);
        	MobileElement notificationElement1 = driver.findElement(By.xpath("(//XCUIElementTypeOther[1]//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='00'])[1]"));
            String newBeginTimeValue = notificationElement1.getAttribute("visible");
            Assert.assertTrue(newBeginTimeValue.equalsIgnoreCase("true"));

        }
           
	}
	
	public void SetEndTime() {
		appDevice.swipeUpScreen(1);
		String Mintwheel = "(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='10'])[2]";
		MobileElement BeginTimeElement = driver.findElement(By.xpath("(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='00'])[2]"));
        String BeginTimeValue = BeginTimeElement.getAttribute("visible");
        if (BeginTimeValue.equalsIgnoreCase("true")) {
            appDevice.swipeScrollWheelDown(Mintwheel, 3);
            appDevice.sleepFor(4000);
            MobileElement notificationElement1 = driver.findElement(By.xpath("(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='00'])[2]"));
            String newBeginTimeValue = notificationElement1.getAttribute("visible");
            Assert.assertTrue(newBeginTimeValue.equalsIgnoreCase("true"));

        } else {
        	System.out.println("else part");
        	String Mintwheel2 = "(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='30'])[2]";
        	String Mintwheel3 = "(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='40'])[2]";
        	String Mintwheel4 = "(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='50'])[2]";
        	appDevice.swipeScrollWheelDown(Mintwheel2, 2);
        	appDevice.swipeScrollWheelDown(Mintwheel3, 1);
        	appDevice.sleepFor(4000);
        	MobileElement notificationElement1 = driver.findElement(By.xpath("(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='50'])[2]"));
            String newBeginTimeValue = notificationElement1.getAttribute("visible");
            Assert.assertTrue(newBeginTimeValue.equalsIgnoreCase("true"));
        	}
        }
	
        
        public void SetDepartureTime1() {
    		String Mintwheel = "(//XCUIElementTypeStaticText[@name='8'])[1]";
    		MobileElement BeginTimeElement = driver.findElement(By.xpath("(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='00'])[1]"));
            String BeginTimeValue = BeginTimeElement.getAttribute("visible");
            if (BeginTimeValue.equalsIgnoreCase("true")) {
                appDevice.swipeScrollWheelUp(Mintwheel, 3);
                appDevice.sleepFor(4000);
                appDevice.swipeUpScreen(4);
            } else {
            	System.out.println("else part");
            	String Mintwheel1 = "(//XCUIElementTypeStaticText[@name='11'])[1]";
            	String Mintwheel2 = "(//XCUIElementTypeStaticText[@name='12'])[1]";
            	appDevice.swipeScrollWheelUp(Mintwheel1, 4);
            	appDevice.swipeScrollWheelUp(Mintwheel2, 5);
            	appDevice.sleepFor(4000);
            	appDevice.swipeUpScreen(2);
            }
	}
	
        public void SetDepartureTime2() {
        	appDevice.swipeUpScreen(4);
    		String Mintwheel = "(//XCUIElementTypeStaticText[@name='8'])[2]";
    		MobileElement BeginTimeElement = driver.findElement(By.xpath("(//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell//XCUIElementTypeStaticText[@value='50'])[2]"));
            String BeginTimeValue = BeginTimeElement.getAttribute("visible");
            if (BeginTimeValue.equalsIgnoreCase("true")) {
                appDevice.swipeScrollWheelDown(Mintwheel, 3);
                appDevice.sleepFor(4000);
            } else {
            	System.out.println("else part");
            	String Mintwheel1 = "(//XCUIElementTypeStaticText[@name='11'])[2]";
            	String Mintwheel2 = "(//XCUIElementTypeStaticText[@name='12'])[2]";
            	_linkTuesDeparture2.click();
            	appDevice.swipeScrollWheelUp(Mintwheel1, 4);
            	appDevice.swipeScrollWheelUp(Mintwheel2, 5);
            	appDevice.sleepFor(4000);
            }
	}
        
        public void SetTemperature() {
    		String Mintwheel = "//XCUIElementTypeOther//XCUIElementTypeTable//XCUIElementTypeCell/XCUIElementTypeStaticText";
    		String TempinCelsius = "//XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeTable";
    		appDevice.swipeScrollWheelUp(Mintwheel, 1);
    		appDevice.swipeScrollWheelDown(Mintwheel, 3);
            appDevice.sleepFor(4000);
           
	}
        
        public void SetDCLimitSlider() {
        	new TouchAction(driver).tap(PointOption.point(273,335)).release().perform();
                appDevice.sleepFor(4000);
           
	}
        
        public void SetACLimitSlider() {
        	new TouchAction(driver).tap(PointOption.point(212,621)).release().perform(); //(x=212,y=612 coordinates of 60%)
                appDevice.sleepFor(4000);
           
	}
        
      
        public void SetDayforDeparture(String DAY, String DepartureTime) {  
        	System.out.println("Set Day for Departure Block");
        	MobileElement Day = driver.findElement(By.xpath ("(//XCUIElementTypeButton[@name='"+DAY+"'])["+DepartureTime+"]"));
        	try  
        				{  
        				System.out.println("if block started");
        				if(Day.getAttribute("value").contains("0")){
        					System.out.println("Exception found"); 					 
        					}
        				}
                    //handling the exception  
        			catch(NullPointerException e)  
        			{  
        				Day.click();
        				System.out.println(DAY+ " Clicked");
        			}
        		}
        }
