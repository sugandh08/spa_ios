package com.spa.genesis.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class RemoteActionsPage_GIA {
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='HORN/LIGHTS']")
	private MobileElement _btnHornLights;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='DOOR LOCKS']")
	private MobileElement _btnDoorLocks;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='START / STOP']")
	private MobileElement _btnStartStop;

	@AndroidFindBy(xpath = "(//android.widget.TextView[@text='Start'])[1]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Start']")
	private MobileElement _btnStart;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Stop']")
	private MobileElement _btnStop;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Lock']")
	private MobileElement _btnLock;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Unlock']")
	private MobileElement _btnUnlock;

	@iOSFindBy(accessibility = "FLASH LIGHTS")
	private MobileElement _btnFlashLight;

	@iOSFindBy(accessibility = "HORN & LIGHTS")
	private MobileElement _btnHornAndLights;

	@iOSFindBy(accessibility = "arrow back")
	private MobileElement arrowBack;

	@iOSFindBy(accessibility = "VEHICLE STATUS")
	private MobileElement _btnVehicleStatus;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Summer']")
	@iOSFindBy(accessibility = "Summer")
	private MobileElement _labelPreset1;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Winter']")
	@iOSFindBy(accessibility = "Winter")
	public MobileElement _labelPreset2;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Preset3']")
	@iOSFindBy(accessibility = "Preset3")
	private MobileElement _labelPreset3;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Preset4']")
	@iOSFindBy(accessibility = "Preset4")
	private MobileElement _labelPreset4;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Start vehicle without presets']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Start vehicle without presets\"]")
	private MobileElement _labelStartvehiclewithoutpresets;

	@iOSFindBy(accessibility = "home remote histroy")
	private MobileElement _btnRemoteHistory;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='remote start setting icon'])[1]")
	private MobileElement btnSettingPreset1;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"remote start setting icon\"])[2]")
	private MobileElement btnSettingPreset2;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='remote start setting icon'])[3]")
	private MobileElement btnSettingPreset3;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='remote start setting icon'])[4]")
	private MobileElement btnSettingPreset4;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name=\"remote_start_button_icon\"])[1]")
	private MobileElement btnStartPreset1;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"SUBMIT\"]")
	private MobileElement btnPresetSubmit;

	@iOSFindBy(accessibility = "Set Preset Name")
	private MobileElement labelSetPresetName;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Set Preset Name']/following-sibling:: XCUIElementTypeTextField")
	private MobileElement inputPresetName;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name=\"gen_map_detail_star\"]")
	private MobileElement iconStarDefaultPreset;

	@iOSFindBy(accessibility = "vehicle info refresh")
	private MobileElement _btnRefreshVehicleStatus;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Start vehicle without presets\"]/parent::XCUIElementTypeOther/preceding-sibling::XCUIElementTypeButton")
	private MobileElement _lnkstartVehicleWithoutPresets;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name=\"gen_map_detail_star\"]")
	private MobileElement _DefaultPresetStar;

	@iOSFindBy(accessibility = "Vehicle Status")
	private MobileElement _txtPageTitleVehicleStatus;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Default Preset']/following-sibling::XCUIElementTypeSwitch)[last()]")
	private MobileElement _switchDefaultPreset;

	@iOSFindBy(accessibility = "Default Preset")
	private MobileElement DefaultPreset;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Set Preset Name']/following-sibling::XCUIElementTypeTextField)[last()]")
	private MobileElement PresetNameTextbox;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Set Preset Name']")
	private MobileElement _lblSetPresetName;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='REAR AND SIDE MIRRORS DEFROSTER']")
	private MobileElement _lblRearandSideMirrorsDefroster;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='HEATED STEERING WHEEL']")
	private MobileElement _lblHeatedSteeringWheel;

	@iOSFindBy(xpath = "(//XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeImage[1])[1]")
	private MobileElement _iconFrontDefroster;

	@iOSFindBy(xpath = "(//XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeImage[2])[1]")
	private MobileElement _iconseats;

	@iOSFindBy(xpath = "(//XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeImage[3])[1]")
	private MobileElement _iconHeatedSteeringwheel;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeSwitch")
	private MobileElement _switchFrontDefroster;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='SEATS']/preceding-sibling::XCUIElementTypeSwitch)[last()]")
	private MobileElement _switchSeats;

	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='TEMPERATURE']/preceding-sibling::XCUIElementTypeSwitch[1])[last()]")
	private MobileElement _switchTemperature;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeButton[2]")
	private MobileElement _PresetStart1Button;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton[2]")
	private MobileElement _PresetStart2Button;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[3]/XCUIElementTypeButton[2]")
	private MobileElement _PresetStart3Button;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeButton[2]")
	private MobileElement _PresetStart4Button;

	@iOSFindBy(xpath = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[contains(@name,'Your request for')]")
	private MobileElement _SuccesfullorUnsuccesssfulPopup;

	// Popup locators
	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOKRemoteMessage;

	public RemoteActionsPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isIconDefaultPresetDisplayed() {
		String IsStarIconDisplayed = "//XCUIElementTypeImage[@name=\\\"gen_map_detail_star\\\"]";
		return IsStarIconDisplayed.equalsIgnoreCase(IsStarIconDisplayed);
	}

	public boolean isLblSetPrestNameDisplayed() {
		appDevice.waitForElementToLoad(labelSetPresetName);
		return appDevice.isElementDisplayed(labelSetPresetName);
	}

	public boolean isLblPreset1Displayed() {
		appDevice.waitForElementToLoad(_labelPreset1);
		return appDevice.isElementDisplayed(_labelPreset1);
	}

	public boolean isLblPreset2Displayed() {
		appDevice.waitForElementToLoad(_labelPreset2);
		return appDevice.isElementDisplayed(_labelPreset2);
	}

	public boolean isLblPreset3Displayed() {
		appDevice.waitForElementToLoad(_labelPreset3);
		return appDevice.isElementDisplayed(_labelPreset3);
	}

	public boolean isLblPreset4Displayed() {
		appDevice.swipeUpScreen(2);
		appDevice.waitForElementToLoad(_labelPreset4);
		return appDevice.isElementDisplayed(_labelPreset4);
	}

	public boolean isLblPresetStartvehiclewithoutpresetsDisplayed() {
		if (appDevice.getCurrentPlatform().equalsIgnoreCase("Ios")) {
			String LabelPresetStartvehiclewithoutpresets = "//XCUIElementTypeStaticText[@name=\\\"Start vehicle without presets\\\"]";
			return LabelPresetStartvehiclewithoutpresets.equalsIgnoreCase(LabelPresetStartvehiclewithoutpresets);
		} else {
			return appDevice.isElementDisplayed(_labelStartvehiclewithoutpresets);
		}
	}

	public boolean isLblRearandSideMirrorsDefrosterDisplayed() {
		appDevice.swipeUpScreen(2);
		appDevice.waitForElementToLoad(_lblRearandSideMirrorsDefroster);
		return appDevice.isElementDisplayed(_lblRearandSideMirrorsDefroster);
	}

	public boolean isLblHeatedSteeringWheelDisplayed() {
		appDevice.waitForElementToLoad(_lblHeatedSteeringWheel);
		return appDevice.isElementDisplayed(_lblHeatedSteeringWheel);
	}

	public void clickOnstartVehicleWithoutPrestsssetting() {
		appDevice.waitForElementToLoad(_lnkstartVehicleWithoutPresets);
		_lnkstartVehicleWithoutPresets.click();
	}

	public void clickOnFirstPresetssssetting() {
		appDevice.waitForElementToLoad(btnSettingPreset1);
		btnSettingPreset1.click();
	}

	public void clickOnPresetsSettingSubmitBtn() {
		appDevice.waitForElementToLoad(btnPresetSubmit);
		btnPresetSubmit.click();
	}

	public void clickOnFirstPresetsSettingStart() {
		appDevice.waitForElementToLoad(btnStartPreset1);
	}

	public void clickOnSecondPresetssssetting() {
		appDevice.waitForElementToLoad(btnSettingPreset2);
		btnSettingPreset2.click();
	}

	public void clickOnThirdPresetssssetting() {
		appDevice.waitForElementToLoad(btnSettingPreset3);
		btnSettingPreset3.click();
	}

	public void clickOnFourthPresetssssetting() {
		appDevice.waitForElementToLoad(btnSettingPreset4);
		btnSettingPreset4.click();
	}

	public void clickOnBackArror() {
		appDevice.waitForElementToLoad(arrowBack);
		arrowBack.click();
	}

	public void clickOnHornAndLightsButton() {
		appDevice.waitForElementToLoad(_btnHornAndLights);
		_btnHornAndLights.click();
	}

	public void clickOnFlashLightsButton() {
		appDevice.waitForElementToLoad(_btnFlashLight);
		_btnFlashLight.click();
	}

	public void clickOnUnlockButton() {
		appDevice.waitForElementToLoad(_btnUnlock);
		_btnUnlock.click();
	}

	public void clickOnLockButton() {
		appDevice.waitForElementToLoad(_btnLock);
		_btnLock.click();
	}

	public void clickOnStopButton() {
		appDevice.waitForElementToLoad(_btnStop);
		_btnStop.click();
	}

	public void clickOnStartButton() {
		appDevice.waitForElementToLoad(_btnStart);
		_btnStart.click();
	}

	public void clickOnHornLightsButton() {
		appDevice.waitForElementToLoad(_btnHornLights);
		_btnHornLights.click();
	}

	public void clickOnDoorLocksButton() {
		appDevice.waitForElementToLoad(_btnDoorLocks);
		_btnDoorLocks.click();
	}

	public void clickOnStartStopButton() {
		appDevice.waitForElementToLoad(_btnStartStop);
		_btnStartStop.click();
	}

	public void clickOnVehicleStatusButton() {
		appDevice.waitForElementToLoad(_btnVehicleStatus);
		_btnVehicleStatus.click();
	}

	public void clickOnRemoteHistoryButton() {
		appDevice.waitForElementToLoad(_btnRemoteHistory);
		_btnRemoteHistory.click();
	}

	public void clickRefreshIcon() {
		_btnRefreshVehicleStatus.click();

	}

	public void clickDefaultPresetoggleSwitch() {
		appDevice.waitForElementToLoad(_switchDefaultPreset);
		_switchDefaultPreset.click();
	}

	public void setDefaultPreset(String DefaultPresetoggle) {
		if (!(_switchDefaultPreset.getAttribute("value").equals(DefaultPresetoggle))) {
			clickDefaultPresetoggleSwitch();
		}
	}

	public void ClearandRenamePresetName(String Presetname) {
		appDevice.waitForElementToLoad(PresetNameTextbox);
		PresetNameTextbox.click();
		PresetNameTextbox.clear();
		PresetNameTextbox.sendKeys(Presetname);
		_lblSetPresetName.click();
	}

	public void clickFrontDefrostertoggleSwitch() {
		appDevice.waitForElementToLoad(_switchFrontDefroster);
		_switchFrontDefroster.click();
	}

	public void clickSeatstoggleSwitch() {
		appDevice.waitForElementToLoad(_switchSeats);
		_switchSeats.click();
	}

	public void clickPresetStart1button() {
		appDevice.waitForElementToLoad(_PresetStart1Button);
		_PresetStart1Button.click();
	}

	public void clickPresetStart2button() {
		appDevice.waitForElementToLoad(_PresetStart2Button);
		_PresetStart2Button.click();
	}

	public void clickPresetStart3button() {
		appDevice.waitForElementToLoad(_PresetStart3Button);
		_PresetStart3Button.click();
	}

	public void clickPresetStart4button() {
		appDevice.swipeUpScreen(1);
		appDevice.waitForElementToLoad(_PresetStart4Button);
		_PresetStart4Button.click();
	}

	public String getFrontDefrostertogglevalue() {
		appDevice.swipeUpScreen(1);
		String FrontDefrosterTogglevalue = _switchFrontDefroster.getAttribute("value");
		return FrontDefrosterTogglevalue;
	}

	public String getseatstogglevalue() {
		String Seatstogglevalue = _switchSeats.getAttribute("value");
		return Seatstogglevalue;
	}

	public boolean isVehicleStatusPageDisplayed() {
		return _txtPageTitleVehicleStatus.isDisplayed();
	}

	public boolean DefaultPresettoggleisEnabledDisplayed() {
		return _switchDefaultPreset.getAttribute("value").equals("1");
	}

	public boolean RenamedPresetName(String Presetname) {
		return PresetNameTextbox.getAttribute("value").equals(Presetname);
	}

	public void clickOnPopupOKButton() {
		appDevice.waitForElementToLoad(_btnOKRemoteMessage);
		_btnOKRemoteMessage.click();
	}

	public void climateOnSuccessPopup() {
		appDevice.waitForElementToLoadWithProvidedTime(_SuccesfullorUnsuccesssfulPopup, 100);
		System.out.println("Remote Command Message:" + _SuccesfullorUnsuccesssfulPopup.getText());
		String REMOTE_COMMAND_MESSAGE = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[contains(@name,'Your request for')]";
		// Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("Start"),"The Remote Start
		// command was not successful");
		Assert.assertTrue(_SuccesfullorUnsuccesssfulPopup.getText().contains("Remote Start"),
				"The Remote Start command was not successful");
		Assert.assertTrue(_SuccesfullorUnsuccesssfulPopup.getText().contains("was successful"),
				"The Remote Start command was not successful");
		// Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),"The
		// Remote Start command was not successful");
		clickOnPopupOKButton();
	}

	public void climateoffSuccessPopup() {
		appDevice.waitForElementToLoadWithProvidedTime(_SuccesfullorUnsuccesssfulPopup, 100);
		System.out.println("Remote Command Message:" + _SuccesfullorUnsuccesssfulPopup.getText());
		String REMOTE_COMMAND_MESSAGE = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[contains(@name,'Your request for')]";
		Assert.assertTrue(_SuccesfullorUnsuccesssfulPopup.getText().contains("Stop"),
				"The Remote Start command was not successful");
		Assert.assertTrue(_SuccesfullorUnsuccesssfulPopup.getText().contains("was successful"),
				"The Remote Start command was not successful");

		// Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("Stop"),"The Remote Start
		// command was not successful");
		// Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),"The
		// Remote Start command was not successful");
		clickOnPopupOKButton();
	}
}
