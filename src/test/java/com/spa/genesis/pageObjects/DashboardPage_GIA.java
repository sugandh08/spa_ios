package com.spa.genesis.pageObjects;

import java.time.Duration;


import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

public class DashboardPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	// Header Locators
	@iOSFindBy(accessibility = "home remote histroy")
	private MobileElement _iconRemoteHistory;

	@iOSFindBy(accessibility = "home notication")
	private MobileElement _iconNotification;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"btn navmenu default\"]")
	private MobileElement _btnMenu;

	@iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='MyHyundai']//XCUIElementTypeTable")
	private MobileElement _panelMenu;

	@iOSFindBy(accessibility = "View Profile")
	private MobileElement _linkUserProfile;

	// Navigation Bars locators
	@AndroidFindBy(xpath = "//android.widget.FrameLayout[@content-desc='MAP']/android.widget.ImageView")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='MAP']")
	private MobileElement _btnMapOption;

	@AndroidFindBy(id = "iv_home_add")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='MVR']")
	private MobileElement _btnMVROption;

	@AndroidFindBy(id = "com.stationdm.genesis:id/iv_home_alarm")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='SCHEDULE']")
	private MobileElement _btnScheduleOption;

	@AndroidFindBy(id = "com.stationdm.genesis:id/remoteFragment")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='REMOTE']")
	private MobileElement _btnRemoteOption;

	@AndroidFindBy(id = "com.stationdm.genesis:id/ivUnlockIcon")
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Unlock']/preceding-sibling::XCUIElementTypeButton)[last()]")
	private MobileElement _btnUnLockOption;

	@AndroidFindBy(id = "com.stationdm.genesis:id/ivLockIcon")
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Lock']/preceding-sibling::XCUIElementTypeButton)[last()]")
	private MobileElement _btnLockOption;

	@AndroidFindBy(id = "setting_icon")
	@iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Start']/preceding-sibling:: XCUIElementTypeButton)[last()]")
	private MobileElement _btnRemoteStart;

	// Popup locators

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Cancel']")
	private MobileElement _btnCancelPopup;

	@AndroidFindBy(xpath = "//android.widget.Button[@text='OK']")
	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOKRemoteMessage;

	@iOSFindBy(accessibility = "Message Center")
	private MobileElement lblMessageCenter;

	@iOSFindBy(accessibility = "home notication")
	private MobileElement iconHomeNotication;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Good')]")
	private MobileElement _labelWelcomeMessage;

	@iOSFindBy(xpath = "(//XCUIElementTypeImage[@name='icn_hi']/following-sibling:: XCUIElementTypeStaticText[contains(@name,'°')])[1]/following-sibling:: XCUIElementTypeStaticText[@name]/following-sibling::XCUIElementTypeImage[@name='icn_lo']/following-sibling:: XCUIElementTypeStaticText[contains(@name,'°')]")
	private MobileElement _imgWeatherTempratureIcon;

	@AndroidFindBy(id = "com.stationdm.genesis:id/map_search_et")
	@iOSFindBy(accessibility = "icn_search")
	private MobileElement _btnSearch;

	@AndroidFindBy(xpath = "//android.widget.FrameLayout[@resource-id='com.stationdm.genesis:id/evActivity']")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"EV\"]")
	private MobileElement _linkEv;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[starts-with(@name,'No')]")
	private MobileElement _txtNotfound;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='remote_statu_icon']/parent::XCUIElementTypeOther")
	private MobileElement _btnVehicleStatus;

	@iOSFindBy(accessibility = "home remote histroy")
	private MobileElement _btnRemoteHistory;

	@iOSFindBy(accessibility = "topnav")
	private MobileElement _btnNavigateBack;

	@iOSFindBy(accessibility = "POI Search")
	private MobileElement _linkSearchPoi;

	@iOSFindBy(accessibility = "Favorites")
	private MobileElement _linkMyPoi;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField")
	private MobileElement txtSearchPOI;

	@iOSFindBy(accessibility = "vehicle info refresh")
	private MobileElement _btnRefreshVehicleStatus;

	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='vehicle_schedule_time']/following-sibling::XCUIElementTypeStaticText")
	private MobileElement _txtDepartureTimeSet;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Plugged In')]")
	private MobileElement _txtPluggedInStatus;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Charging')]")
	private MobileElement _txtChargingStatus;

	@iOSFindBy(accessibility = "vehicle start charge")
	private MobileElement _btnStartCharge;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Last Updated')]")
	private MobileElement _txtLastUpdateStatusStatus;

	@iOSFindBy(accessibility = "IAEtut btn gotit default")
	private MobileElement _btnGotIt;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='SCHEDULE']")
	private MobileElement _btnSchedule;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Car Care Scheduling']")
	private MobileElement _popupCarCareSchedule;

	@AndroidFindBy(id = "android:id/button2")
	@iOSFindBy(accessibility = "Cancel")
	private MobileElement _btnCancel;

	@iOSFindBy(accessibility = "Visit Website")
	private MobileElement _btnVisitWebsite;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='add event']")
	private MobileElement linkAddEvent;

	@iOSFindBy(accessibility = "IAEtut btn addevent default")
	private MobileElement _btnAddEventWindow;

	@iOSFindBy(accessibility = "Add event")
	private MobileElement _btnAddEventPopup;

	@iOSFindBy(accessibility = "Tap for Remote Start")
	private MobileElement labelQuickStartTutorial;

	@iOSFindBy(accessibility = "Long press for Remote Presets and Settings")
	private MobileElement labelPresetTutorial;

	@iOSFindBy(accessibility = "Tap to Lock Doors")
	private MobileElement labelLockTutorial;

	@iOSFindBy(accessibility = "Tap to Unlock Doors")
	private MobileElement labelUnLockTutorial;

	@iOSFindBy(accessibility = "Access your Settings to enable or disable Quick Start")
	private MobileElement labelSettingTutorial;

	@iOSFindBy(accessibility = "WE'VE MADE IT EASIER TO ACCESS THE MOST USED REMOTE FEATURES!")
	private MobileElement labelRemoteFeatureTutorial;

	@iOSFindBy(accessibility = "Go To Settings")
	private MobileElement btnGoToSettingf;

	@AndroidFindBy(id = "android:id/message")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText")
	private MobileElement txtGetPopupMessage;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Schedule Valet Pick-Up']")
	private MobileElement btnValetPickUp;

	@AndroidFindBy(id = "com.stationdm.genesis:id/tvDropOff")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Schedule Drop-Off']")
	private MobileElement btnDropOff;

	@AndroidFindBy(xpath = "//android.widget.FrameLayout[@content-desc='CAR CARE']/android.widget.ImageView")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Car care']")
	private MobileElement btnCareCare;

	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='com.stationdm.genesis:id/tvEVTitle']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"GENESIS GV60\"]")
	private MobileElement _linkEVpageheadertittle;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='OK']")
	private MobileElement _ok;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='home dk']")
	private MobileElement _iconDigitalKey;

	public DashboardPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);

	}
	
	public boolean isDigitalKeyiconDisplayed() {
		appDevice.waitForElementToLoad(_iconDigitalKey);
		return appDevice.isElementDisplayed(_iconDigitalKey);
	}
	
	public void clickDigitalKeyIcon() {
		_iconDigitalKey.click();
	}

	public boolean isLabelQuickStartTutorialDisplay() {
		appDevice.waitForElementToLoad(labelQuickStartTutorial);
		return labelQuickStartTutorial.isDisplayed();

	}

	public boolean isLabelPresetTutorialDisplay() {
		appDevice.waitForElementToLoad(labelPresetTutorial);
		return labelPresetTutorial.isDisplayed();

	}

	public boolean isLabelLockTutorailDisplay() {
		appDevice.waitForElementToLoad(labelLockTutorial);
		return labelLockTutorial.isDisplayed();

	}

	public boolean isLabelUnLockTutorialDisplay() {
		appDevice.waitForElementToLoad(labelUnLockTutorial);
		return labelUnLockTutorial.isDisplayed();

	}

	public boolean isLabelSettingTutorailDisplay() {
		appDevice.waitForElementToLoad(labelSettingTutorial);
		return labelSettingTutorial.isDisplayed();

	}

	public boolean isLabelRemoteFeatureTutorialDisplay() {
		appDevice.waitForElementToLoad(labelRemoteFeatureTutorial);
		return labelRemoteFeatureTutorial.isDisplayed();

	}

	public void clickGoToSettingBtn() {
			appDevice.isElementDisplayed(btnGoToSettingf); 
				btnGoToSettingf.click();
			}

	public void clickAddEvent() {
		appDevice.waitForElementToLoad(linkAddEvent);
		linkAddEvent.click();
	}

	public void clickEvbutton() {
		appDevice.sleepFor(2000);
		appDevice.waitForElementToLoad(_linkEv);
		_linkEv.click();
		appDevice.waitForElementToLoad(_linkEVpageheadertittle);
		appDevice.isElementDisplayed(_linkEVpageheadertittle);

	}

	public void clickAddEventFromAddEventWindow() {
		appDevice.waitForElementToLoadWithProvidedTime(_btnAddEventWindow, 3);
		if (appDevice.isElementDisplayed(_btnAddEventWindow)) {
			_btnAddEventWindow.click();

		}

	}

	public boolean isLinkAddEventDisplay() {
		return linkAddEvent.isDisplayed();

	}

	public boolean isAddEventButtonPopupDisplay() {

		return _btnAddEventPopup.isDisplayed();

	}

	public void clickSchedule() {
		appDevice.waitForElementToLoad(_btnSchedule);
		_btnSchedule.click();
	}

	public void clickCancel() {
		appDevice.waitForElementToLoad(_btnCancel);
		_btnCancel.click();
	}

	public boolean isPopupCarCareScheduleDisplay() {

		return _popupCarCareSchedule.isDisplayed();

	}

	public boolean isCancelButtonDisplay() {

		return _btnCancel.isDisplayed();

	}

	public boolean isVisitWebsiteButtonDisplay() {

		return _btnVisitWebsite.isDisplayed();

	}

	public boolean isUnlockButtonDisplay() {
		appDevice.waitForElementToLoad(_btnUnLockOption);
		return appDevice.isElementDisplayed(_btnUnLockOption);

	}

	public boolean isUnlockButtonDisplayWithinExpectedTime(int time) {
		appDevice.waitForElementToLoadWithProvidedTime(_btnUnLockOption, time);
		return appDevice.isElementDisplayed(_btnUnLockOption);

	}

	public boolean isUnlockButtonDisplayWithOutHandelException() {
		appDevice.waitForElementToLoadWithProvidedTime(_btnUnLockOption, 3);
		return _btnUnLockOption.isDisplayed();

	}

	public void clickUnLockButton() {
		appDevice.waitForElementToLoad(_btnUnLockOption);
		_btnUnLockOption.click();
	}

	public boolean isLockButtonDisplay() {
		appDevice.waitForElementToLoad(_btnLockOption);
		return _btnLockOption.isDisplayed();

	}

	public void clickLockButton() {
		appDevice.waitForElementToLoad(_btnLockOption);
		_btnLockOption.click();
	}

	public boolean isMenuPanelDisplayed() {
		return _panelMenu.isDisplayed();
	}

	public boolean isUserProfileMenuSectionDisplayed() {
		return _linkUserProfile.isDisplayed();
	}

	// Menu functions

	public void openMenu() {

		appDevice.waitForElementToLoad(_btnMenu);
		_btnMenu.click();
		// appDevice.waitForElementToLoad(_panelMenu);
	}

	public void navigateToUserProfile() {
		_linkUserProfile.click();
	}

	public void clickMapsOption() {
		_btnMapOption.click();
	}

	// Popup method
	public String getResultPopUpMessageText(String strAlertName) {
		String remoteMessage = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[@name='" + strAlertName
				+ "']/following-sibling::XCUIElementTypeStaticText";
		appDevice.sleepFor(3000);
//		if (_txtFaceId.isDisplayed()) {
//			_btnNotNowFaceId.click();
//		}
		appDevice.waitForElementToLoad(_btnOkPopUp);
		return driver.findElement(By.xpath(remoteMessage)).getText();

	}

	public void clickOnPopupOKButton() {
		appDevice.waitForElementToLoad(_btnOKRemoteMessage);
		_btnOKRemoteMessage.click();
	}

	public void clickOnRemoteHistoryIcon() {
		appDevice.waitForElementToLoad(_iconRemoteHistory);
		System.out.println("RemoteHistory Page");
		_iconRemoteHistory.click();

	}

	public void clickHomeNotificationButton() {
		iconHomeNotication.click();
	}

	public boolean labelMessageCdasDisplayed() {
		appDevice.waitForElementToLoad(lblMessageCenter);
		return lblMessageCenter.isDisplayed();
	}

	public String getWelcomeText() {
		return _labelWelcomeMessage.getAttribute("value");
	}

	public boolean isWeatherTempratureIconDisplayed() {
		return appDevice.isElementDisplayed(_imgWeatherTempratureIcon);
	}

	public boolean isVehicleNameDisplayed(String strVehicleName) {

		boolean flag = false;
		try {
			MobileElement vehicleName = driver
					.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + strVehicleName + "']"));
			if (vehicleName.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			e.getStackTrace();

		}
		return flag;

	}

	public void navigateToMapPageAndClickSearchPOI() {
		_btnMapOption.click();
		_linkSearchPoi.click();
		appDevice.sleepFor(5000);
	}

	public void clickOnVehicleStatusButton() {
		appDevice.waitForElementToLoad(_btnVehicleStatus);
		_btnVehicleStatus.click();

	}

	public void navigateToRemoteHistoryPage() {
		appDevice.waitForElementToLoad(_btnRemoteHistory);
		_btnRemoteHistory.click();

	}

	public void navigateToMapOptionPage() {
		_btnMapOption.click();
		appDevice.sleepFor(1000);
	}

	public void clickCancelPopup() {
		if (appDevice.isElementDisplayed(_btnCancelPopup)) {
			_btnCancelPopup.click();
		}

	}

	public void clickNavigateBack() {
		_btnNavigateBack.click();

	}

	public void enterSearchText(String zip) {

		txtSearchPOI.sendKeys(zip + "\n");
		try {
			appDevice.sleepFor(10000);

		} catch (ElementNotVisibleException e) {
			if (_txtNotfound.isDisplayed()) {
				_btnOkPopUp.click();

			} else
				System.out.println("Please try using different zip code");
		}

	}

	public boolean isCurrentLocationDisplay(String strCurrentLocation) {

		String currentLocation = "//XCUIElementTypeStaticText[@name='" + strCurrentLocation + "']";

		return appDevice.isElementDisplayed(driver.findElement(By.xpath(currentLocation)));

	}

	public boolean isSearchFielDisplay() {

		return _btnSearch.isEnabled();

	}

	public void navigateToChargeManagementScreen() {
		appDevice.waitForElementToLoad(_txtDepartureTimeSet);
		_txtDepartureTimeSet.click();
	}

	public String getCommandsPopupMessageText(String strPopupText) {
		appDevice.sleepFor(20000);
		String remoteMessage = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[contains(@name,'" + strPopupText
				+ "')]";
		String Message1 = "//android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[contains(@text,'"
				+ strPopupText + "')]";
		appDevice.sleepFor(30000);
		appDevice.isElementDisplayed(_btnOkPopUp);
		appDevice.waitForElementToLoad(_btnOkPopUp);
		if (appDevice.getCurrentPlatform().equalsIgnoreCase("Android")) {
			return driver.findElement(By.xpath(Message1)).getText();
		} else {
			return driver.findElement(By.xpath(remoteMessage)).getText();
		}
	}

	public void clickOnRemoteStarttButton() {
		appDevice.waitForElementToLoad(_btnRemoteStart);
		_btnRemoteStart.click();
		appDevice.sleepFor(5000);
	}

	public void longPressOnRemoteStartButton() {

		TouchAction action = new TouchAction(driver)
				.longPress(
						longPressOptions().withElement(element(_btnRemoteStart)).withDuration(Duration.ofMillis(5000)))
				.release().perform();

	}

	public void longPressOnLockButton() {

		TouchAction action = new TouchAction(driver)
				.longPress(
						longPressOptions().withElement(element(_btnLockOption)).withDuration(Duration.ofMillis(5000)))
				.release().perform();

	}

	public void longPressOnUnlockButton() {

		TouchAction action = new TouchAction(driver)
				.longPress(
						longPressOptions().withElement(element(_btnUnLockOption)).withDuration(Duration.ofMillis(5000)))
				.release().perform();

	}

	public boolean isRemoteStartButtonDisplay() {

		return appDevice.isElementDisplayed(_btnRemoteStart);
	}

	public String getPluggedInStatus() {
		return _txtPluggedInStatus.getText();
	}

	public String getChargingStatus() {
		return _txtChargingStatus.getText();
	}

	public void clickOnStartChargeButton() {
		_btnStartCharge.click();

	}

	public String getLatUpdateValue() {

		appDevice.waitForElementToLoad(_txtLastUpdateStatusStatus);
		return _txtLastUpdateStatusStatus.getAttribute("value");

	}

	public void clickRefreshIcon() {
		_btnRefreshVehicleStatus.click();
	}

	public void clickGotItButton() {
		if (appDevice.isElementDisplayed(_btnGotIt)) {
			_btnGotIt.click();
		}
	}

	public void clickRemoteOption() {
		appDevice.isElementDisplayed(_btnRemoteOption);
		_btnRemoteOption.click();
	}

	public String getPopupMessage() {
		// appDevice.sleepFor(2);
		if (appDevice.isElementDisplayed(txtGetPopupMessage)) {
			return txtGetPopupMessage.getAttribute("name");
		} else {
			return "No Error Popup";
		}
	}

	public void clickCancelButton() {
		if (appDevice.isElementDisplayed(txtGetPopupMessage)) {
			_btnCancel.click();
		}
	}

	public boolean isBtnDropOffDisplay() {
		// appDevice.isElementDisplayed(btnDropOff);
		return appDevice.isElementDisplayed(btnDropOff);

	}

	public void clickBtnDropOff() {
		btnDropOff.click();
	}

	public void clickBtnCareCare() {
		btnCareCare.click();
	}

}