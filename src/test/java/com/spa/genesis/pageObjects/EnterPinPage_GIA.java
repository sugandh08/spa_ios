package com.spa.genesis.pageObjects;

import java.time.Duration;

import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;

public class EnterPinPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id=\"com.stationdm.genesis:id/tv_enter_pin\"]")
	@iOSFindBy(accessibility = "Enter PIN")
	private MobileElement _msgEnterPIN;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"0\"]")
	@iOSFindBy(accessibility = "btn pin 0 default")
	private MobileElement _btnZero;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"1\"]")
	@iOSFindBy(accessibility = "btn pin 1 default")
	private MobileElement _btnOne;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"2\"]")
	@iOSFindBy(accessibility = "btn pin 2 default")
	private MobileElement _btnTwo;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"3\"]")
	@iOSFindBy(accessibility = "btn pin 3 default")
	private MobileElement _btnThree;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"4\"]")
	@iOSFindBy(accessibility = "btn pin 4 default")
	private MobileElement _btnFour;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"5\"]")
	@iOSFindBy(accessibility = "btn pin 5 default")
	private MobileElement _btnFive;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"6\"]")
	@iOSFindBy(accessibility = "btn pin 6 default")
	private MobileElement _btnSix;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"7\"]")
	@iOSFindBy(accessibility = "btn pin 7 default")
	private MobileElement _btnSeven;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"8\"]")
	@iOSFindBy(accessibility = "btn pin 8 default")
	private MobileElement _btnEight;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"9\"]")
	@iOSFindBy(accessibility = "btn pin 9 default")
	private MobileElement _btnNine;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"CANCEL\"]")
	@iOSFindBy(accessibility = "btn pin cancel default")
	private MobileElement _btnCancel;

	@AndroidFindBy(xpath = "//android.widget.ImageView[@resource-id=\"com.stationdm.genesis:id/deleteImgae\"]")
	@iOSFindBy(accessibility = "btn pin back default")
	private MobileElement _btnEnter;

	public EnterPinPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public void enterPin(String pin) {
		String[] digits = pin.replace(".0", "").split("");

		// For each digit found
		for (String digit : digits) {
			tapDigit(digit);
		}
	}

	private void tapDigit(String digit) {
		switch (digit) {
		case "0":
			appDevice.sleepFor(2000);
			_btnZero.click();
			break;

		case "1":
			appDevice.sleepFor(2000);
			_btnOne.click();
			break;

		case "2":
			appDevice.sleepFor(2000);
			_btnTwo.click();
			break;

		case "3":
			appDevice.sleepFor(2000);
			_btnThree.click();
			break;

		case "4":
			appDevice.sleepFor(2000);
			_btnFour.click();
			break;

		case "5":
			appDevice.sleepFor(2000);
			_btnFive.click();
			break;

		case "6":
			appDevice.sleepFor(2000);
			_btnSix.click();
			break;

		case "7":
			appDevice.sleepFor(2000);
			_btnSeven.click();
			break;

		case "8":
			appDevice.sleepFor(2000);
			_btnEight.click();
			break;

		case "9":
			appDevice.sleepFor(2000);
			_btnNine.click();
			break;

		}

	}

	public boolean isEnterPinPopupDisplayed() {
		return _msgEnterPIN.getText().equals("Enter PIN");
	}

}
