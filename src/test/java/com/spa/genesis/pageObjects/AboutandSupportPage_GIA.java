package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class AboutandSupportPage_GIA {
	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	@iOSFindBy(accessibility = "About & Support")
	private MobileElement _titleAboutandSupport;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Guides']")
	private MobileElement _headerGuides;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Getting Started Guide']")
	private MobileElement _subMenuGettingStartedGuide;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Indicator Guide']")
	private MobileElement _subMenuIndictorGuide;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Owner’s Manual']")
	private MobileElement _subMenuOwnerManual;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Maintenance information']")
	private MobileElement _subMenuMaintenanceInfo;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Warranty Info']")
	private MobileElement _subMenuWarrantyInfo;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Genesis Virtual Guide']")
	private MobileElement _subMenuGenesisVirtualGuide;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Genesis Resources']")
	private MobileElement _headerGenesisResources;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Getting Started']")
	private MobileElement _subMenuGettingStarted;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Connected Services']")
	private MobileElement _subMenuConnectedServices;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Vehicle Health']")
	private MobileElement _subMenuVehicleHealth;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Manuals & Warranties']")
	private MobileElement _subMenuManualWarranties;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Bluetooth Assistance']")
	private MobileElement _subMenuBluetoothAssistance;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Technology and Navigation']")
	private MobileElement _subMenuTechAndNavigation;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Accessories and Parts']")
	private MobileElement _subMenuAccessoriesparts;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='General Information']")
	private MobileElement _subMenuGeneralInfo;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Links\"]")
	private MobileElement _headerLinks;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='MyGenesis.com']")
	private MobileElement _subMenuGenesiaCom;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Genesis USA']")
	private MobileElement _subMenuGenesisUSA;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Genesis Accessories']")
	private MobileElement _subMenuGenesisAccessories;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Genesis Collision Center']")
	private MobileElement _subMenuGenesisCollisonCenter;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Contact Us']")
	private MobileElement _headerContactUs;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Email App Support']")
	private MobileElement _subMenuEmailSupport;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Consumer Assistance Center']")
	private MobileElement _subMenuConsumerAssisCenterr;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@label,'Version')]")
	private MobileElement _headerVersion;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='FAQ']")
	private MobileElement _subMenuFAQ;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Terms & Conditions']")
	private MobileElement _subMenuTermsCondition;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Privacy Policy\"]")
	private MobileElement _subMenuPrivacyPolicy;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Visit Privacy Policy']")
	private MobileElement _visitPrivacyPolicy;

	@iOSFindBy(accessibility = "PRIVACY POLICY")
	private MobileElement _titlePrivacyPolicy;

	@iOSFindBy(accessibility = "Downloading...")
	private MobileElement _DownlaodingPopUp;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _PopUpOK;

	@iOSFindBy(xpath = "///XCUIElementTypeStaticText[@name=\"Done\"]")
	private MobileElement _OwnersManualDoneBtn;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"MyGenesis.com\"]")
	private MobileElement _genesisWebPage;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Genesis Virtual Guide']")
	private MobileElement _genesisVirtualGuide;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"GENESIS FAQ\"]")
	private MobileElement _faqPage;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"TERMS & CONDITIONS\"]")
	private MobileElement _genesisTermsConditons;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"EMAIL APP SUPPORT\"]")
	private MobileElement _emailAppSupport;

	public AboutandSupportPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public boolean isAboutAndSupportPageDisplayed() {
		return _titleAboutandSupport.isDisplayed();

	}

	public boolean isGuideHeaderDisplayed() {
		return _headerGuides.isDisplayed();

	}

	public boolean issubmenuGettingStartedDisplayed() {
		return _subMenuGettingStartedGuide.isDisplayed();

	}

	public boolean issubmenuOwnerManualDisplayed() {
		return _subMenuOwnerManual.isDisplayed();

	}

	public boolean issubmenuWarrantyInfoDisplayed() {
		return _subMenuWarrantyInfo.isDisplayed();

	}

	public boolean issubmenuMaintenaceInfoDisplayed() {
		return _subMenuMaintenanceInfo.isDisplayed();

	}

	public boolean issubmenuIndicatorGuideDisplayed() {
		return _subMenuIndictorGuide.isDisplayed();

	}

	public boolean issubmenuGenesisVirtualDisplayed() {
		return _subMenuGenesisVirtualGuide.isDisplayed();

	}

	public boolean isGenesisResourceHeaderDisplayed() {
		return _headerGenesisResources.isDisplayed();

	}

	public boolean isGettingStartedDisplayed() {
		return _subMenuGettingStarted.isDisplayed();

	}

	public boolean isConnectedServicesDisplayed() {
		return _subMenuConnectedServices.isDisplayed();

	}

	public boolean isVehicleHealthDisplayed() {
		return _subMenuVehicleHealth.isDisplayed();

	}

	public boolean isManualWarrantyDisplayed() {
		return _subMenuManualWarranties.isDisplayed();

	}

	public boolean isBluetoothAssistanceDisplayed() {
		return _subMenuBluetoothAssistance.isDisplayed();

	}

	public boolean isTechNavigationDisplayed() {
		return _subMenuTechAndNavigation.isDisplayed();

	}

	public boolean isAccessoriesPartsDisplayed() {
		appDevice.swipeUpScreen(1);
		return _subMenuAccessoriesparts.isDisplayed();
	}

	public boolean isGeneralInfoDisplayed() {
		return _subMenuGeneralInfo.isDisplayed();

	}

	public boolean isLinksHeaderDisplayed() {
		appDevice.swipeUpScreen(1);
		return _headerLinks.isDisplayed();

	}

	public boolean isGenesisComDisplayed() {
		appDevice.swipeUpScreen(4);
		return _subMenuGenesiaCom.isDisplayed();

	}

	public boolean isGenesisUSADisplayed() {
		return _subMenuGenesisUSA.isDisplayed();

	}

	public boolean isGenesisAccessoriesDisplayed() {
		return _subMenuGenesisAccessories.isDisplayed();
	}

	public boolean isGenesisCollisionCenterDisplayed() {
		appDevice.swipeUpScreen(1);
		return _subMenuGenesisCollisonCenter.isDisplayed();

	}

	public boolean isContactUsHeaderDisplayed() {
		appDevice.swipeUpScreen(3);
		return _headerContactUs.isDisplayed();

	}

	public boolean isEmailSupportDisplayed() {
		appDevice.swipeUpScreen(4);
		return _subMenuEmailSupport.isDisplayed();

	}

	public boolean isConsumerAssistanceDisplayed() {
		return _subMenuConsumerAssisCenterr.isDisplayed();
	}

	public boolean isVersionHeaderDisplayed() {
		appDevice.swipeUpScreen(2);
		return _headerVersion.isDisplayed();

	}

	public boolean isFAQDisplayed() {
		appDevice.swipeUpScreen(4);
		return _subMenuFAQ.isDisplayed();

	}

	public boolean isTermsConditionDisplayed() {
		appDevice.swipeUpScreen(2);
		return _subMenuTermsCondition.isDisplayed();
	}

	public boolean isPrivacyPolicyDisplayed() {
		return _subMenuPrivacyPolicy.isDisplayed();

	}

	public boolean isTitlePrivacyPolicyDisplayed() {
		return _titlePrivacyPolicy.isDisplayed();

	}

	public void clickPrivacyPolicyPage() {
		_subMenuPrivacyPolicy.click();
		_visitPrivacyPolicy.click();

	}

	public void clickOwnersManual() {
		if(_subMenuOwnerManual.isDisplayed()) {
		_subMenuOwnerManual.click();
		}
	}

	public void clickGenesisCom() {
		_subMenuGenesiaCom.click();

	}

	public boolean isGenesisComPageDisplayed() {
		return _genesisWebPage.isDisplayed();
	}

	public void clicksubmenuGenesisVirtual() {
		_subMenuGenesisVirtualGuide.click();

	}

	public void clickFAQ() {
		_subMenuFAQ.click();

	}

	public boolean isFAQPageDisplayed() {
		return _faqPage.isDisplayed();
	}

	public boolean isGenesisVirtualGuidePageDisplayed() {
		return _genesisVirtualGuide.isDisplayed();
	}

	public boolean isDownloadingPopUpDisplayed() {
		return _DownlaodingPopUp.isDisplayed();
	}

	public void clickOK() {
		if(_PopUpOK.isDisplayed()) {
		_PopUpOK.click();
	}
	}

	public void clickTermsCondition() {
		_subMenuTermsCondition.click();
	}

	public boolean isTitleTermsConditonsDisplayed() {
		appDevice.waitForElementToLoad(_genesisTermsConditons);
		return _genesisTermsConditons.isDisplayed();
	}

	public void clickDone() {
		_OwnersManualDoneBtn.click();
	}

	public void clickEmailSupport() {
		 _subMenuEmailSupport.click();

	}
	
	public boolean isTitleEmailAppSupportDisplayed() {
		return _emailAppSupport.isDisplayed();
	}

	public boolean isOwnersManualDisplayed() {
		return _OwnersManualDoneBtn.isDisplayed();
	}
}
