package com.spa.genesis.pageObjects;

import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Keys;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import freemarker.core._TemplateModelException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.HowToUseLocators;
import io.appium.java_client.pagefactory.LocatorGroupStrategy;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class MapPage_GIA{

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;
	private final int IMPLICIT_WAIT = 10;
	
	public MapPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	// Map Options
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='FAVORITES']")
	private MobileElement tabFavorites;

	@AndroidFindBy(xpath ="//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView[3]")
	@iOSFindBy(accessibility = "icn Dealer default")
	private MobileElement _linkGenDealers;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"icn Gas default\"]")
	private MobileElement _linkNerebyGas;

	@iOSFindBy(accessibility = "icn FMC default")
	private MobileElement _linkCarFinder;
	
	@AndroidFindBy(xpath = "//android.widget.ImageView[@resource-id=\"com.stationdm.genesis:id/home_map_svm_bar\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"icn SVM default\"]")
	private MobileElement _linkSVMbtn;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[contains(@name,'sdm popupmenu map')]")
	private MobileElement _popUpMenuMap;

	@AndroidFindBy(id = "com.stationdm.genesis:id/title_left_icon")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='arrow back']")
	private MobileElement _btnBack;

	@AndroidFindBy(xpath ="//android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.EditText")
	@iOSFindBy(xpath = "//XCUIElementTypeImage[@name='map_img_search']/following-sibling::XCUIElementTypeTextField")
	private MobileElement _searchBar;

	@iOSFindBy(accessibility = "sdm search icn")
	private MobileElement _btnSearch;

	@iOSFindBy(accessibility = "icn POI default")
	private MobileElement _linkSearchPoi;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[starts-with(@name,'No')]")
	private MobileElement _txtNotfound;

	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;

	@iOSFindBy(accessibility = "sdm btn share default")
	private MobileElement _linkShare;

	@iOSFindBy(accessibility = "map icn favorite desel")
	private MobileElement _linkSave;

	@iOSFindBy(accessibility = "btn poicall default")
	private MobileElement _linkPoiCall;

	@iOSFindBy(accessibility = "btn sendtocarsmall default")
	private MobileElement btnSendToCarIcon;

	@iOSFindBy(accessibility = "//XCUIElementTypeStaticText[@name='SEND TO CAR']")
	private MobileElement _linkSendToCar;

	@AndroidFindBy(xpath ="//android.widget.RelativeLayout[2]/android.view.ViewGroup/android.widget.ImageView[1]")
	@iOSFindBy(accessibility = "map icn arrow up")
	private MobileElement _tooltipArrowUp;

	@iOSFindBy(accessibility = "map icn arrow down")
	private MobileElement _tooltipArrowDown;

	@iOSFindBy(accessibility = "POI successfully added.")
	private MobileElement _txtPOISuccessfullyAdded;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'This location has already been saved to your favorites.')]")
	private MobileElement _txtPOIAlreadySaved;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='OK']/ancestor::XCUIElementTypeAlert//XCUIElementTypeStaticText")
	private MobileElement _labelMesssage;

	@iOSFindBy(accessibility = "btn map list default")
	private MobileElement _poiViewList;

	@iOSFindBy(accessibility = "sdm btn delete default")
	private MobileElement _linkDelete;

	@iOSFindBy(accessibility = "map new car finder")
	private MobileElement iconMapNewCarFinder;

	@iOSFindBy(accessibility = "btn centermap default")
	private MobileElement iconMapGPSIcon;

	@iOSFindBy(xpath = "//XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
	private MobileElement txtMapLocationAddress;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
	private MobileElement txtMapPOILocationAddress;
	
	@iOSFindBy(xpath = "//XCUIElementTypeAlert[@name='Location Services Disabled']")
	private MobileElement _alertLocationServiceDisabled;

	@iOSFindBy(accessibility = "Cancel")
	private MobileElement buttonCancelAlert;

	@iOSFindBy(accessibility = "Favorites")
	private MobileElement linkFavorites;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='map icn arrow up']/following-sibling::XCUIElementTypeStaticText)[1]")
	private MobileElement labelAddress;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[4]")
	private MobileElement labelDealerAddress;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='SEND TO CAR']")
	private MobileElement btnSendToCar;

	@iOSFindBy(accessibility = "POI successfully sent to Genesis.")
	private MobileElement labelSuccessfullySentToCar;

	@iOSFindBy(accessibility = "RETAILER LOCATOR")
	private MobileElement _mapDealerLocator;

	@iOSFindBy(accessibility = "PLACES OF INTEREST")
	private MobileElement _mapFavourites;

	@iOSFindBy(accessibility = "GAS STATION")
	private MobileElement _mapGasStation;

	@iOSFindBy(accessibility = "POI SEARCH")
	private MobileElement _mapPoiSearch;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"SURROUND VIEW MONITOR\"]")
	@iOSFindBy(accessibility = "SURROUND VIEW MONITOR")
	private MobileElement _mapSVMpage;

	@iOSFindBy(accessibility = "btn poi prefer star icon")
	//@iOSFindBy(accessibility = "btn setgdealer default")
	private MobileElement _mapSetAsPreferred;

	@iOSFindBy(accessibility = "Your preferred Genesis Dealer Service Center has been updated.")
	private MobileElement labelPreferredGenesisDealerSuccess;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='map img navigation']/following-sibling:: XCUIElementTypeStaticText[contains(@name,'mi')]")
	private MobileElement btnMapNavigationIcon;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Do you want to call this phone number')]")
	private MobileElement popupCalltext;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Call']")
	private MobileElement btnCallPopup;

	@iOSFindBy(accessibility = "icn_hours")
	private MobileElement iconHours;

	@iOSFindBy(accessibility = "Shop Accessories")
	private MobileElement linkShopAccessories;
	
	@AndroidFindBy(xpath="//android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='SCHEDULE SERVICE']|//XCUIElementTypeStaticText[@name=\"Schedule Service\"]")
	private MobileElement pageScheduleService;
	
	@AndroidFindBy(xpath="//*[@text='SCHEDULE SERVICE']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='SCHEDULE SERVICE']")
	private MobileElement btnScheduleService;
	
	@AndroidFindBy(xpath="//android.widget.RelativeLayout[2]/android.view.ViewGroup/android.widget.TextView[1]")
	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='map icn arrow down']//following-sibling::XCUIElementTypeStaticText)[1]")
	private MobileElement txtDealerName;
	
	@HowToUseLocators(iOSAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
	@AndroidFindBy(xpath ="//android.widget.TextView[contains(@text, 'Service Appointment')]|//android.view.View[contains(@text, 'Service Appointment')]|//android.widget.FrameLayout//android.view.View[@resource-id='root']//android.view.View[contains(@text,'Schedule A Service')]|//android.view.View[@text='Schedule A Service Appointment']|//android.view.View[@resource-id='ext-element-31']|//android.view.View/android.widget.TextView|//android.view.View[@text='Schedule A Service Appointment']|//android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.widget.TextView")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Unable to authenticate.']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='Dealership information']/XCUIElementTypeOther[1]//XCUIElementTypeStaticText|//XCUIElementTypeOther[@name='ServiceEdge Appointments Mobile']/XCUIElementTypeOther[1]//XCUIElementTypeStaticText")
	@iOSFindBy(xpath="//XCUIElementTypeStaticText[contains(@name,'Schedule A Service')]|//XCUIElementTypeOther[@name=\"navigation\"]//XCUIElementTypeLink[contains(@name,'Genesis')]|//XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[1]//XCUIElementTypeStaticText")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeButton/following-sibling::XCUIElementTypeLink||//XCUIElementTypeOther[@name='Services Page']//XCUIElementTypeOther[@name='banner']|//XCUIElementTypeOther[@name='Start Page']//XCUIElementTypeOther[@name='banner']|//XCUIElementTypeOther[@name='ServiceEdge Appointments Mobile']")
	private MobileElement txtDealerNameOnScheduleServicePage;
	
	@HowToUseLocators(iOSAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
	@AndroidFindBy(xpath ="//android.widget.TextView[contains(@text, 'Service Appointment')]|//android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.widget.TextView|//android.view.View [contains(@text, 'Service Appointment')]|//android.widget.FrameLayout//android.view.View[@resource-id='root']//android.view.View[contains(@text,'Schedule A Service')]|//android.view.View[@text='Schedule A Service Appointment']|//android.view.View[@resource-id='ext-element-31']|//android.view.View/android.widget.TextView|//android.view.View[@text='Schedule A Service Appointment']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Unable to authenticate.']")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='Dealership information']/XCUIElementTypeOther[1]//XCUIElementTypeStaticText|//XCUIElementTypeOther[@name='ServiceEdge Appointments Mobile']/XCUIElementTypeOther[1]//XCUIElementTypeStaticText")
	@iOSFindBy(xpath="//XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[1]//XCUIElementTypeStaticText|//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name=\"navigation\"]//XCUIElementTypeLink[contains(@name,'Genesis')]|//XCUIElementTypeStaticText[contains(@name,'Schedule A Service')]|//XCUIElementTypeOther[@name='navigation']/XCUIElementTypeButton/following-sibling::XCUIElementTypeLink|//XCUIElementTypeOther[@name='Services Page']//XCUIElementTypeOther[@name='banner']|//XCUIElementTypeOther[@name='Start Page']//XCUIElementTypeOther[@name='banner']|//XCUIElementTypeOther[@name='ServiceEdge Appointments Mobile']|")
	private MobileElement screenScheduleServicePage2;
	
	@HowToUseLocators(iOSAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
	@AndroidFindBy(xpath ="//android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView")
	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='Error - CDK']|//XCUIElementTypeWebView//XCUIElementTypeStaticText[contains(@name,'schedulingUrl')]")
	@iOSFindBy(xpath ="//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
	@iOSFindBy(accessibility = "{\"schedulingUrl\":null,\"success\":false,\"code\":2,\"message\":\"Scheduling Url not configured\"}")
	private MobileElement errorPageSchedulePage;
	
	@HowToUseLocators(iOSAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
	@AndroidFindBy(xpath ="//android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView")
	@iOSFindBy(xpath ="//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
	@iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='Error - CDK']/XCUIElementTypeOther)[1]|//XCUIElementTypeWebView//XCUIElementTypeStaticText[contains(@name,'schedulingUrl')]")
	@iOSFindBy(accessibility = "{\"schedulingUrl\":null,\"success\":false,\"code\":2,\"message\":\"Scheduling Url not configured\"}")
	private MobileElement txtNetworkIssue;
	

	@iOSFindBy(xpath = "//XCUIElementTypeOther[@name='main']/XCUIElementTypeOther[@name='SERVICE VALET']")
	private MobileElement ScreenValetSchedulePage;
	

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='CHARGE STATION']")
	private MobileElement _linkChargeStationPage;
	

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Close']")
	private MobileElement _linkCloseBtn;
	
	@AndroidFindBy(xpath="//androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Cancel']")
	private MobileElement _linkCancelBtn;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Doors & Trunk are closed\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Doors & Trunk are closed\"]")
	private MobileElement _linkDoorsandTrunkareClosed;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,\"United States\")]")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[contains (@value,\"United States\")]")
	private MobileElement _linkSVMaddress;
	
	@AndroidFindBy(xpath = "//android.widget.ImageView[@resource-id=\"com.stationdm.genesis:id/svmDetail_share_img\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"map svm icon share\"]")
	private MobileElement _linkSVMShareimage;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id=\"com.stationdm.genesis:id/svmDetail_count_txt\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Your last')]")
	private MobileElement _linkLast3ImageViewingText;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id=\"com.stationdm.genesis:id/svmDetail_time_btn\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeButton/XCUIElementTypeStaticText")
	private MobileElement _linklast3imagesbutton;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id=\"com.stationdm.genesis:id/svmDetail_request_btn\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"Request Images\"]")
	private MobileElement _linkRequestImagesbutton;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextView/XCUIElementTypeTextView[1]")
	private MobileElement _linkRequestImagesPagetxt;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id=\"com.stationdm.genesis:id/svmResult_title_txt\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Your SVM images are ready!\"]")
	private MobileElement _linkRequestImagesReady;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id=\"com.stationdm.genesis:id/svmConfirm_btn\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Confirm\"]")
	private MobileElement _linkRequestImagesConfirmBtn;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id=\"com.stationdm.genesis:id/svmResult_btn\"]")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"View Images Now\"]")
	private MobileElement _linkViewImagesNowBtn;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Schedule Valet Pick-Up']")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"Schedule Valet Pick-Up\"]")
	private MobileElement _linkScheduleValetPickUp;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Schedule Drop-Off']")
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"Schedule Drop-Off\"]")
	private MobileElement _linkScheduleDropoff;
	
	public boolean isPageScheduleServiceDisplay() {
		appDevice.waitForElementToLoadWithProvidedTime(pageScheduleService,5);
		return appDevice.isElementDisplayed(pageScheduleService);
	}
	
	public void clickBtnScheduleService() {
		//appDevice.waitForElementToLoad(btnScheduleService);
		btnScheduleService.click();
	}

	public boolean isScheduleValetPickUpDisplay() {
		appDevice.waitForElementToLoad(_linkScheduleValetPickUp);
		return appDevice.isElementDisplayed(_linkScheduleValetPickUp);
	}
	
	public boolean isAddressDisplay() {
		appDevice.waitForElementToLoad(labelAddress);
		return appDevice.isElementDisplayed(labelAddress);
	}
	
	public boolean isBtnScheduleServiceDisplay() {
		appDevice.waitForElementToLoad(btnScheduleService);
		return appDevice.isElementDisplayed(btnScheduleService);
	}

	public boolean isBtnSendToCarDisplay() {
		appDevice.waitForElementToLoad(btnSendToCar);
		return appDevice.isElementDisplayed(btnSendToCar);
	}
	
	public String getSetAsPreferedEnabledState() {
		appDevice.waitForElementToLoad(_mapSetAsPreferred);
		return _mapSetAsPreferred.getAttribute("enabled");
	}

	public void clickShopAccessoriesLink() {
		appDevice.waitForElementToLoad(linkShopAccessories);
		linkShopAccessories.click();
	}
	
	public void clickScheduleDropoff() {
		appDevice.waitForElementToLoad(_linkScheduleDropoff);
		_linkScheduleDropoff.click();
	}
	
	
	public boolean isShopAccessoriesLinkDisplayed() {
		return linkShopAccessories.isDisplayed();
	}

	public boolean isIconHoursDisplay() {
		appDevice.waitForElementToLoad(iconHours);
		return appDevice.isElementDisplayed(iconHours);
	}

	public boolean isBtnCallPopupDisplay() {
		appDevice.waitForElementToLoad(btnCallPopup);
		return appDevice.isElementDisplayed(btnCallPopup);
	}

	public boolean isBtnMapNavigationIconDisplay() {
		appDevice.waitForElementToLoad(btnMapNavigationIcon);
		return appDevice.isElementDisplayed(btnMapNavigationIcon);
	}

	public void clickDealersLocatorOption() {
		appDevice.waitForElementToLoad(_linkGenDealers);
		_linkGenDealers.click();
		
	}

	public void clickCarFinderOption() {
		appDevice.waitForElementToLoad(_linkCarFinder);
		_linkCarFinder.click();
	}
	
	public void clickSurroundViewMonitor() {
		appDevice.waitForElementToLoad(_linkSVMbtn);
		_linkSVMbtn.click();
	}
	
	public void clickNearByGas() {
		_linkNerebyGas.click();

	}

	public void clickOnBackButton() {
		_btnBack.click();
	}
	
	public void clickonCancelButton() {
		_linkCancelBtn.click();
	}

	public void navigateToFavorites() {
		clickPOISearch();
		tabFavorites.click();
		appDevice.sleepFor(10000);
	}
	
	public void ClickCloseBtn() {
		_linkCloseBtn.click();
	}
	
	public boolean isChargeStationDisplayed() {
		return appDevice.isElementDisplayed(_linkChargeStationPage);
	}

	public boolean txtNotfoundPopupDisplay() {
		try {
			return _txtNotfound.isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}

	}

	public void navigateToHyundaiDealer() {
		_linkGenDealers.click();
		appDevice.sleepFor(10000);
		if (txtNotfoundPopupDisplay()) {
			_btnOkPopUp.click();
		}
	}

	public void navigateToNearbyGas() {
		_linkNerebyGas.click();
		appDevice.sleepFor(10000);
		if (_txtNotfound.isDisplayed()) {
			_btnOkPopUp.click();
		}
	}

	public void enterSearchText(String zip) {
		
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("android")) {
			appDevice.waitForElementToLoad(_searchBar);
			_searchBar.click();
			_searchBar.sendKeys(zip);
			new TouchAction(driver).tap(PointOption.point(916,2045)).release().perform(); 
			
			if (appDevice.isElementDisplayed(_btnOkPopUp)) {
				_btnOkPopUp.click();
			}
		    
			appDevice.sleepFor(2000);;
		}else {
			appDevice.sleepFor(2000);
			_searchBar.click();
			_searchBar.sendKeys(zip+"\n");;
		}
	
	}

	public String searchResultGetText(String searchText) {
		return driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+ searchText+"']")).getAttribute("value");
	}

	public String clickOnSaveIcon() {
		_tooltipArrowUp.click();
		_linkSave.click();
		appDevice.sleepFor(10000);

		if (_btnOkPopUp.isDisplayed()) {
			String strMsg = _labelMesssage.getText();
			_btnOkPopUp.click();
			return strMsg;
		} else {
			return null;
		}
	}

	public boolean clickSavedPOI(String savedPOIText) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		System.out.println("ID " + parentID);
		HashMap<String, String> scrollObject = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 20; i++) {
			MobileElement poiName = appDevice.getDriver()
					.findElement((By.xpath("//XCUIElementTypeStaticText[@name='" + savedPOIText + "']")));
			if (poiName.isDisplayed()) {
				poiName.click();
				flag = true;
				break;

			} else {
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

		_tooltipArrowUp.click();
		appDevice.sleepFor(5000);
		_linkSave.isDisplayed();
		_linkSave.click();
		appDevice.sleepFor(5000);
		_btnOkPopUp.click();

		return flag;
	}

	public boolean isIconMapNewCarFinderDisplay() {

		return iconMapNewCarFinder.isDisplayed();

	}

	public boolean isMapGpsIconDisplay() {
		return iconMapGPSIcon.isDisplayed();

	}

	public String getTxtDisplayedUnderSearchTextBox() {
		return _searchBar.getAttribute("value");
	}

	public boolean verifyAddressInExpandableList(String savedPOIText) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		System.out.println("ID " + parentID);
		HashMap<String, String> scrollObject = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 50; i++) {
			try {
				MobileElement poiName = appDevice.getDriver()
						.findElement((By.xpath("//XCUIElementTypeStaticText[contains(@name,'" + savedPOIText + "')]")));
				System.out.println(appDevice.isElementDisplayed(poiName));
				if (appDevice.isElementDisplayed(poiName)) {
					poiName.click();
					flag = true;
					break;

				} else {
					appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
				}
			} catch (NoSuchElementException ex) {
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

		return flag;
	}

	public boolean verifyFullAddressInExpandableList(String savedPOIText, String strAddressLabel) {
		_poiViewList.click();

		MobileElement parent = appDevice.getDriver().findElement(By.xpath("//XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTable"));
		// identifying the parent Table
		String parentID = parent.getId();
		HashMap<String, String> scrollObject = new HashMap<>();
		HashMap<String, String> scrollObjectUp = new HashMap<>();
		scrollObject.put("element", parentID);
		scrollObject.put("direction", "down");

		boolean flag = false;
		for (int i = 0; i <= 10; i++) {
			try {

				MobileElement poiName = appDevice.getDriver()
						.findElement((By.xpath("//XCUIElementTypeStaticText[@name='"
								+ strAddressLabel +"']/following-sibling::XCUIElementTypeStaticText[contains(@name,'"
								+ savedPOIText +"')]")));

				System.out.println(appDevice.isElementDisplayed(poiName));

				if (appDevice.isElementDisplayed(poiName)) {
					poiName.click();
					flag = true;
					break;

				} else {
					appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
				}
			} catch (NoSuchElementException ex) {
				appDevice.getDriver().executeScript("mobile:scroll", scrollObject);
			}

		}

		return flag;
	}

	public String getMapLocationAddress() {
		appDevice.waitForElementToLoad(txtMapLocationAddress);
		return txtMapLocationAddress.getAttribute("value");
	}
	
	public String getMapPOILocationAddress() {
		appDevice.waitForElementToLoad(txtMapPOILocationAddress);
		return txtMapPOILocationAddress.getAttribute("value");
	}

	public boolean isSearchBarDisplay() {
		appDevice.waitForElementToLoadWithProvidedTime(_searchBar,2);
		return _searchBar.isDisplayed();

	}

	public boolean isAlertLocationServiceDisabledDisplay() {

		appDevice.waitForElementToLoadWithProvidedTime(buttonCancelAlert, 3);
		driver.switchTo().alert();
		return appDevice.isElementDisplayed(_alertLocationServiceDisabled);

	}

	public void clickCancelAlertButton() {
		buttonCancelAlert.click();
	}

	public void clickGPSIcon() {
		iconMapGPSIcon.click();

	}

	public String getMapLocationAddressLabel() {
		return labelAddress.getAttribute("value");
	}

	public String getMapDealerAddressLabel() {
		return labelDealerAddress.getAttribute("value");
	}
	
	public void clickFavorites() {
		linkFavorites.click();
	}

	public void clickSendToCar() {
		appDevice.sleepFor(60000);
		appDevice.waitForElementToLoad(btnSendToCar);
		btnSendToCar.click();
	}

	public boolean isPoiSuccessfullySentToCar() {
		appDevice.waitForElementToLoad(labelSuccessfullySentToCar);
		boolean b = appDevice.isElementDisplayed(labelSuccessfullySentToCar);
		_btnOkPopUp.click();
		return b;
	}

	public void clickPOISearch() {
		appDevice.waitForElementToLoad(_mapPoiSearch);
		_linkSearchPoi.click();
	}

	public boolean isDealerLocatorPageDisplayed() {
		appDevice.sleepFor(4000);
		appDevice.waitForElementToLoad(_mapDealerLocator);
		return _mapDealerLocator.isDisplayed();
	}

	public boolean isGasStationPageDisplayed() {
		appDevice.sleepFor(5000);
		appDevice.waitForElementToLoad(_mapGasStation);
		return _mapGasStation.isDisplayed();
	}
	
	public boolean isSVMPageDisplayed() {
		appDevice.sleepFor(5000);
		appDevice.waitForElementToLoad(_mapSVMpage);
		return _mapSVMpage.isDisplayed();
	}
	public boolean isDoorsandTrunksareClosedDisplayed() {
		appDevice.waitForElementToLoad(_linkDoorsandTrunkareClosed);
		return _linkDoorsandTrunkareClosed.isDisplayed();
	}
	
	public boolean isSVMaddressDisplayed() {
		appDevice.waitForElementToLoad(_linkSVMaddress);
		return _linkSVMaddress.isDisplayed();
	}
	
	public boolean isSVMImageShareDisplayed() {
		appDevice.waitForElementToLoad(_linkSVMShareimage);
		return _linkSVMShareimage.isDisplayed();
	}
	
	public boolean isLast3imageviewingtextDisplayed() {
		appDevice.waitForElementToLoad(_linkLast3ImageViewingText);
		return _linkLast3ImageViewingText.isDisplayed();
	}
	
	public String getLast3imageviewingtxt() {
		appDevice.waitForElementToLoad(_linkLast3ImageViewingText);
		System.out.println("current platform is: " +appDevice.getCurrentPlatform());
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("android")) {
			return _linkLast3ImageViewingText.getAttribute("text");
		}else {
			return _linkLast3ImageViewingText.getAttribute("value");
		}
		
	}
	
	public String getRequestImagePagetxt () {
		appDevice.waitForElementToLoad(_linkRequestImagesPagetxt);
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("android")) {
			return _linkRequestImagesPagetxt.getAttribute("text");
		}else {
			return _linkRequestImagesPagetxt.getAttribute("value");
		}
	}
	
	public String getImageViewingLastTimetxt () {
		appDevice.sleepFor(5000);
		appDevice.waitForElementToLoad(_linklast3imagesbutton);
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("android")) {
		return _linklast3imagesbutton.getAttribute("text");
		}
		else {
			return _linklast3imagesbutton.getAttribute("value");
		}
	}
	
	public String getSVRequestImageReadytxt () {
		appDevice.waitForElementToLoad(_linkRequestImagesReady);
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("android")) {
			return _linkRequestImagesReady.getAttribute("text");
		}else {
			return _linkRequestImagesReady.getAttribute("value");
		}
	}
	
	public boolean isSVMLast3ImageButtonDisplayed() {
		appDevice.waitForElementToLoad(_linklast3imagesbutton);
		return _linklast3imagesbutton.isDisplayed();
	}
	
	public boolean isRequestImagesButtonDisplayed() {
		appDevice.waitForElementToLoad(_linkRequestImagesbutton);
		appDevice.swipeUpScreen(2);
		return _linkRequestImagesbutton.isDisplayed();
	}
	
	
	public void clickLast3ImageButton() {
		_linklast3imagesbutton.click();
	}
	
	public void clickRequestImagesConfirmBtn() {
		_linkRequestImagesConfirmBtn.click();
	}
	
	public void clickViewImagesNowBtnBtn() {
		_linkViewImagesNowBtn.click();
	}
	
	public void clickRequestImagesBtn() {
		
		_linkRequestImagesbutton.click();
	}
	
	public boolean isFavoritesPageDisplayed() {
		appDevice.sleepFor(5000);
		appDevice.waitForElementToLoad(_mapFavourites);
		return _mapFavourites.isDisplayed();
	}

	public boolean isPoiSearchPageDisplayed() {
		appDevice.waitForElementToLoad(_mapPoiSearch);
		return _mapPoiSearch.isDisplayed();
	}

	public void clickDownArrow() {
		_tooltipArrowDown.click();
	}

	public void clickUpArrow() {
		appDevice.waitForElementToLoad(_tooltipArrowUp);
		_tooltipArrowUp.click();
	}
	
	public boolean isUpArrowDisplay() {
		appDevice.waitForElementToLoad(_tooltipArrowUp);
		return _tooltipArrowUp.isDisplayed();
	}


	public void clickSetAsPreferred() {
		appDevice.waitForElementToLoad(_mapSetAsPreferred);
		_mapSetAsPreferred.click();
	}

	public boolean islabelPreferredGenesisDealerSuccess() {
		appDevice.waitForElementToLoad(labelPreferredGenesisDealerSuccess);
		boolean b = appDevice.isElementDisplayed(labelPreferredGenesisDealerSuccess);	
		return b;
	}
	
	public void clickOkBtn() {
		_btnOkPopUp.click();
	}
	
	
	public String getDealerName() {
	//	appDevice.waitForElementToLoad(txtDealerName);
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("android")) {
			return txtDealerName.getAttribute("text");
		}else {
			return txtDealerName.getAttribute("value");
		}
		
	}
	
	
	public String getScheduleServiceNameFromScheduleServicePage() {
		appDevice.waitForElementToLoad(txtDealerNameOnScheduleServicePage);
		return txtDealerNameOnScheduleServicePage.getAttribute("name");
	}
	
	public boolean isScreenScheduleServicePage2Displayed() {
		appDevice.sleepFor(4000);
		appDevice.waitForElementToLoadWithProvidedTime(screenScheduleServicePage2,3);
		//System.out.println("Schedule Page Status" +appDevice.isElementDisplayed(screenScheduleServicePage2));
		return appDevice.isElementDisplayed(screenScheduleServicePage2);

	}
	
	public boolean isErrorScheduleServicePageDisplayed() {
		appDevice.waitForElementToLoadWithProvidedTime(errorPageSchedulePage, 3);
		return appDevice.isElementDisplayed(errorPageSchedulePage);

	}
	
	public String getNetworkIssueText() {
		if(appDevice.getCurrentPlatform().equalsIgnoreCase("android")) {
			return txtNetworkIssue.getAttribute("text");
		}else {
			return txtNetworkIssue.getAttribute("value");
		}
	}
	
	public boolean isCallBtnDisplayed() {
		return _linkPoiCall.isDisplayed();
	}
}	
