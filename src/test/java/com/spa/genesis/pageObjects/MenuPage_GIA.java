package com.spa.genesis.pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;

public class MenuPage_GIA {

	private BlueLinkAppDevice appDevice;
	private AppiumDriver<MobileElement> driver;

	private final int IMPLICIT_WAIT = 10;

	// Menu option locators
	
	@iOSFindBy(accessibility = "arrow back")
	private MobileElement _iconArrowBack;

	@iOSFindBy(accessibility = "Schedule Service")
	private MobileElement _linkScheduleServices;
	
	@iOSFindBy(accessibility = "MY RETAILER")
	private MobileElement _linkMyRetailer;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"ACCESSORIES\"]")
	private MobileElement _linkAccessories;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='MY RETAILER']")
	private MobileElement _linkDealerLocator;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='ROADSIDE ASSISTANCE']")
	private MobileElement _linkCallRoadside;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='FINANCE']")
	private MobileElement _linkFinance;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"ABOUT & SUPPORT\"]")
	private MobileElement _linkAboutAndSupport;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"SETTINGS\"]")
	private MobileElement _linkSettings;
	
	@iOSFindBy(accessibility = "SERVICE VALET")
	private MobileElement _linkServiceValet;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"ALERT SETTINGS\"]")
	private MobileElement _linkAlertSettings;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"SERVICE HISTORY TOOL\"]")
	private MobileElement _linkServiceHistoryTool;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"SWITCH VEHICLE\"]")
	private MobileElement _linkSwitchVehicle;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"LOGOUT\"]")
	private MobileElement _linkLogout;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='OK']")
	private MobileElement _btnLogOutOk;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Cancel']")
	private MobileElement _btnCancel;
	
	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Call']")
	private MobileElement _btnCall;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Would you like to exit the app and call Roadside Assistance?']")
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Would you like to exit the app and call Roadside Assistance?']")
	private MobileElement alertRoadSide;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='sdm navibar menu'])[2]")
	private MobileElement _btnCloseMenuPanel;

	@iOSFindBy(accessibility = "btn navmenu default")
	private MobileElement _btnMenu;

	@iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='MyHyundai']//XCUIElementTypeTable")
	private MobileElement _panelMenu;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Sign Out']")
	private MobileElement labelSignOut;
	
	@iOSFindBy(accessibility = "OK")
	private MobileElement _btnOkPopUp;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"ACCESSORIES\"]")
	private MobileElement labelAccessories;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Register finance account']")
	private MobileElement labelFinance;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='SERVICE HISTORY TOOL']")
	private MobileElement labelServiceHistory;

	public MenuPage_GIA(BlueLinkAppDevice empAppDevice) {
		this.appDevice = empAppDevice;
		this.driver = empAppDevice.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(this.driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
	}

	public void clickSignOut() {
		labelSignOut.click();

	}
	
	public boolean isArrowIconDisplay() {
		appDevice.waitForElementToLoadWithProvidedTime(_iconArrowBack,2);
		System.out.println("AfterMethod "+appDevice.isElementDisplayed(_iconArrowBack));
		return appDevice.isElementDisplayed(_iconArrowBack);
	}
	
	public void clickArrowBack() {
		appDevice.waitForElementToLoad(_iconArrowBack);
		_iconArrowBack.click();
	}

	public void navigateToAlertSettings() {

		appDevice.sleepFor(5000);
		int x=_linkAlertSettings.getLocation().getX();
		int y=_linkAlertSettings.getLocation().getY();
		new TouchAction(driver).press(PointOption.point(x, y)).release().perform();

	}
	
	public void logOUT() {
		appDevice.sleepFor(5000);
		_linkLogout.click();

	}

	public void navigateToScheduleServices() {
		_linkScheduleServices.click();
	}

	public void navigateToAccessories() {
		_linkAccessories.click();
	}
	
	public void navigateToServiceHistory() {
		_linkServiceHistoryTool.click();
	}

	public void navigateToDealerLocator() {
		_linkMyRetailer.click();
		_linkMyRetailer.click();
		
//		int x=_linkDealerLocator.getLocation().getX();
//		int y=_linkDealerLocator.getLocation().getY();
//		new TouchAction(driver).press(PointOption.point(x, y)).release().perform();
//		appDevice.sleepFor(5000);
	}

	public void navigateToAboutAndSupport() {
		_linkAboutAndSupport.click();
	}

	public void navigateToSettings() {
		appDevice.sleepFor(5000);
		System.out.println("SettingNavigation");
		int x=_linkSettings.getLocation().getX();
		int y=_linkSettings.getLocation().getY();
		
		new TouchAction(driver).press(PointOption.point(x,y)).release().perform();
	}

	public void navigateToSwitchVehicle() {
		_linkSwitchVehicle.click();
	}

	public void logOut() {
		_linkLogout.click();
		appDevice.waitForElementToLoad(_btnLogOutOk);
		_btnLogOutOk.click();
	}

	public void closeMenu() {
		_btnCloseMenuPanel.click();
	}

	// Popup method
	public String getRemoteResultPopUpMessageText(String remoteCommand) {

		String remoteMessage = "//XCUIElementTypeAlert//XCUIElementTypeStaticText[contains(@name,'" + remoteCommand
				+ "')]";

		appDevice.sleepFor(10000);

		appDevice.waitForElementToLoad(_btnOkPopUp);

		return driver.findElement(By.xpath(remoteMessage)).getText();

	}

	public boolean isVehicleNameDisplayed(String strVehicleName) {

		boolean flag = false;
		try {
			MobileElement vehicleName = driver
					.findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + strVehicleName + "']"));
			if (vehicleName.isDisplayed()) {
				flag = true;
			}

		} catch (Exception e) {
			e.getStackTrace();

		}
		return flag;

	}

	public void openMenu() {

		appDevice.waitForElementToLoad(_btnMenu);
		_btnMenu.click();
		//appDevice.waitForElementToLoad(_panelMenu);
	}

	public boolean isServiceHistoryToolDisplayed() {
		String LabelServiceHistory = "//XCUIElementTypeStaticText[@name=\"SERVICE HISTORY TOOL\"]\n";
		return LabelServiceHistory.equalsIgnoreCase(LabelServiceHistory);		
	}
	
	public boolean isMenuPanelDisplayed() {
		return _panelMenu.isDisplayed();
	}
	
	public String txtAboutSupportDisplayed()
	{
		return _linkAboutAndSupport.getText();
	}

	public boolean isServiceValetDisplayed() {
		try {
		return _linkServiceValet.isDisplayed();
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
	}

	public boolean isAccessoriesDisplayed() {
		String LinkAccessories = "//XCUIElementTypeStaticText[@name=\\\"ACCESSORIES\\\"]";
		return LinkAccessories.equalsIgnoreCase(LinkAccessories);
	}

	public boolean isRoadSideAssistanceDisplayed() {
		String LinkCallRoadside = "//XCUIElementTypeStaticText[@name=\\\"ROADSIDE ASSISTANCE\\\"]";
		return LinkCallRoadside.equalsIgnoreCase(LinkCallRoadside);
	}
	
	public void clickRoadSideAssistance() {
		_linkCallRoadside.click();
	}

	public boolean isMyRetailerDisplayed() {
		String LinkDealerLocator = "//XCUIElementTypeStaticText[@name=\\\"MY RETAILER\\\"]";
		return LinkDealerLocator.equalsIgnoreCase(LinkDealerLocator);
	}

	public boolean isFinanceDisplayed() {
		String LinkFinance = "//XCUIElementTypeStaticText[@name=\\\"FINANCE\\\"]";
		return LinkFinance.equalsIgnoreCase(LinkFinance);
	}

	public boolean isAccessoriesPageDisplayed() {
		return labelAccessories.isDisplayed();
	}

	public void navigateToFinance() {
		_linkFinance.click();
		
	}

	public boolean isServiceHistoryPageDisplayed() {
		return labelServiceHistory.isDisplayed();
	}

	public boolean isFinancePageDisplayed() {
		return labelFinance.isDisplayed();
	}
	
	public boolean isAlertRoadsideDisplayed() {
		appDevice.waitForElementToLoad(alertRoadSide);
		return alertRoadSide.isDisplayed();

	}
	
	public boolean isAlertBtnCallRoadsideDisplayed() {
		return _btnCall.isDisplayed();

	}
	
	public boolean isAlertBtnCancelRoadsideDisplayed() {
		return _btnCancel.isDisplayed();

	}
	
	public void clickCancelRaodside() {
		_btnCancel.click();
	}
}