package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.auto.framework.base.Log;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.EnterPinPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;



public class SVMTest extends BaseTest {
	private static BlueLinkAppDevice empAppDevice;
	private static EnterPinPage_GIA enterPinPage;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MapPage_GIA mapPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		enterPinPage = new EnterPinPage_GIA(empAppDevice);
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	@Test(description = "Test Surround View Monitor Page opening successfully", dataProvider = "TestLoginData")
	public void testSurroundViewMonitorPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);

		dashboardPage.navigateToMapOptionPage();
		mapPage.clickSurroundViewMonitor();
		
		Assert.assertTrue(mapPage.isSVMPageDisplayed(), "SVM page is not displaying.");
		Log.info("SVM page is displayed...");
		Assert.assertTrue(mapPage.isDoorsandTrunksareClosedDisplayed(), "Doors & Trunk are closed is not displaying.");
		Log.info("Doors & Trunk are closed is displayed...");
		Assert.assertTrue(mapPage.isSVMaddressDisplayed(), "SVM address is not displaying.");
		Log.info("SVM address is displayed");
		Assert.assertTrue(mapPage.isSVMImageShareDisplayed(), "SVM Image share icon is not displaying.");
		Log.info("SVM Image share icon is displayed...");
		Assert.assertTrue(mapPage.getLast3imageviewingtxt().contains("image viewings"),"Text is not correct");
		Log.info("Your last 3 image viewings text is displayed...");
		Assert.assertTrue(mapPage.isSVMLast3ImageButtonDisplayed(), "Last 3 images viewing button is not displaying.");
		Log.info("Last 3 images viewing button is displayed...");
		Assert.assertTrue(mapPage.isRequestImagesButtonDisplayed(), "Request images is not displaying.");
		Log.info("Request images button is displayed...");
		
		mapPage.clickRequestImagesBtn();
		mapPage.clickRequestImagesConfirmBtn();
		Log.info("Request images Confirm Button is pressed...");
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "The enter pin popup not appears");
		enterPinPage.enterPin(strPin);
		
		String MESSAGE = dashboardPage.getCommandsPopupMessageText("SVM Image");
		System.out.println("Message:" + MESSAGE);
		//Assert.assertTrue(MESSAGE.contains("We received a find my car SVM Image request for your"), "Text not matched");
		Assert.assertTrue(MESSAGE.contains("processed successfully"), "Process not successfull");
		dashboardPage.clickOnPopupOKButton();

		Assert.assertTrue(mapPage.getSVRequestImageReadytxt().contains("Your SVM images are ready!"),"Your SVM images are ready! Text not displayed ");
		Log.info(" Your SVM images are ready! text is displayed...");
		mapPage.clickViewImagesNowBtnBtn();
		
		Assert.assertTrue(mapPage.isSVMPageDisplayed(), "SVM page is not displaying.");
		Log.info("SVM page is displayed...");
		Assert.assertTrue(mapPage.getImageViewingLastTimetxt().contains("Today"),"Request view images updated successfully ");
		Log.info("Showing Today with time meaans SVM image request processed successfully");
		
		
	} 

	
}
