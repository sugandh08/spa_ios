package com.spa.genesis.tests;

import java.io.IOException;
import java.util.List;


import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MenuPage_GIA;
import com.spa.genesis.pageObjects.SettingsPage_GIA;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SettingsTest extends BaseTest {
	private String  strEmail, strPassword, strVin  = "";
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static SettingsPage_GIA settingsPage;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MenuPage_GIA menuPage;
	//private int i = 0;

	@Factory(dataProvider = "userDataProvider")
	public SettingsTest(String strVehicleType, String strTestRun, String strEmail, String strPassword, String strPin,
			String strVin, String strUserType) {

	
		this.strEmail = strEmail;
		this.strPassword = strPassword;
		this.strVin = strVin;
	

	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		settingsPage = new SettingsPage_GIA(empAppDevice);
		loginPage = new LoginPage_GIA(empAppDevice);
		menuPage = new MenuPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		System.out.println(strEmail);
		//i = 0;

	}

	public void relaunchAppAndInitializedVariable() throws Exception {
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		settingsPage = new SettingsPage_GIA(empAppDevice);
		loginPage = new LoginPage_GIA(empAppDevice);
		menuPage = new MenuPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
	}

	@DataProvider(name = "userDataProvider")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();

		Object[][] t = excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");

		return t;
	}

	public void loginConditions() throws Exception {
		try {
			System.out.println("Logged In using : " + strEmail);
			//dashboardPage.isUnlockButtonDisplayWithinExpectedTime(2);
			
			if(!settingsPage.isSettingsPageDisplayed())
			{
			if (!dashboardPage.isUnlockButtonDisplayWithinExpectedTime(2)) {
				loginPage.validLogin(strEmail, strPassword, strVin);
				System.out.println(dashboardPage.isUnlockButtonDisplayWithOutHandelException());
			} else {
				System.out.println("Already Login with " + strEmail);
			}
			}
			else {
				System.out.println("Setting Page Already Displayed");
			}
		} catch (NoSuchElementException ex) {
			System.out.println("Logged In Again Due To No Such Element Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		} catch (WebDriverException e) {
			System.out.println("Logged In Again Due To Wed Driver Error : " + strEmail);
			relaunchAppAndInitializedVariable();
			loginPage.validLogin(strEmail, strPassword, strVin);

		}

	}

	// Test Connected Care Settings
	@Test(description = "Connected Care- Pressing the toggle all SMS button in connected care toggles every connected care SMS setting")
	public void togglingAllConnectedCareSmsSetsAllSmsToSameState() throws Exception {

		// Reporter.log(s);
		Assert.assertTrue(true);
		loginConditions();

		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Connected Care", 1);

		String toggleState = settingsPage.toggleAllNotification("Connected Care", 1);

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Connected Care");
		for (MobileElement notificationText : allLinks) {

			Assert.assertTrue(
					settingsPage.getNotifictaionEnabledState(notificationText.getText(), "text").equals("false")
							|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(),
									"text") == toggleState,
					notificationText.getAttribute("name") + " toggle state not matched with all selected toggle");
		}

	}

	@Test(description = "Connected Care- Pressing the toggle all Email button in connected care toggles every connected care Email setting")
	public void togglingAllConnectedCareEmailSetsAllEmailsToSameState() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Connected Care", 1);

		String toggleState = settingsPage.toggleAllNotification("Connected Care", 2);

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Connected Care");
		for (MobileElement notificationText : allLinks) {

			Assert.assertTrue(
					settingsPage.getNotifictaionEnabledState(notificationText.getText(), "email").equals("false")
							|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(),
									"email") == toggleState,
					notificationText.getAttribute("name") + " toggle state not matched with all selected toggle");
		}

	}

	@Test(description = "Connected Care- Pressing the toggle all App button in connected care toggles every connected care App setting")
	public void togglingAllConnectedCareAppSetsAllAppsToSameState() throws Exception {
		loginConditions();

		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Connected Care", 1);

		String toggleState = settingsPage.toggleAllNotification("Connected Care", 3);

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Connected Care");
		for (MobileElement notificationText : allLinks) {

			Assert.assertTrue(
					settingsPage.getNotifictaionEnabledState(notificationText.getText(), "phone").equals("false")
							|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(),
									"phone") == toggleState,
					notificationText.getAttribute("name") + " toggle state not matched with all selected toggle");
		}
	}

	@Test(description = "Connected Care- Verify ACN sms toggle changes sms setting")
	public void acnSmsToggleChangesSmsSetting() throws Exception {
		System.out.println("Logged In using : " + strEmail);
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC COLLISION NOTIFICATION (ACN)", "text");

	}

	@Test(description = "Connected Care- Verify SOS sms toggle changes sms setting")
	public void sosSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("SOS EMERGENCY ASSISTANCE", "text");

	}

	@Test(description = "Connected Care- Verify Automatic Dtc sms toggle changes sms setting")
	public void automaticDtcSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC DTC", "text");

	}

	@Test(description = "Connected Care- Verify Monthly vehicle health report sms toggle changes sms setting")
	public void mvhrSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("MONTHLY VEHICLE HEALTH REPORT", "text");

	}

	@Test(description = "Connected Care- Verify Maintenance Alert sms toggle changes sms setting")
	public void maintenanceAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("MAINTENANCE ALERT", "text");

	}

	@Test(description = "Connected Care- Verify ACN sms toggle changes sms setting")
	public void acnEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);
		settingsPage.toggleAllNotification("Connected Care", 2);

		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC COLLISION NOTIFICATION (ACN)", "email");
		}

	}

	@Test(description = "Connected Care- Verify SOS Email toggle changes Email setting")
	public void sosEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);
		settingsPage.clickOnToggleButtonForNotification("SOS EMERGENCY ASSISTANCE", "email");
		}

	}

	@Test(description = "Connected Care- Verify Automatic Dtc Email toggle changes Email setting")
	public void automaticDtcEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC DTC", "email");
		}

	}

	@Test(description = "Connected Care- Verify Monthly vehicle health report Email toggle changes Email setting")
	public void mvhrEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("MONTHLY VEHICLE HEALTH REPORT", "email");
		}

	}

	@Test(description = "Connected Care- Verify Maintenance Alert Email toggle changes Email setting")
	public void maintenanceAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("MAINTENANCE ALERT", "email");
		}
	}

	@Test(description = "Connected Care- Verify ACN Phone toggle changes Phone setting")
	public void acnPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC COLLISION NOTIFICATION (ACN)", "phone");
		}

	}

	@Test(description = "Connected Care- Verify SOS Phone toggle changes Phone setting")
	public void sosPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("SOS EMERGENCY ASSISTANCE", "phone");
		}

	}

	@Test(description = "Connected Care- Verify Automatic Dtc Phone toggle changes Phone setting")
	public void automaticDtcPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("AUTOMATIC DTC", "phone");
		}

	}

	@Test(description = "Connected Care- Verify Monthly vehicle health report Phone toggle changes Phone setting")
	public void mvhrPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("MONTHLY VEHICLE HEALTH REPORT", "phone");
		}
	}

	@Test(description = "Connected Care- Verify Maintenance Alert Phone toggle changes Phone setting")
	public void maintenanceAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Connected Care", 1);

		settingsPage.clickOnToggleButtonForNotification("MAINTENANCE ALERT", "phone");
		}
	}

	// Test Remote Settings
	@Test(description = "Remote- Pressing the toggle all SMS button in Remote toggles every Remote SMS setting")
	public void togglingAllRemoteSmsSetsAllSmssToSameState() throws Exception {
		loginConditions();
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		String toggleState = settingsPage.toggleAllNotification("Remote", 1);

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Remote");
		for (MobileElement notificationText : allLinks) {

			Assert.assertTrue(
					settingsPage.getNotifictaionEnabledState(notificationText.getText(), "text").equals("false")
							|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(),
									"text") == toggleState,
					notificationText.getAttribute("name") + " toggle state not matched with all selected toggle");
		}
	}

	@Test(description = "Remote- Pressing the toggle all Email button in Remote toggles every Remote Email setting")
	public void togglingAllRemoteEmailSetsAllEmailsToSameState() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		String toggleState = settingsPage.toggleAllNotification("Remote", 2);

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Remote");
		for (MobileElement notificationText : allLinks) {

			Assert.assertTrue(
					settingsPage.getNotifictaionEnabledState(notificationText.getText(), "email").equals("false")
							|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(),
									"email") == toggleState,
					notificationText.getAttribute("name") + " toggle state not matched with all selected toggle");
		}
	}

	@Test(description = "Remote- Pressing the toggle all App button in Remote toggles every Remote App setting")
	public void togglingAllRemoteAppSetsAllAppsToSameState() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		//settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		String toggleState = settingsPage.toggleAllNotification("Remote", 3);

		List<MobileElement> allLinks = settingsPage.xpathOfAllNotificationTexts("Remote");
		for (MobileElement notificationText : allLinks) {

			Assert.assertTrue(
					settingsPage.getNotifictaionEnabledState(notificationText.getText(), "phone").equals("false")
							|| settingsPage.getNotifictaionStateOfSetting(notificationText.getText(),
									"phone") == toggleState,
					notificationText.getAttribute("name") + " toggle state not matched with all selected toggle");
		}
	}

	@Test(description = "Remote- Verify panic Notification sms toggle changes sms setting")
	public void panicNotificationSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		if(settingsPage.isSettingsPageDisplayed())
		{
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("PANIC NOTIFICATION", "text");
		}
	}

	@Test(description = "Remote- Verify Alarm Notification sms toggle changes sms setting")
	public void alarmNotificationSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("ALARM NOTIFICATION", "text");
	}

	@Test(description = "Remote- Verify Horn And Lights sms toggle changes sms setting")
	public void hornAndLightsSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("HORN AND LIGHTS/LIGHTS ONLY", "text");
	}

	@Test(description = "Remote- Verify Remote Engine Start Stop sms toggle changes sms setting")
	public void remoteEngineStartStopSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("REMOTE ENGINE START/STOP", "text");
	}

	@Test(description = "Remote- Verify Remote Door Lock Unlock sms toggle changes sms setting")
	public void remoteDoorLockUnlockSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("REMOTE DOOR LOCK/UNLOCK", "text");
	}

	@Test(description = "Remote- Verify Valet Alert sms toggle changes sms setting")
	public void valetAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("VALET ALERT", "text");
	}

	@Test(description = "Remote- Verify Geo Fence Alert sms toggle changes sms setting")
	public void geoFenceAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("GEO-FENCE ALERT", "text");
	}

	@Test(description = "Remote- Verify Speed Alert sms toggle changes sms setting")
	public void speedAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("SPEED ALERT", "text");
	}

	@Test(description = "Remote- Verify Curfew Alert sms toggle changes sms setting")
	public void curfewAlertSmsToggleChangesSmsSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("CURFEW ALERT", "text");
	}

	@Test(description = "Remote- Verify panic Notification Email toggle changes Email setting")
	public void panicNotificationEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("PANIC NOTIFICATION", "email");
	}

	@Test(description = "Remote- Verify Alarm Notification Email toggle changes Email setting")
	public void alarmNotificationEmailToggleChangesEmailSetting() throws Exception {
		System.out.println("Logged In using : " + strEmail);
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("ALARM NOTIFICATION", "email");
	}

	@Test(description = "Remote- Verify Horn And Lights Email toggle changes Email setting")
	public void hornAndLightsEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("HORN AND LIGHTS/LIGHTS ONLY", "email");
	}

	@Test(description = "Remote- Verify Remote Engine Start Stop Email toggle changes Email setting")
	public void remoteEngineStartStopEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("REMOTE ENGINE START/STOP", "email");
	}

	@Test(description = "Remote- Verify Remote Door Lock Unlock Email toggle changes Email setting")
	public void remoteDoorLockUnlockEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("REMOTE DOOR LOCK/UNLOCK", "email");
	}

	@Test(description = "Remote- Verify Valet Alert Email toggle changes Email setting")
	public void valetAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("VALET ALERT", "email");
	}

	@Test(description = "Remote- Verify Geo Fence Alert Email toggle changes Email setting")
	public void geoFenceAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("GEO-FENCE ALERT", "email");
	}

	@Test(description = "Remote- Verify Speed Alert Email toggle changes Email setting")
	public void speedAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("SPEED ALERT", "email");
	}

	@Test(description = "Remote- Verify Curfew Alert Email toggle changes Email setting")
	public void curfewAlertEmailToggleChangesEmailSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("CURFEW ALERT", "email");
	}

	@Test(description = "Remote- Verify panic Notification Phone toggle changes Phone setting")
	public void panicNotificationPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("PANIC NOTIFICATION", "phone");
	}

	@Test(description = "Remote- Verify Alarm Notification Phone toggle changes Phone setting")
	public void alarmNotificationPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("ALARM NOTIFICATION", "phone");
	}

	@Test(description = "Remote- Verify Horn And Lights Phone toggle changes Phone setting")
	public void hornAndLightsPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("HORN AND LIGHTS/LIGHTS ONLY", "phone");
	}

	@Test(description = "Remote- Verify Remote Engine Start Stop Phone toggle changes Phone setting")
	public void remoteEngineStartStopPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("REMOTE ENGINE START/STOP", "phone");
	}

	@Test(description = "Remote- Verify Remote Door Lock Unlock Phone toggle changes Phone setting")
	public void remoteDoorLockUnlockPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("REMOTE DOOR LOCK/UNLOCK", "phone");
	}

	@Test(description = "Remote- Verify Valet Alert Phone toggle changes Phone setting")
	public void valetAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("VALET ALERT", "phone");
	}

	@Test(description = "Remote- Verify Geo Fence Alert Phone toggle changes Phone setting")
	public void geoFenceAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("GEO-FENCE ALERT", "phone");
	}

	@Test(description = "Remote- Verify Speed Alert Phone toggle changes Phone setting")
	public void speedAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("SPEED ALERT", "phone");
	}

	@Test(description = "Remote- Verify Curfew Alert Phone toggle changes Phone setting")
	public void curfewAlertPhoneToggleChangesPhoneSetting() throws Exception {
		loginConditions();
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.isSettingsPageDisplayed();
		settingsPage.scrollByText("Remote", 1);

		settingsPage.clickOnToggleButtonForNotification("CURFEW ALERT", "phone");
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("AfterClass");
		driver.quit();
	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod() {
		System.out.println("AfterMethodStart");
		if (settingsPage.isArrowIconDisplay()) {
			System.out.println("AfterMethod");
		settingsPage.clickArrowBack();
	}

	}

}
