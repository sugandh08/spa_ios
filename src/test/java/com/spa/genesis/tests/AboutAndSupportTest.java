package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.AboutandSupportPage_GIA;

import com.spa.genesis.pageObjects.DashboardPage_GIA;

import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MenuPage_GIA;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class AboutAndSupportTest extends BaseTest {
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MenuPage_GIA menuPage;
	private static AboutandSupportPage_GIA aboutsupport;

	@DataProvider(name = "TestLoginData")
	public static Object[][] giaUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		menuPage = new MenuPage_GIA(empAppDevice);
		aboutsupport = new AboutandSupportPage_GIA(empAppDevice);

	}

	@Test(description = "Test submenu under Guide section in About and Support", dataProvider = "TestLoginData")
	public void testSubMenuUnderGuideSectionInAboutSupport(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is displayed");
		Assert.assertTrue(aboutsupport.isGuideHeaderDisplayed(), "Guide header is displayed");
		Assert.assertTrue(aboutsupport.issubmenuGettingStartedDisplayed(),
				"Sub menu Getting strted guide Page is displayed");
		Assert.assertTrue(aboutsupport.issubmenuIndicatorGuideDisplayed(), "Sub Menu Indicator Guide is displayed");
		Assert.assertTrue(aboutsupport.issubmenuOwnerManualDisplayed(), "Sub Menu Owner Manual is displayed");
		Assert.assertTrue(aboutsupport.issubmenuMaintenaceInfoDisplayed(),
				"Sub Menu Maintenance Information is displayed");
		Assert.assertTrue(aboutsupport.issubmenuWarrantyInfoDisplayed(), "Sub Menu Warranty Info is displayed");
		

	}

	@Test(description = "Test submenu under Genesis resources section in About and Support", dataProvider = "TestLoginData")
	public void testSubMenuUnderGenesisResourcesInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is displayed");
		Assert.assertTrue(aboutsupport.isGenesisResourceHeaderDisplayed(), "Genesis Resources header is displayed");
		Assert.assertTrue(aboutsupport.isGettingStartedDisplayed(), "Sub menu getting Started is displayed");
		Assert.assertTrue(aboutsupport.isConnectedServicesDisplayed(), "Sub menu Connected service is displayed");
		Assert.assertTrue(aboutsupport.isVehicleHealthDisplayed(), "Sub menu vehicle health is displayed");
		Assert.assertTrue(aboutsupport.isManualWarrantyDisplayed(), "Sub menu manual warranty is displayed");
		Assert.assertTrue(aboutsupport.isBluetoothAssistanceDisplayed(), "Sub menu bluetooth assistance is displayed");
		Assert.assertTrue(aboutsupport.isTechNavigationDisplayed(), "Sub menu technology and navigation is displayed");
		Assert.assertTrue(aboutsupport.isAccessoriesPartsDisplayed(), "Sub menu accessories parts is displayed");
		Assert.assertTrue(aboutsupport.isGeneralInfoDisplayed(), "Sub menu Gneral information is displayed");

	}
	@Test(description = "Test submenu under Links section in About and Support", dataProvider = "TestLoginData")
	public void testSubMenuUnderLinksSectionInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is not displayed");
		Assert.assertTrue(aboutsupport.isLinksHeaderDisplayed(), "Links header is not displayed");
		Assert.assertTrue(aboutsupport.isGenesisComDisplayed(), "Sub menu genesis com is not displayed");
		Assert.assertTrue(aboutsupport.isGenesisUSADisplayed(), "Sub menu genesis USA is not displayed");
		Assert.assertTrue(aboutsupport.isGenesisAccessoriesDisplayed(), "Sub menu genesis accessories is not displayed");
		Assert.assertTrue(aboutsupport.isGenesisCollisionCenterDisplayed(), "Sub menu genesis collision center is not displayed");
		
	}
	
	@Test(description = "Verify user can see FAQ questions when click on it", dataProvider = "TestLoginData")
	public void testFAQSectionInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is not displayed");
		Assert.assertTrue(aboutsupport.isFAQDisplayed(), "Sub menu FAQ is not displayed");
		aboutsupport.clickFAQ();
		Assert.assertTrue(aboutsupport.isFAQPageDisplayed(), "Sub menu FAQ is not displayed");
		
	}
	
	@Test(description = "Verify user can see MyGenesis.com web page when click on it", dataProvider = "TestLoginData")
	public void testmyGenesiscomLinksSectionInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is not displayed");
		Assert.assertTrue(aboutsupport.isGenesisComDisplayed(), "Sub menu genesis com is not displayed");
		aboutsupport.clickGenesisCom();
		Assert.assertTrue(aboutsupport.isGenesisComPageDisplayed(), "web page genesis.com is nitdisplayed");
	}
	
	
//	@Test(description = "Support for Genesis Virtual Guide", dataProvider = "TestLoginData")
//	public void testGenesisVitualGuideInAboutSupport(String strVehicleType, String strTestRun,
//			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {
//
//		loginPage.validLogin(strEmail, strPassword, strVIN);
//		dashboardPage.openMenu();
//		menuPage.navigateToAboutAndSupport();
//		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is not displayed");
//		Assert.assertTrue(aboutsupport.issubmenuGenesisVirtualDisplayed(),
//				"Sub Menu Genesis Virtual Guide is not displayed");
//		aboutsupport.clicksubmenuGenesisVirtual();
//		Assert.assertTrue(aboutsupport.isGenesisVirtualGuidePageDisplayed(), "genesis virtual guide page  is not displayed");
//		
//	}
	
	@Test(description = "Test submenu under Contact Us section in About and Support", dataProvider = "TestLoginData")
	public void testSubMenuUnderContactUsSectionInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is displayed");
		Assert.assertTrue(aboutsupport.isContactUsHeaderDisplayed(), "Contact Us header is displayed");
		Assert.assertTrue(aboutsupport.isEmailSupportDisplayed(), "Sub menu Email App Support is displayed");
		Assert.assertTrue(aboutsupport.isConsumerAssistanceDisplayed(), "Sub menu Consumer Assistance cemter is displayed");
		
		
	}
	@Test(description = "Test submenu under Version section in About and Support", dataProvider = "TestLoginData")
	public void testSubMenuUnderVersionSectionInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		Assert.assertFalse(menuPage.isServiceValetDisplayed(), "Service Valet is displayed");
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is displayed");
		Assert.assertTrue(aboutsupport.isVersionHeaderDisplayed(), "Version  header is displayed");
		Assert.assertTrue(aboutsupport.isFAQDisplayed(), "Sub menu FAQ is displayed");
		Assert.assertTrue(aboutsupport.isTermsConditionDisplayed(), "Sub menu Terms and Condition is displayed");
		Assert.assertTrue(aboutsupport.isPrivacyPolicyDisplayed(), "Sub menu Privacy Policy is displayed");
		aboutsupport.clickPrivacyPolicyPage();
		
		Assert.assertTrue(aboutsupport.isTitlePrivacyPolicyDisplayed(), "Privacy Policy is displayed");
		
		
		
	}
	@Test(description = "Test subheaders in About and Support", dataProvider = "TestLoginData")
	public void testSubHeaderLinksInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is displayed");
		Assert.assertTrue(aboutsupport.isGuideHeaderDisplayed(), "Guide header is displayed");
		Assert.assertTrue(aboutsupport.isGenesisResourceHeaderDisplayed(), "Genesis Resources header is displayed");
		Assert.assertTrue(aboutsupport.isLinksHeaderDisplayed(), "Links header is displayed");	
		Assert.assertTrue(aboutsupport.isContactUsHeaderDisplayed(), "Contact Us header is displayed");
		Assert.assertTrue(aboutsupport.isVersionHeaderDisplayed(), "Version  header is displayed");
	
		
	}
	
	@Test(description = "Test About and Support link in one line", dataProvider = "TestLoginData")
	public void testAboutSupportInOneLine(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		String textAboutSupport = "ABOUT & SUPPORT";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		String text = menuPage.txtAboutSupportDisplayed();
		//System.out.println(text);
		Assert.assertEquals(text, textAboutSupport,"About and Support' link in one line in place of Support and About Genesis is displayed");
		
	}

	
	@Test(description = "Test owner Manual Link in About and Support", dataProvider = "TestLoginData")
	public void testOwnerManualLinkInAboutSupport(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is displayed");	
		Assert.assertTrue(aboutsupport.issubmenuOwnerManualDisplayed(), "Sub Menu Owner Manual is displayed");
		aboutsupport.clickOwnersManual();
		Assert.assertTrue(aboutsupport.isDownloadingPopUpDisplayed(), "Downloading Owner Manual is displayed");
		aboutsupport.clickOK();
		aboutsupport.clickOwnersManual();
		Assert.assertTrue(aboutsupport.isOwnersManualDisplayed(), "Owner Manual is displayed");
		//aboutsupport.clickDone();
	}
	
	
	@Test(description = "Verify user can see Terms and Conditions page with info when click on it", dataProvider = "TestLoginData")
	public void testTermsandConditionsSectionInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		Assert.assertFalse(menuPage.isServiceValetDisplayed(), "Service Valet is displayed");
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is displayed");
		Assert.assertTrue(aboutsupport.isTermsConditionDisplayed(), "Sub menu Terms and Condition is displayed");
		aboutsupport.clickTermsCondition();
		Assert.assertTrue(aboutsupport.isTitleTermsConditonsDisplayed(), "Terms and conditons is displayed");
	}
	
	@Test(description = "Verify user can see Email App support screen when click on it and submit incident", dataProvider = "TestLoginData")
	public void testEmailAppsupportSectionInAboutSupport(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		Assert.assertFalse(menuPage.isServiceValetDisplayed(), "Service Valet is displayed");
		menuPage.navigateToAboutAndSupport();
		Assert.assertTrue(aboutsupport.isAboutAndSupportPageDisplayed(), "About and Support Page is displayed");
		Assert.assertTrue(aboutsupport.isEmailSupportDisplayed(), "Sub menu Email App Support is displayed");
		aboutsupport.clickEmailSupport();
		Assert.assertTrue(aboutsupport.isTitleEmailAppSupportDisplayed(), "Email App Support page is displayed");
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
		//empAppDevice.getServer().stop();
	}

}
