package com.spa.genesis.tests;



import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.ChargeScheduleEVPage_GIA;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ChargeSchedulesEVTest extends BaseTest{
	
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static ChargeScheduleEVPage_GIA ChargeSchedulePage;
	private static MapPage_GIA mapPage;
	
	
	@BeforeMethod(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		ChargeSchedulePage = new ChargeScheduleEVPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);		
	
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	
	
	public void relaunchAppAndInitializedVariable() throws Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		ChargeSchedulePage = new ChargeScheduleEVPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
		
	}
	
	
	
	@AfterMethod()
	public void tearDown() {
		driver.quit();
		reportEnd();
	}
	
	@Test(description = "Test ChargeSchedule Page", dataProvider = "TestLoginData")
	public void testGenesisEvpage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		Assert.assertTrue(ChargeSchedulePage.isRefreshButtonDisplayed(), "Refresh button is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isBatteryPercentageDisplayed(), "Battery Percentage is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isVehicleImageDisplayed(), "Vehicle image is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isChargeStationDisplayed(),"Charge Station is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isChargeLimitSettingsDisplayed(),"Charge Limit Settings is not displayed");
	}
	
	@Test(description = "Test ChargeSchedule Page", dataProvider = "TestLoginData")
	public void testDepartureOneTwo(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoDisplayed(), "Departure Time 2 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoToggleDisplayed(), "Departure Time 2 toggle is not displayed");
	}
	
	@Test(description = "Test Charge schedule page icon text messages and off-peak charging toggle button", dataProvider = "TestLoginData")
	public void testIconMessagesandOffPeakChargingWithtoggle(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTimeicon();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeicontextDisplayed(), "Departure Time icon text is not displayed");
		ChargeSchedulePage.ClickClimateicon();
		Assert.assertTrue(ChargeSchedulePage.isClimateicontextDisplayed(), "Climate icon text is not displayed");
		ChargeSchedulePage.ClickOffPeakChargingicon();
		Assert.assertTrue(ChargeSchedulePage.isOffPeakChargingicontextDisplayed(), "Off-Peak Charging icon text is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isOffpeakChargingdisplayed(),"Off Peak Charging is not Displayed");
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
		Assert.assertTrue(ChargeSchedulePage.isOffpeakChargingtoggledisplayed(),"Off Peak Charging toggle is not Displayed");
	}
	}
	
	@Test(description = "Test Charge schedule page Charge Station", dataProvider = "TestLoginData")
	public void testChargeStationNavigatingMaps(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeStation();
		mapPage.ClickCloseBtn();
		Assert.assertTrue(mapPage.isChargeStationDisplayed(), "Charge Station is not displayed");
		ChargeSchedulePage.ClickArrowBackbtn();
		Assert.assertTrue(ChargeSchedulePage.isChargeStationDisplayed(),"Charge Station is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isVehicleImageDisplayed(), "Vehicle image is not displayed");	
	}
	
	@Test(description = "Verify User Can set Timed Alert Value and save", dataProvider = "TestLoginData")
	public void testChargeCompletionAlert(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		Assert.assertTrue(ChargeSchedulePage.isChargeCompletionAlertDisplayed(),"Charge Completion Alert is not displayed");
		ChargeSchedulePage.ClickChargeCompletionAlert();
		Assert.assertTrue(ChargeSchedulePage.isSetTimedAlertdisplayed(),"Set Time Alert is not displayed");
		ChargeSchedulePage.ClickApplyBtn();
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");	
	}

	@Test(description = "Toggle OffPeakCharging to true/False make OffPeakPreffrence true/False", dataProvider = "TestLoginData")
	public void testOffPeakChargingTogglOnOff(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
			String value1 = ChargeSchedulePage.GetOffpeakChargingToggleValue();
			System.out.println(value1);
			ChargeSchedulePage.ClickSubmitBtn();
			ChargeSchedulePage.ClickSaveBtn();
			String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
			System.out.println("Remote Result Text:" + resultText);
			Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
			dashboardPage.clickOnPopupOKButton();
			ChargeSchedulePage.ClickChargeSchedule();
			ChargeSchedulePage.ClickoffPeakChargingToggle();
			String value2 = ChargeSchedulePage.GetOffpeakChargingToggleValue();
			System.out.println(value2);
			Assert.assertNotEquals(value1, value2,"Toggle is working ");
			String value3 = ChargeSchedulePage.GetOffpeakChargingToggleValue();
			ChargeSchedulePage.ClickoffPeakChargingToggle();
			ChargeSchedulePage.ClickSubmitBtn();
			ChargeSchedulePage.ClickSaveBtn();
			String resultText1 = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
			System.out.println("Remote Result Text:" + resultText1);
			Assert.assertTrue(resultText1.contains("successfully"), "Successfully does not display");
			dashboardPage.clickOnPopupOKButton();
			ChargeSchedulePage.ClickChargeSchedule();
			String value4 = ChargeSchedulePage.GetOffpeakChargingToggleValue();
			Assert.assertNotEquals(value3, value4,"Toggle is working ");
			}
		}	
	
	@Test(description = "Verify Off peak Charging priority only", dataProvider = "TestLoginData")
	public void testOffPeakChargingPriorityOnly(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		if(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDeparture1toggle();
		}
		if(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDeparture2toggle();
		}
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
			if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
				ChargeSchedulePage.ClickoffPeakChargingToggle();
			}
		}
		ChargeSchedulePage.ClickOffPeakCharging();
		ChargeSchedulePage.SetEndTime();		
		ChargeSchedulePage.ClickOffPeakChargingOnlybtn();
		ChargeSchedulePage.ClickOffPeakChargingPrioritybtn();
		ChargeSchedulePage.ClickOffPeakSavebtn();
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is enabled");
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
	}
	
	@Test(description = "Set Charge Schedule Off Peak charging Toggle with departure time 1 ON", dataProvider = "TestLoginData")
	public void testOffPeakChargingwithDeparture1(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		if(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDeparture1toggle();
		}
		if(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDeparture2toggle();
		}
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
			if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
			}
		}
		ChargeSchedulePage.ClickOffPeakCharging();
		//ChargeSchedulePage.SetEndTime();		
		ChargeSchedulePage.ClickOffPeakChargingOnlybtn();
		ChargeSchedulePage.ClickOffPeakChargingPrioritybtn();
		ChargeSchedulePage.ClickOffPeakSavebtn();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue), "climate toggle is enabled");
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue), "climate toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is enabled");
		
		
	}
	
	@Test(description = "Set Charge Schedule Off Peak charging Toggle with departure time 2 ON", dataProvider = "TestLoginData")
	public void testOffPeakChargingwithDeparture2(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		if(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDeparture1toggle();
		}
		if(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDeparture2toggle();
		}
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
			if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
			}
		}
		ChargeSchedulePage.ClickOffPeakCharging();
		ChargeSchedulePage.SetEndTime();		
		ChargeSchedulePage.ClickOffPeakChargingOnlybtn();
		ChargeSchedulePage.ClickOffPeakChargingPrioritybtn();
		ChargeSchedulePage.ClickOffPeakSavebtn();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue), "climate toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is not enabled");
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue), "climate toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is enabled");
		
	}
	
	@Test(description = "Set Charge Schedule Off Peak charging Toggle with departure time 1 and 2 ON", dataProvider = "TestLoginData")
	public void testOffPeakChargingwithDeparture1and2(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		if(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDeparture1toggle();
		}
		if(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDeparture2toggle();
		}
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
			if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
			}
		}
		ChargeSchedulePage.ClickOffPeakCharging();
		ChargeSchedulePage.SetBeginTime();		
		ChargeSchedulePage.ClickOffPeakChargingOnlybtn();
		ChargeSchedulePage.ClickOffPeakChargingPrioritybtn();
		ChargeSchedulePage.ClickOffPeakSavebtn();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue), "climate toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is not enabled");
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
			}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue), "climate toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is enabled");
		
	}
	
	@Test(description = "Set Begin Value and end value in off Peak Charging settings", dataProvider = "TestLoginData")
	public void testbeginandendtime(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		if(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDeparture1toggle();
		}
		if(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDeparture2toggle();
		}
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
			if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
			}
		}
		ChargeSchedulePage.ClickOffPeakCharging();
		ChargeSchedulePage.SetBeginTime();
		ChargeSchedulePage.SetEndTime();
		ChargeSchedulePage.ClickOffPeakChargingOnlybtn();
		ChargeSchedulePage.ClickOffPeakChargingPrioritybtn();
		ChargeSchedulePage.ClickOffPeakSavebtn();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue), "climate toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is enabled");
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
			}
	
	@Test(description = "Set Charge Schedule success Departure Time 1 Only", dataProvider = "TestLoginData")
	public void testDeparture1only(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime1();
		ChargeSchedulePage.ClickDepartureSaveBtn();
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
			}
	
	@Test(description = "Set Charge Schedule success Departure Time 2 Only", dataProvider = "TestLoginData")
	public void testDeparture2only(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoDisplayed(), "Departure Time 2 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoToggleDisplayed(), "Departure Time 2 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime2();

		//Tue is Day, 2 is Departure Time 2
		ChargeSchedulePage.SetDayforDeparture("Tue","2");
		
		ChargeSchedulePage.ClickDepartureSaveBtn();
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
			}
	
	@Test(description = "Set Charge Schedule success Departure Time 1 & 2", dataProvider = "TestLoginData")
	public void testDeparture1and2both(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoDisplayed(), "Departure Time 2 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoToggleDisplayed(), "Departure Time 2 toggle is not displayed");
		
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		
		ChargeSchedulePage.SetDepartureTime1();
		ChargeSchedulePage.SetDayforDeparture("Tue","1");
		ChargeSchedulePage.SetDepartureTime2();
		ChargeSchedulePage.SetDayforDeparture("Wed","2");
		ChargeSchedulePage.ClickDepartureSaveBtn();
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
			}
	
	@Test(description = "Set Departure Time success Departure Time 1 Multiple days", dataProvider = "TestLoginData")
	public void testDeparture1withmultipleday(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime1();
		ChargeSchedulePage.SetDayforDeparture("Mon","1");
		ChargeSchedulePage.SetDayforDeparture("Tue","1");
		ChargeSchedulePage.SetDayforDeparture("Wed","1");
		ChargeSchedulePage.ClickDepartureSaveBtn();
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
			}
	
	@Test(description = "Set Departure Time success Departure Time 2 Multiple days", dataProvider = "TestLoginData")
	public void testDeparture2withmultipleday(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoDisplayed(), "Departure Time 2 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoToggleDisplayed(), "Departure Time 2 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime2();

		//Tue is Day, 2 is Departure Time 2
		ChargeSchedulePage.SetDayforDeparture("Mon","2");
		ChargeSchedulePage.SetDayforDeparture("Tue","2");
		ChargeSchedulePage.SetDayforDeparture("Wed","2");
		
		ChargeSchedulePage.ClickDepartureSaveBtn();
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
			}
	
	@Test(description = "Set Charge Schedule success Charge Schedule 1 Temperature OFF", dataProvider = "TestLoginData")
	public void testDeparture1withTempOFF(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime1();
		ChargeSchedulePage.SetDayforDeparture("Mon","1");
		ChargeSchedulePage.ClickDepartureSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
		if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
		}
		}
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		}
	
	@Test(description = "Set Departure Time success Departure Time 2 Multiple days", dataProvider = "TestLoginData")
	public void testDeparture2withTempOFF(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoDisplayed(), "Departure Time 2 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoToggleDisplayed(), "Departure Time 2 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime2();

		//Tue is Day, 2 is Departure Time 2
		ChargeSchedulePage.SetDayforDeparture("Mon","2");
		
		ChargeSchedulePage.ClickDepartureSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
		if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
		}
		}
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
			}
	
	@Test(description = "Set Charge Schedule success Charge Schedule 1 Temperature ON Defrost OFF", dataProvider = "TestLoginData")
	public void testDeparture1withTempONdefrostOFF(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime1();
		ChargeSchedulePage.SetDayforDeparture("Mon","1");
		ChargeSchedulePage.ClickDepartureSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
		if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
		}
		}
		ChargeSchedulePage.GotoClimateScreen();
		if(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimateFrontDefrostToggle();
		}
		ChargeSchedulePage.ClickClimateSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		
		//Verification
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue), "climate toggle is disabled");
		ChargeSchedulePage.GotoClimateScreen();
		Assert.assertTrue(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleOFFValue), "Climate Defrost toggle is enabled");
		}
	
	@Test(description = "Set Charge Schedule success Charge Schedule 2 Temperature ON Defrost OFF", dataProvider = "TestLoginData")
	public void testDeparture2withTempONdefrostOFF(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime2();
		ChargeSchedulePage.SetDayforDeparture("Mon","2");
		ChargeSchedulePage.ClickDepartureSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
		if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
		}
		}
		ChargeSchedulePage.GotoClimateScreen();
		if(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimateFrontDefrostToggle();
		}
		ChargeSchedulePage.ClickClimateSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		
		//Verification
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue), "climate toggle is enabled");
		ChargeSchedulePage.GotoClimateScreen();
		Assert.assertTrue(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleOFFValue), "Climate Defrost toggle is enabled");
		}
	
	@Test(description = "Set Charge Schedule success Charge Schedule 1 Temperature ON Defrost ON", dataProvider = "TestLoginData")
	public void testDeparture1withTempONdefrostON(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime1();
		ChargeSchedulePage.SetDayforDeparture("Mon","1");
		ChargeSchedulePage.ClickDepartureSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
		if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
		}
		}
		ChargeSchedulePage.GotoClimateScreen();
		if(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimateFrontDefrostToggle();
		}
		ChargeSchedulePage.ClickClimateSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		
		//Verification
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue), "climate toggle is diabled");
		ChargeSchedulePage.GotoClimateScreen();
		Assert.assertTrue(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleONValue), "Climate Defrost toggle is enabled");
		}
	
	@Test(description = "Set Charge Schedule success Charge Schedule 2 Temperature ON Defrost ON", dataProvider = "TestLoginData")
	public void testDeparture2withTempONdefrostON(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		ChargeSchedulePage.SetDepartureTime2();
		ChargeSchedulePage.SetDayforDeparture("Mon","2");
		ChargeSchedulePage.ClickDepartureSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
		if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickoffPeakChargingToggle();
		}
		}
		ChargeSchedulePage.GotoClimateScreen();
		if(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimateFrontDefrostToggle();
		}
		ChargeSchedulePage.ClickClimateSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		
		//Verification
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleOFFValue), "Departure time 1 toggle is not enabled");
		Assert.assertTrue(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue), "Departure time 2 toggle is enabled");
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue), "climate toggle is enabled");
		ChargeSchedulePage.GotoClimateScreen();
		Assert.assertTrue(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleONValue), "Climate Defrost toggle is enabled");
		}
	
	@Test(description = "Verify Start Charge Message Vehicle Unplugged", dataProvider = "TestLoginData")
	public void testStartChargeMsgVehicleUnpluged(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		Assert.assertTrue(ChargeSchedulePage.isVehicleNotPluggedInDisplayed(),"Vehicle Plugged In is Displayed");
		Assert.assertTrue(ChargeSchedulePage.GetStartStopChargeTextValue().contains("START CHARGE"),"Stop Charge is Displayed");
		ChargeSchedulePage.ClickStartChargebutton();
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("Your vehicle is not charging"), "Your vehicle is not charging does not display");
		dashboardPage.clickOnPopupOKButton();	
	}
	
	
	@Test(description = "Set Temp And Front Defrost ON/OFF", dataProvider = "TestLoginData")
	public void testTempwithDefrostONOFF(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		ChargeSchedulePage.GotoClimateScreen();
		ChargeSchedulePage.SetTemperature();
		if(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimateFrontDefrostToggle();
		}
		ChargeSchedulePage.ClickClimateSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue), "climate toggle is disabled");
		ChargeSchedulePage.GotoClimateScreen();
		Assert.assertTrue(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleONValue),"Defrost toggle is off");
		
		//Temp with Front Defrost OFF
		if(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimateFrontDefrostToggle();
		}
		ChargeSchedulePage.ClickClimateSaveBtn();
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText1 = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText1);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue), "climate toggle is disabled");
		ChargeSchedulePage.GotoClimateScreen();
		Assert.assertTrue(ChargeSchedulePage.GetClimateFrontDefrostToggleValue().equalsIgnoreCase(ToggleOFFValue),"Defrost toggle is ON");
					
	}
	
	@Test(description = "Verify user can see Weekdays and Weekend option for Depature Tine", dataProvider = "TestLoginData")
	public void testDepartureWeekdaysandWeekend(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		ChargeSchedulePage.ClickDepartureTime();
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneDisplayed(), "Departure Time 1 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeOneToggleDisplayed(), "Departure Time 1 toggle is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoDisplayed(), "Departure Time 2 is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isDepartureTimeTwoToggleDisplayed(), "Departure Time 2 toggle is not displayed");
		
		if(ChargeSchedulePage.GetDTime1ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime1toggle();
		}
		if(ChargeSchedulePage.GetDTime2ToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
			ChargeSchedulePage.ClickDTime2toggle();
		}
		
		ChargeSchedulePage.SetDepartureTime1();
		ChargeSchedulePage.SetDayforDeparture("Mon","1");
		ChargeSchedulePage.SetDayforDeparture("Tue","1");
		ChargeSchedulePage.SetDayforDeparture("Wed","1");
		ChargeSchedulePage.SetDayforDeparture("Thu","1");
		ChargeSchedulePage.SetDayforDeparture("Fri","1");
		ChargeSchedulePage.SetDayforDeparture("Sat","1");
		ChargeSchedulePage.SetDayforDeparture("Sun","1");
		ChargeSchedulePage.ClickSundayDTime1();
		ChargeSchedulePage.ClickSaturdayDTime1();
		ChargeSchedulePage.SetDepartureTime2();
		ChargeSchedulePage.SetDayforDeparture("Mon","2");
		ChargeSchedulePage.SetDayforDeparture("Tue","2");
		ChargeSchedulePage.SetDayforDeparture("Wed","2");
		ChargeSchedulePage.SetDayforDeparture("Thu","2");
		ChargeSchedulePage.SetDayforDeparture("Fri","2");
		ChargeSchedulePage.SetDayforDeparture("Sat","2");
		ChargeSchedulePage.SetDayforDeparture("Sun","2");
		ChargeSchedulePage.ClickMondayDTime2();
		ChargeSchedulePage.ClickTuesdayDTime2();
		ChargeSchedulePage.ClickWeddayDTime2();
		ChargeSchedulePage.ClickThursdayDTime2();
		ChargeSchedulePage.ClickFridayDTime2();
		ChargeSchedulePage.ClickDepartureSaveBtn();
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetDeparture1DaysValue().contains("Weekdays"),"Weekdays not showing");
		Assert.assertTrue(ChargeSchedulePage.GetDeparture2DaysValue().contains("Weekends"),"Weekends not showing");
		
		}

	@Test(description = "Set Charge Schedule Off Peak Times Charge only during Off Peak Hours ON", dataProvider = "TestLoginData")
	public void testOffPeakHoursON(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		String ToggleONValue = "1";
		String ToggleOFFValue = "0";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargeSchedule();
		if(ChargeSchedulePage.GetDeparturetime1ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDeparture1toggle();
		}
		if(ChargeSchedulePage.GetDeparturetime2ToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickDeparture2toggle();
		}
		if(ChargeSchedulePage.GetClimateToggleValue().equalsIgnoreCase(ToggleONValue)) {
			ChargeSchedulePage.ClickClimatetoggle();
		}
		if(ChargeSchedulePage.isOffpeakChargingtoggledisplayed()) {
			if(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleOFFValue)) {
				ChargeSchedulePage.ClickoffPeakChargingToggle();
			}
		}
		ChargeSchedulePage.ClickOffPeakCharging();
		ChargeSchedulePage.SetEndTime();		
		ChargeSchedulePage.ClickOffPeakChargingOnlybtn();
		ChargeSchedulePage.ClickOffPeakChargingPrioritybtn();
		ChargeSchedulePage.ClickOffPeakChargingOnlybtn();
		ChargeSchedulePage.ClickOffPeakSavebtn();
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingToggleValue().equalsIgnoreCase(ToggleONValue), "Off peak toggle is enabled");
		ChargeSchedulePage.ClickSubmitBtn();
		ChargeSchedulePage.ClickSaveBtn();
		if(ChargeSchedulePage.isVehicleimmediatechargingDisplayed()) {
			dashboardPage.clickOnPopupOKButton();
		}
		String resultText = dashboardPage.getCommandsPopupMessageText("Electric Charge Schedule");
		System.out.println("Remote Result Text:" + resultText);
		Assert.assertTrue(resultText.contains("successfully"), "Successfully does not display");
		dashboardPage.clickOnPopupOKButton();
		
		ChargeSchedulePage.ClickChargeSchedule();
		Assert.assertTrue(ChargeSchedulePage.GetOffpeakChargingSettingValue().contains("Off-Peak Charging Only"),"Off-Peak Charging Priority showing");
	}

	@Test(description = "Verify AC/DC Limit setting is displayed + Verify the Ac/Dc Slider working ", dataProvider = "TestLoginData")
	public void testDCACLimit(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickEvbutton();
		ChargeSchedulePage.ClickChargelimitSettings();
		Assert.assertTrue(ChargeSchedulePage.isDCChargeLimitDisplayed(),"DC Charge Limit is not displayed");
		Assert.assertTrue(ChargeSchedulePage.isACChargeLimitDisplayed(),"AC Charge Limit is not displayed");
		ChargeSchedulePage.SetDCLimitSlider();
		Assert.assertTrue(ChargeSchedulePage.GetDCLimitValue().contains("80"),"DC Limit is not set to 80");
		ChargeSchedulePage.SetACLimitSlider();
		Assert.assertTrue(ChargeSchedulePage.GetACLimitValue().contains("60"),"DC Limit is not set to 60");
		Thread.sleep(5000);
		
	}
	
	
}

