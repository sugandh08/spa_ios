package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;

import com.spa.genesis.pageObjects.DashboardPage_GIA;

import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class DashboardTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;

	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MapPage_GIA mapPage;
	

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
		
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}


	@Test(description = "Search POI by passing zipcode as Search Parameter ", dataProvider = "TestLoginData")
	public void testSearchDashboardPoiDisablePopup(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException
	
	{
		String str_SearchText = "20001";
		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		Assert.assertTrue(dashboardPage.isSearchFielDisplay(), "Search field not present");
		dashboardPage.enterSearchText(str_SearchText);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),"Location Service Disabled alert not displayed");
	}
	

	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
