package com.spa.genesis.tests;

import java.io.IOException;


import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.auto.framework.base.Log;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.DigitalKeyPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MenuPage_GIA;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class DigitalKeyTest extends BaseTest {
	private static AppiumDriver<MobileElement> driver;
	private static BlueLinkAppDevice empAppDevice;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MenuPage_GIA menuPage;
	private static DigitalKeyPage_GIA digitalkeyPage;
	
	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		menuPage = new MenuPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		digitalkeyPage = new DigitalKeyPage_GIA(empAppDevice);
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}
	
	@Test(description = "Verify DigitalKey Page", dataProvider = "TestLoginData")
	public void testDigitalKeyPageFromDashboard(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException, InterruptedException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		Assert.assertTrue(dashboardPage.isDigitalKeyiconDisplayed(),"Digital key icon is not displayed");
		Log.info("Digital Key icon Displayed...");
		dashboardPage.clickDigitalKeyIcon();
		Assert.assertTrue(digitalkeyPage.isPageTitleDisplayed("Digital Key"), "Digital Key Page is not displayed");
		Log.info("Digital Key Page is Displayed...");
		Assert.assertTrue(digitalkeyPage.isPageHeaderTextDisplayed(), "Page Headet Text is not displayed");
		Log.info("Set up Digital Key and your smartphone can take the place of your key fob. is Displayed...");
		Assert.assertTrue(digitalkeyPage.isEasyToUseIconandTextDisplayed(), "Easy To Use Icon and Text is not displayed");
		Log.info("Easy To Use Icon and Text Displayed...");
		Assert.assertTrue(digitalkeyPage.isSecureandProtectedIconandTextDisplayed(), "Secure and Protected Icon and Text is not displayed");
		Log.info("Secure and Protected Icon and Text Displayed...");
		Assert.assertTrue(digitalkeyPage.isSharetheConvenienceIconandTextDisplayed(), "Share the Convenience Icon and Text is not displayed");
		Log.info("Share the Convenience Icon and Text Displayed...");
		digitalkeyPage.clickSetupDigitalKey();
		Assert.assertTrue(digitalkeyPage.isDigitalKeyActivationPageisDisplayed("DIGITAL KEY ACTIVATION"), "DIGITAL KEY ACTIVATION page is not displayed");
		Log.info("Digital Key Activatin Page is Displayed...");
		//Assert.assertTrue(digitalkeyPage.isLearnMoreaboutDigitalKeyisDisplayed(), "Learn More about Digital Key link is not displayed");
		//Log.info("Learn More about Digital Key link is Displayed...");
		//Assert.assertTrue(digitalkeyPage.isCancelSetupandReturnToDashboardisDisplayed(), "Cancel Setup and Return To Dashboard link is not displayed");
		//Log.info("Cancel Setup and Return To Dashboard link is Displayed");
		digitalkeyPage.clickIconi();
		Assert.assertTrue(digitalkeyPage.isPageTitleDigitalkeyhelpDisplayed(), "Digital key help page is not displayed");
		Log.info("Digital key help page is Displayed");
		Assert.assertTrue(digitalkeyPage.istxtWatchDigitalKeyVideoDisplayed(), "Watch Digital Key Video is not displayed");
		Log.info("Watch Digital Key Video text is Displayed");
		Assert.assertTrue(digitalkeyPage.istxtPhoneSupportDisplayed(), " Phone Support is not displayed");
		Log.info("Phone Support text is Displayed");
		Assert.assertTrue(digitalkeyPage.istxtDigitalKeyFAQDisplayed(), "Digital Key FAQ is not displayed");
		Log.info("Digital Key FAQ text is Displayed");
	}
	
	@AfterClass
	public void tearDown() {
		Log.info("Quiting Driver...");
		reportEnd();
		driver.quit();
	}
}
