package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;

import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class DenyLocationMapTest extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MapPage_GIA mapPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}
	
	
	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtSearchPOI(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		String STR_SEARCHTEXT = "93551";
		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		dashboardPage.clickCancelPopup();
		mapPage.ClickCloseBtn();

		Assert.assertTrue(mapPage.isPoiSearchPageDisplayed(), "Poi Search page is not displaying.");
		mapPage.enterSearchText(STR_SEARCHTEXT);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtCarFinder(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		dashboardPage.clickCancelPopup();
		mapPage.ClickCloseBtn();
		mapPage.clickCarFinderOption();
				
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledWhenSelectGPSIcon(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		dashboardPage.clickCancelPopup();
		mapPage.ClickCloseBtn();

//		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
//				"Location Service Disabled alert not displayed");
//
//		mapPage.clickCancelAlertButton();
		mapPage.clickGPSIcon();

		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");

	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtGasStation(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		//String STR_SEARCHTEXT = "93551";

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		dashboardPage.clickCancelPopup();
		mapPage.ClickCloseBtn();
		mapPage.clickNearByGas();

		//Assert.assertTrue(mapPage.isGasStationPageDisplayed(), "Gas Station page is not displaying.");
		//mapPage.enterSearchText(STR_SEARCHTEXT);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtDealerLocator(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		//String STR_SEARCHTEXT = "93551";

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		dashboardPage.clickCancelPopup();
		mapPage.ClickCloseBtn();
		mapPage.clickDealersLocatorOption();
		//Assert.assertTrue(mapPage.isDealerLocatorPageDisplayed(), "Dealer Locator page is not displaying.");
		//mapPage.enterSearchText(STR_SEARCHTEXT);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testLocationServiceDisabledAtFavorites(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException

	{
		// String STR_SEARCHTEXT = "93551";

		loginPage.validLoginSelectDenyPopup(strEmail, strPassword, strVIN);
		dashboardPage.navigateToMapOptionPage();
		dashboardPage.clickCancelPopup();
		mapPage.ClickCloseBtn();
		mapPage.navigateToFavorites();
		
		//Assert.assertTrue(mapPage.isFavoritesPageDisplayed(), "Favorites page is not displaying.");
		// mapPage.enterSearchText(STR_SEARCHTEXT);
		Assert.assertTrue(mapPage.isAlertLocationServiceDisabledDisplay(),
				"Location Service Disabled alert not displayed");
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}

