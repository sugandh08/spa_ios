package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.auto.framework.base.Log;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.CarCarePage_GIA;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class CarCareTest   extends BaseTest{
	
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static CarCarePage_GIA carCarePage;
	private static MapPage_GIA mapPage;
	
	
	
	
	@BeforeMethod(alwaysRun = true)
	public void beforeClass() throws IOException, Exception {
		//Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		carCarePage = new CarCarePage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
	

	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	
	
	public void relaunchAppAndInitializedVariable() throws Exception {
		//Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		carCarePage = new CarCarePage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
	}
	
	
	
	@AfterMethod()
	public void tearDown() {
		driver.quit();
	}

	
	@Test(description = "Test MVR History Page", dataProvider = "TestLoginData")
	public void testMVRHistoryPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickBtnCareCare();
		carCarePage.clickDiagnosticReport();
		if(carCarePage.isMonthlyVehicleReportPageDisplayed())
		{
			Assert.assertTrue(carCarePage.isMonthlyVehicleReportPageDisplayed(), "MVR is not displayed");
			carCarePage.clickMVRHistory();	
			Assert.assertTrue(carCarePage.isMVRHistoryDisplayed(), "MVR History is not displayed");
		}
		
	}
	
	
	
	@Test(description = "Test MVR Diagnostic Page", dataProvider = "TestLoginData")
	public void testMVRDiagnosticPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickBtnCareCare();
		carCarePage.clickDiagnosticReport();
		if(carCarePage.isMonthlyVehicleReportPageDisplayed())
		{
			Assert.assertTrue(carCarePage.isMonthlyVehicleReportPageDisplayed(), "MVR is not displayed");
			carCarePage.clickMVRDiagnostics();
			Assert.assertTrue(carCarePage.isAllSystemNormalDisplayed(),"All system normal is not displayed");	
		}

	}
	
	@Test(description = "Test Diagnostic Page", dataProvider = "TestLoginData")
	public void testDiagnosticPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickBtnCareCare();
		carCarePage.clickDiagnosticReport();
		if(carCarePage.isMonthlyVehicleReportPageDisplayed())
		{
			Assert.assertTrue(carCarePage.isMonthlyVehicleReportPageDisplayed(), "MVR is not displayed");
			Assert.assertTrue(carCarePage.isAllSystemNormalDisplayed(),"All system normal is not displayed");	
			Assert.assertTrue(carCarePage.isbtnScheduleServiceDisplayed(), "Schedule Service button not appears");
			Assert.assertTrue(carCarePage.isViewVehicleHealthDisplayed()," View Vehicle Health button  not appears");
		}

	}
	
	@Test(description = "Test Diagnostic Button Schedule Service Page", dataProvider = "TestLoginData")
	public void testDiagnosticBtnScheduleServicePage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickBtnCareCare();
		carCarePage.clickDiagnosticReport();
		if(carCarePage.isMonthlyVehicleReportPageDisplayed())
		{
			Assert.assertTrue(carCarePage.isMonthlyVehicleReportPageDisplayed(), "MVR is not displayed");
			Assert.assertTrue(carCarePage.isAllSystemNormalDisplayed(),"All system normal is not displayed");	
			Assert.assertTrue(carCarePage.isbtnScheduleServiceDisplayed(), "Schedule Service button not appears");
			carCarePage.clickBtnScheduleService();
			if (dashboardPage.isBtnDropOffDisplay()) 
				dashboardPage.clickBtnDropOff();
			if (mapPage.isPageScheduleServiceDisplay()) 
				Assert.assertTrue(mapPage.isScreenScheduleServicePage2Displayed(),"Schedule Service Page not displayed");
			else {
				boolean testText=dashboardPage.getPopupMessage().contains("Online scheduling is not currently available for this dealer.");
				Assert.assertTrue(testText,"Online scheduling is not currently available for this dealer. this pop up is not displayed");
				Assert.assertTrue(dashboardPage.isCancelButtonDisplay(),"Alert Cancel Button Displayed");
				dashboardPage.clickCancelButton();
			}
		}

	}
	
	@Test(description = "Test Diagnostic Button View Vehicle Health Page", dataProvider = "TestLoginData")
	public void testDiagnosticBtnViewVehicleHealthPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickBtnCareCare();
		carCarePage.clickDiagnosticReport();
		if(carCarePage.isMonthlyVehicleReportPageDisplayed())
		{
			Assert.assertTrue(carCarePage.isMonthlyVehicleReportPageDisplayed(), "MVR is not displayed");
			Assert.assertTrue(carCarePage.isAllSystemNormalDisplayed(),"All system normal is not displayed");	
			Assert.assertTrue(carCarePage.isbtnScheduleServiceDisplayed(), "Schedule Service button not appears");
			carCarePage.clickBtnViewVehicleHealth();
			Assert.assertTrue(carCarePage.isVehicleHealthLabelDisplayed(), "Vehicle Health Page not appears");
		}

	}
	
	@Test(description = "Test Bluetooth Page", dataProvider = "TestLoginData")
	public void testBluetoothPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickBtnCareCare();
		carCarePage.clickBluetooth();
		Assert.assertTrue(carCarePage.isBluetoothLabelDisplayed(), "Error occured while opening Bluetooth Page !!");	
		Log.info("Bluetooth Page Opened Successfully!!");
		Log.info("!!-------Test Completed!!----------");
	}

	@Test(description = "Test Owner Manual Page", dataProvider = "TestLoginData")
	public void testOwnerManualPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickBtnCareCare();
		carCarePage.clickOwnerManual();
		if(carCarePage.isAlertDownloadDisplayed()) {
		Assert.assertTrue(carCarePage.isAlertDownloadDisplayed(), "Alert is not displayed");
		Assert.assertTrue(carCarePage.isAlertOkDisplayed(),"Alert ok Button is not displayed");	
		carCarePage.clickOkDownload();
		carCarePage.clickOwnerManual();
		}
		Assert.assertTrue(carCarePage.isDonedisplayed(), "Done button not displayed");
		
	}
	

	@Test(description = "Test Schedule Service Page", dataProvider = "TestLoginData")
	public void testScheduleServicePage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickBtnCareCare();
		carCarePage.clickDiagnosticReport();
		carCarePage.clickBtnScheduleService();
		if (dashboardPage.isBtnDropOffDisplay()) 
			dashboardPage.clickBtnDropOff();
		if (mapPage.isPageScheduleServiceDisplay()) {
			Assert.assertTrue(mapPage.isScreenScheduleServicePage2Displayed(),"Schedule Service Page not displayed");
		}
		else {
			boolean testText=dashboardPage.getPopupMessage().contains("Online scheduling is not currently available for this dealer.");
		Assert.assertTrue(testText,"Online scheduling is not currently available for this dealer. this pop up is not displayed");
		Assert.assertTrue(dashboardPage.isCancelButtonDisplay(),"Alert Cancel Button Displayed");
		dashboardPage.clickCancelButton();
		}
	}
	
	//No RoadSide Assistance option present under car care option
//	@Test(description = "Test RoadSide Assistance Page", dataProvider = "TestLoginData")
//	public void testRoadSideAssistancePage(String strVehicleType, String strTestRun, String strEmail,
//			String strPassword, String strPin, String strVIN, String strUserType) throws Exception {
//		
//		loginPage.validLogin(strEmail, strPassword, strVIN);
//		dashboardPage.clickBtnCareCare();
//		carCarePage.clickRoadSideAssistaence();
//		Assert.assertTrue(carCarePage.isAlertRoadsideDisplayed(), "Alert is not displayed");
//		Assert.assertTrue(carCarePage.isAlertBtnCallRoadsideDisplayed(),"Alert Call Button is not displayed");	
//		Assert.assertTrue(carCarePage.isAlertBtnCancelRoadsideDisplayed()," Alert Cancel Button button not appears");
//		carCarePage.clickCancelRaodside();
//
//	}
}
