package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.ClimateSettingsPage_GIA;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.EnterPinPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.RemoteActionsPage_GIA;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class RemotePresetTest extends BaseTest {


	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static LoginPage_GIA loginPage;
	private static EnterPinPage_GIA enterPinPage;
	private static RemoteActionsPage_GIA remoteActionsPage;
	private static ClimateSettingsPage_GIA climateSettingsPage;
	private static DashboardPage_GIA dashboardPage;


	@DataProvider(name = "TestLoginData")
	public static Object[][] giaUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		enterPinPage = new EnterPinPage_GIA(empAppDevice);
		remoteActionsPage = new RemoteActionsPage_GIA(empAppDevice);
		climateSettingsPage = new ClimateSettingsPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
		// empAppDevice.getServer().stop();
	}

//	@Test(description = "Remote History Test", dataProvider = "TestLoginData")
//	public void testRemoteHistoryStatus(String strVehicleType,
//			String strTestRun, String strEmail, String strPassword, String strPin, String strVin) throws IOException, InterruptedException {
//
//		loginPage.validLogin(strEmail, strPassword, strVin);
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnStartButton();
//		climateSettingsPage.clickOnbtnStartVehicleWithoutPresets();
//		climateSettingsPage.clickOnbtnbtnStartVehicleNow();
//		enterPinPage.enterPin(strPin);
//		String REMOTE_COMMAND = "Remote Start";
//		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
//		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);
//		dashboardPage.clickOnPopupOKButton();
//		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
//				"The Remote Start command was not successful");
//		Thread.sleep(3000);
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnStopButton();
//		remoteActionsPage.climateoffSuccessPopup();
//		dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnVehicleStatusButton();
//		remoteActionsPage.clickRefreshIcon();
//		Thread.sleep(3000);
//		Assert.assertTrue(remoteActionsPage.isVehicleStatusPageDisplayed(),"Vehicle Status page is not visible");
//		remoteActionsPage.clickOnRemoteHistoryButton();
//		Assert.assertTrue(remoteHistoryPage.isPresent(),"Remote History page is not visible");
//		Assert.assertTrue(remoteHistoryPage.getMostRecentCommandName().contains("SUCCESS"),"Remote Success Command is not shown");
//		Assert.assertTrue(remoteHistoryPage.getMostRecentFailedCommandStatus().contains("?"),"Remote Failed Command is not shown");
//	}

//	@Test(description = "Remote History Test", dataProvider = "TestLoginData")
//	public void verifystatusrefresh(String strVehicleType,
//			String strTestRun, String strEmail, String strPassword, String strPin, String strVin) throws IOException, InterruptedException {
//
//		loginPage.validLogin(strEmail, strPassword, strVin);
//	dashboardPage.clickRemoteOption();
//		remoteActionsPage.clickOnVehicleStatusButton();
//		remoteActionsPage.clickRefreshIcon();
//		Thread.sleep(3000);
//		Assert.assertFalse(remoteActionsPage.isVehicleStatusPageDisplayed(),"Vehicle Status page is not visible");
//		
//	}

	@Test(description = "Presets options are displayed / Vehicle starts successful via Remote preset start using PIN", dataProvider = "TestLoginData")
	public void testPresetsoptions(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws InterruptedException {

		String REMOTE_COMMAND = "Remote Climate Control Start";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartButton();
		Assert.assertTrue(remoteActionsPage.isLblPreset1Displayed(), "Preset1 is not displayed");
		Assert.assertTrue(remoteActionsPage.isLblPreset2Displayed(), "Preset2 is not displayed");
		Assert.assertTrue(remoteActionsPage.isLblPreset3Displayed(), "Preset3 is not displayed");
		Assert.assertTrue(remoteActionsPage.isLblPreset4Displayed(), "Preset4 is not displayed");
		Assert.assertTrue(remoteActionsPage.isLblPresetStartvehiclewithoutpresetsDisplayed(),
				"Preset Start vehicle without presets is not displayed");

		climateSettingsPage.clickOnbtnStartVehicleWithoutPresets();
		// climateSettingsPage.setTemperatureScrollValue(TEMERATURE_VALUE);
		// climateSettingsPage.setStartSettingsStates(FRONT_DEFROST_VALUE,
		// TEMERATURE_VALUE);
		climateSettingsPage.clickOnbtnbtnStartVehicleNow();
		enterPinPage.enterPin(strPin);
		Thread.sleep(5000);
		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);
		dashboardPage.clickOnPopupOKButton();
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Start command was not successful");
		Thread.sleep(3000);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStopButton();
		remoteActionsPage.climateoffSuccessPopup();

	}

	@Test(description = "Verify presets default-presets functionality/Verify Presets name is editable", dataProvider = "TestLoginData")
	public void testDefaultpreset(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) {

		String DefaultPresetValue = "1";
		String Presetname = "Presetnamerename";
		String Presetname2 = "Winter";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartButton();
		Assert.assertTrue(remoteActionsPage.isLblPreset2Displayed(), "Preset2 is not displayed");
		remoteActionsPage.clickOnSecondPresetssssetting();
		remoteActionsPage.setDefaultPreset(DefaultPresetValue);
		remoteActionsPage.clickOnPresetsSettingSubmitBtn();
		Assert.assertTrue(remoteActionsPage.isIconDefaultPresetDisplayed(), "Default Preset star is not displayed");
		remoteActionsPage.clickOnSecondPresetssssetting();
		Assert.assertTrue(remoteActionsPage.DefaultPresettoggleisEnabledDisplayed());
		remoteActionsPage.ClearandRenamePresetName(Presetname);
		remoteActionsPage.clickOnPresetsSettingSubmitBtn();
		remoteActionsPage.clickOnSecondPresetssssetting();
		Assert.assertTrue(remoteActionsPage.RenamedPresetName(Presetname));
		remoteActionsPage.ClearandRenamePresetName(Presetname2);
		remoteActionsPage.clickOnPresetsSettingSubmitBtn();
		remoteActionsPage.clickOnSecondPresetssssetting();
		Assert.assertTrue(remoteActionsPage.RenamedPresetName(Presetname2));
	}

	@Test(description = "Verify presets are getting updated successfully", dataProvider = "TestLoginData")
	public void testPresetsUpdatedSuccessfully(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartButton();
		remoteActionsPage.clickOnSecondPresetssssetting();

		String FrontDefrosterBeforeupdate = remoteActionsPage.getFrontDefrostertogglevalue();
		// String SeatsBeforeupdate = remoteActionsPage.getseatstogglevalue();
		remoteActionsPage.clickFrontDefrostertoggleSwitch();
		// remoteActionsPage.clickSeatstoggleSwitch();
		remoteActionsPage.clickOnPresetsSettingSubmitBtn();
		remoteActionsPage.clickOnSecondPresetssssetting();
		String FrontDefrosterAfterupdate = remoteActionsPage.getFrontDefrostertogglevalue();
		// String SeatsAfterupdate = remoteActionsPage.getseatstogglevalue();
		Assert.assertNotEquals(FrontDefrosterBeforeupdate, FrontDefrosterAfterupdate);
		// Assert.assertNotEquals(SeatsBeforeupdate,SeatsAfterupdate );

	}

	@Test(description = "Verify Rear and side mirror defroster and heated steering wheel are showing separatly under remote presets for Gen2.5 Vehicle", dataProvider = "TestLoginData")
	public void testRearandSideorHeatedSteeringshowingseperatly(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartButton();
		remoteActionsPage.clickOnSecondPresetssssetting();

		Assert.assertTrue(remoteActionsPage.isLblRearandSideMirrorsDefrosterDisplayed(),
				"RearandSideMirrorsDefroster is not Displayed");
		Assert.assertTrue(remoteActionsPage.isLblHeatedSteeringWheelDisplayed(),
				"HeatedSteeringWheel is not Displayed");
	}

	@Test(description = "Verify Presets options are displayed for secondary driver", dataProvider = "TestLoginData")
	public void testPresetoptionforSecondaryDriver(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) {

		String Email = "anudeep.gautam@infogain.com";
		String Password = "test123456";
		String VIN = "KMUKEDTB4PU002154";

		loginPage.validLoginSecondaryDriver(Email, Password, VIN);
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStartButton();
		Assert.assertTrue(remoteActionsPage.isLblPreset1Displayed(), "Preset1 is not displayed");
		Assert.assertTrue(remoteActionsPage.isLblPreset2Displayed(), "Preset2 is not displayed");
		Assert.assertTrue(remoteActionsPage.isLblPreset3Displayed(), "Preset3 is not displayed");
		Assert.assertTrue(remoteActionsPage.isLblPreset4Displayed(), "Preset4 is not displayed");
		Assert.assertTrue(remoteActionsPage.isLblPresetStartvehiclewithoutpresetsDisplayed(),
				"Preset Start vehicle without presets is not displayed");
	}
//	
//	@Test(description = "Verify vehicle starts successful via Remote preset start using all Preset option", dataProvider = "TestLoginData")
//    public void testPresetAllStart(String strVehicleType, String strTestRun, String strEmail,    
//			String strPassword, String strPin, String strVIN, String strUserType) {
//		
//			
//			loginPage.validLogin(strEmail, strPassword, strVIN);
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStartButton();
//			//Preset 1 START
//			remoteActionsPage.clickPresetStart1button();
//			enterPinPage.enterPin(strPin);
//			remoteActionsPage.climateOnSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStopButton();
//			remoteActionsPage.climateoffSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStartButton();
//			//Preset 2 START
//			remoteActionsPage.clickPresetStart2button();
//			enterPinPage.enterPin(strPin);
//			remoteActionsPage.climateOnSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStopButton();
//			remoteActionsPage.climateoffSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStartButton();
//			//Preset 3 START
//			remoteActionsPage.clickPresetStart3button();
//			enterPinPage.enterPin(strPin);
//			remoteActionsPage.climateOnSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStopButton();
//			remoteActionsPage.climateoffSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStartButton();
//			//Preset 4 START
//			remoteActionsPage.clickPresetStart4button();
//			enterPinPage.enterPin(strPin);
//			remoteActionsPage.climateOnSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStopButton();
//			remoteActionsPage.climateoffSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStartButton();
//			//Preset Start Vehicle Without Preset
//			climateSettingsPage.clickOnbtnStartVehicleWithoutPresets();
//			climateSettingsPage.clickOnbtnbtnStartVehicleNow();
//			enterPinPage.enterPin(strPin);
//			remoteActionsPage.climateOnSuccessPopup();
//			dashboardPage.clickRemoteOption();
//			remoteActionsPage.clickOnStopButton();
//			remoteActionsPage.climateoffSuccessPopup();
//			
//			
//			
//		
//			
//			
//	}
}
