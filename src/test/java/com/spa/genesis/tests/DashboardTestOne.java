package com.spa.genesis.tests;

import java.io.IOException;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.ClimateSettingsPage_GIA;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.EnterPinPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;
import com.spa.genesis.pageObjects.MenuPage_GIA;
import com.spa.genesis.pageObjects.RemoteActionsPage_GIA;
import com.spa.genesis.pageObjects.SettingsPage_GIA;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class DashboardTestOne extends BaseTest {

	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;

	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MapPage_GIA mapPage;
	private static ClimateSettingsPage_GIA climateSettingsPage;
	private static MenuPage_GIA menuPage;
	private static SettingsPage_GIA settingsPage;
	private static EnterPinPage_GIA enterPinPage;
	private static RemoteActionsPage_GIA remoteActionsPage;

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);
		climateSettingsPage = new ClimateSettingsPage_GIA(empAppDevice);
		menuPage = new MenuPage_GIA(empAppDevice);
		settingsPage = new SettingsPage_GIA(empAppDevice);
		enterPinPage = new EnterPinPage_GIA(empAppDevice);
		remoteActionsPage = new RemoteActionsPage_GIA(empAppDevice);

	}

	@DataProvider(name = "TestLoginData")
	public static Object[][] myhUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	@Test(description = "Verify the elements present in dashboard", dataProvider = "TestLoginData")
	public void testElementsPresentOnDashboard(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException 
	{

		String strCurrentLocation = "Kanpur";

		loginPage.validLogin(strEmail, strPassword, strVIN);
		//Assert.assertTrue(dashboardPage.isWeatherTempratureIconDisplayed(), "Weather icon not displayed");
		Assert.assertTrue(dashboardPage.isCurrentLocationDisplay(strCurrentLocation), "Current Location not displayed");
		Assert.assertTrue(dashboardPage.isRemoteStartButtonDisplay(), "Remote Start button not displayed");
		Assert.assertTrue(dashboardPage.isUnlockButtonDisplay(), "Remote Unlock button not displayed");
		Assert.assertTrue(dashboardPage.isLockButtonDisplay(), "Remote Lock button not displayed");
	}
	
	@Test(description = "Search POI by passing zipcode as Search Parameter", dataProvider = "TestLoginData")
	public void testSearchDashboardPOI(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException

	{
		String str_SearchText = "Abu Dhabi";
		loginPage.validLogin(strEmail, strPassword, strVIN);

		Assert.assertTrue(dashboardPage.isSearchFielDisplay(), "Search field not present");
		dashboardPage.enterSearchText(str_SearchText);
		Assert.assertTrue(mapPage.isPoiSearchPageDisplayed(), "Poi Search page is not displaying.");
		Assert.assertTrue(mapPage.searchResultGetText(str_SearchText).contains(str_SearchText), "Address Not Matched");
	}
	
	@Test(description = "Verify lock button ", dataProvider = "TestLoginData")
	public void testLockButton(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		String REMOTE_COMMAND = "Remote Door Lock";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickLockButton();
		//Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		//Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
		//enterPinPage.enterPin(strVIN);

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);

		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);

		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Door Lock command was not successful");
	}
	
	@Test(description = "Verify add event ", dataProvider = "TestLoginData")
	public void testAddEvent(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);

		Assert.assertTrue(dashboardPage.isLinkAddEventDisplay(), "Add Event link not displayed");
		dashboardPage.clickAddEvent();
		dashboardPage.clickAddEventFromAddEventWindow();
		Assert.assertTrue(dashboardPage.isCancelButtonDisplay(), "Cancel button not displayed");
		Assert.assertTrue(dashboardPage.isAddEventButtonPopupDisplay(), "Add Event button not displayed");
		Assert.assertTrue(dashboardPage.getResultPopUpMessageText("Add Event").contains("Add an event with location and start time in calendar app to get Genesis alert."),
				"Add Event popup not displayed");
	}
	
	@Test(description = "Verify long press the remote start by toggle on the quick start", dataProvider = "TestLoginData")
	public void testByDefaultQuickStartToggle(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		String strToggleValue = settingsPage.getQuickStartToggleOnOffValue();
		//System.out.println(strToggleValue);
		Assert.assertEquals(strToggleValue, "0", "The Quick Start toggle is off by default");
	}
	
	@Test(description = "Verify long press the remote unlock by toggle on the quick start", dataProvider = "TestLoginData")
	public void testUnlockLongPress(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		clickQuickStartToggleAndBackToArrowButton();
		dashboardPage.longPressOnUnlockButton();
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
	}
	
	@Test(description = "Verify long press the remote lock by toggle on the quick start", dataProvider = "TestLoginData")
	public void testLockLongPress(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		clickQuickStartToggleAndBackToArrowButton();
		dashboardPage.longPressOnLockButton();
		String REMOTE_COMMAND = "Remote Door Lock";
		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
		System.out.println("Remote Command Message:" + REMOTE_COMMAND_MESSAGE);
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Door Lock command was not successful");
		//Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
	}
	
	@Test(description = "Verify tutorial page  ", dataProvider = "TestLoginData")
	public void testTutorialPage(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.enterLoginCredentialPerformLogin(strEmail, strPassword);
		loginPage.handelSwichVehicle(strVIN);

		Assert.assertTrue(dashboardPage.isLabelQuickStartTutorialDisplay(), "Quick Star Tutorial not displayed");
		Assert.assertTrue(dashboardPage.isLabelPresetTutorialDisplay(), "Preset Tutorial not displayed");
		Assert.assertTrue(dashboardPage.isLabelLockTutorailDisplay(), "Lock Tutorial not displayed");
		Assert.assertTrue(dashboardPage.isLabelUnLockTutorialDisplay(), "Unlock Tutorial not displayed");
		Assert.assertTrue(dashboardPage.isLabelSettingTutorailDisplay(), "Setting Tutorial not displayed");
		Assert.assertTrue(dashboardPage.isLabelRemoteFeatureTutorialDisplay(), "Remote Feature Tutorial not displayed");
		dashboardPage.clickGoToSettingBtn();
		loginPage.clickOkPopupButton();
		loginPage.clickAllAfterExitPopup();
		Assert.assertTrue(settingsPage.isSettingsPageDisplayed(), "Setting Page not displayed");

	}
	
	@Test(description = "Verify remote unlock button ", dataProvider = "TestLoginData")
	public void testUnLockButton(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		String REMOTE_COMMAND = "Remote Door Unlock";
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.clickUnLockButton();
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
		enterPinPage.enterPin(strPin);

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Door UnLock command was not successful");
	}
	
	@Test(description = "Verify long press the remote start by toggle on the quick start", dataProvider = "TestLoginData")
	public void testQuickStartLongPress(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		clickQuickStartToggleAndBackToArrowButton();
		dashboardPage.longPressOnRemoteStartButton();
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		//Assert.assertTrue(climateSettingsPage.isSwitchDefrostDisplay(), "Defrost not displayed");
	}

	@Test(description = "Verify quick remote start by toggle on the quick start  ", dataProvider = "TestLoginData")
	public void testQuickStart(String strVehicleType, String strTestRun, String strEmail, String strPassword,
			String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		clickQuickStartToggleAndBackToArrowButton();
		dashboardPage.clickOnRemoteStarttButton();
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		Assert.assertTrue(enterPinPage.isEnterPinPopupDisplayed(), "Enter Pin page not displayed");
	}

	@Test(description = "Verify navigate to remote start and remote stop", dataProvider = "TestLoginData")
	public void testNavigateToRemoteStartPage(String strVehicleType, String strTestRun, String strEmail,
			String strPassword, String strPin, String strVIN, String strUserType) throws IOException {
		loginPage.validLogin(strEmail, strPassword, strVIN);
		String REMOTE_COMMAND = "Start";
		String REMOTE_COMMAND2 = "Stop";

		dashboardPage.clickOnRemoteStarttButton();
		//add preset part
		Assert.assertTrue(climateSettingsPage.isRemoteFeaturePageDisplay(), "Remote Feature page not displayed");
		remoteActionsPage.clickOnstartVehicleWithoutPrestsssetting();
		Assert.assertTrue(climateSettingsPage.isSwitchDefrostDisplay(), "Defrost not displayed");
		//climateSettingsPage.setStartSettingsStates(FRONT_DEFROST_VALUE, HEATED_ACCESSORIES_VALUE);
		climateSettingsPage.clickOnbtnbtnStartVehicleNow();
		enterPinPage.enterPin(strPin);
		//dashboardPage.clickOnPopupOKButton();

		String REMOTE_COMMAND_MESSAGE = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND);
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE.contains("was successful"),
				"The Remote Start command was not successful");

		climateSettingsPage.clickOnRemoteCommandOKButton();
		dashboardPage.clickRemoteOption();
		remoteActionsPage.clickOnStopButton();
		//enterPinPage.enterPin(strPin);
		//dashboardPage.clickOnPopupOKButton();

		String REMOTE_COMMAND_MESSAGE2 = dashboardPage.getCommandsPopupMessageText(REMOTE_COMMAND2);
		Assert.assertTrue(REMOTE_COMMAND_MESSAGE2.contains("was successful"),
				"The Remote Stop command was not successful");
	}
	
	
	
	
	private void clickQuickStartToggleAndBackToArrowButton() {
		dashboardPage.openMenu();
		menuPage.navigateToSettings();
		settingsPage.clickQuickStartToggle();
		settingsPage.clickArrowBack();

	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	
}
