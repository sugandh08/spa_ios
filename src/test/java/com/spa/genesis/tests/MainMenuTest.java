package com.spa.genesis.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.auto.framework.base.BaseTest;
import com.myh_genesis_spa.utils.BlueLinkAppDevice;
import com.myh_genesis_spa.utils.BlueLinkAppDeviceFactory;
import com.myh_genesis_spa.utils.DeviceConfiguration;
import com.myh_genesis_spa.utils.ExcelReader;
import com.spa.genesis.pageObjects.DashboardPage_GIA;
import com.spa.genesis.pageObjects.LoginPage_GIA;
import com.spa.genesis.pageObjects.MapPage_GIA;
import com.spa.genesis.pageObjects.MenuPage_GIA;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class MainMenuTest extends BaseTest {
	private static BlueLinkAppDevice empAppDevice;
	private static AppiumDriver<MobileElement> driver;
	private static LoginPage_GIA loginPage;
	private static DashboardPage_GIA dashboardPage;
	private static MenuPage_GIA menuPage;
	private static MapPage_GIA mapPage;

	@DataProvider(name = "TestLoginData")
	public static Object[][] giaUserValidation() throws Exception {
		ExcelReader excelReader = new ExcelReader();
		return excelReader.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/TestingData/LoginData _GIA.xlsx", "Users");
	}

	@BeforeMethod(alwaysRun = true)
	public static void beforeClass() throws IOException, Exception {
		Thread.sleep(3000);
		empAppDevice = BlueLinkAppDeviceFactory.create(DeviceConfiguration.getInstance());
		driver = empAppDevice.getDriver();
		loginPage = new LoginPage_GIA(empAppDevice);
		dashboardPage = new DashboardPage_GIA(empAppDevice);
		menuPage = new MenuPage_GIA(empAppDevice);
		mapPage = new MapPage_GIA(empAppDevice);

	}

	@Test(description = "Verify user can see various submenu link under Burger menu", dataProvider = "TestLoginData")
	public void testSubmenudisplayInMainMenu(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException, InterruptedException {

		
		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		Assert.assertTrue(menuPage.isServiceHistoryToolDisplayed(), "Service History tool is not displayed");
		Assert.assertTrue(menuPage.isAccessoriesDisplayed(), "Accesssories is not displayed");
		Assert.assertTrue(menuPage.isMyRetailerDisplayed(), "My retailer is not displayed");
		Assert.assertTrue(menuPage.isRoadSideAssistanceDisplayed(), "Road side assistence is not displayed");
		Assert.assertTrue(menuPage.isFinanceDisplayed(), "Finance is not displayed");
		dashboardPage.openMenu();
			
	}
	
	@Test(description = "Verify user can see Accessories page when click on Accessories link", dataProvider = "TestLoginData")
	public void testAccessoriesPageinMainMenu(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToAccessories();
		Assert.assertTrue(menuPage.isAccessoriesPageDisplayed(),"Accessories Page is not opened");
		if (menuPage.isArrowIconDisplay()) {
			System.out.println("AfterMethod");
			menuPage.clickArrowBack();
			}
		
			
	}
	
	@Test(description = "Verify user can see Service History Records when click on Service History Tool", dataProvider = "TestLoginData")
	public void testServiceHistoryPageinMainMenu(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToServiceHistory();
		Assert.assertTrue(menuPage.isServiceHistoryPageDisplayed(),"Service History Page is not opened");
		if (menuPage.isArrowIconDisplay()) {
			System.out.println("AfterMethod");
			menuPage.clickArrowBack();
			}
			
	}

	@Test(description = "Verify user can see Finance page when click on Finance", dataProvider = "TestLoginData")
	public void testFinancePageinMainMenu(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.navigateToFinance();
		Assert.assertTrue(menuPage.isFinancePageDisplayed(),"Finance Page is not opened");
	}
	
	@Test(description = "Verify user can see Call Roadside Assisstance prompt message with 'Call' and 'Cancel' button", dataProvider = "TestLoginData")
	public void testRoadSideAssistancePopup(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		menuPage.clickRoadSideAssistance();
		Assert.assertTrue(menuPage.isAlertRoadsideDisplayed(), "Alert is not displayed");
		Assert.assertTrue(menuPage.isAlertBtnCallRoadsideDisplayed(),"Alert Call Button is not displayed");	
		Assert.assertTrue(menuPage.isAlertBtnCancelRoadsideDisplayed()," Alert Cancel Button button not appears");
		menuPage.clickCancelRaodside();		
	}
	
	@Test(description = "Verify My retailer page and various features inside it", dataProvider = "TestLoginData")
	public void testMyRetailerPage(String strVehicleType, String strTestRun,
			String strEmail, String strPassword, String strPin, String strVIN, String strUserType) throws IOException, InterruptedException {

		loginPage.validLogin(strEmail, strPassword, strVIN);
		dashboardPage.openMenu();
		Assert.assertTrue(menuPage.isMyRetailerDisplayed(), "My retailer is not displayed");
		menuPage.navigateToDealerLocator();
		mapPage.clickUpArrow();
		Assert.assertTrue(mapPage.isBtnScheduleServiceDisplay(),"Schedule Service Button is not displayed");
		Assert.assertTrue(mapPage.isShopAccessoriesLinkDisplayed(),"Shop Accessories link is not displayed");
		Assert.assertTrue(mapPage.isCallBtnDisplayed(),"Call button is not displayed");
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("After Class");
		driver.quit();
		//empAppDevice.getServer().stop();
	}

}

